﻿/*!
 * 框架页显示效果类
 *创建人：qinnailin
 * 创建时间：2014/7/20 
 *
 *
 * 
 */

var menuwidth = 200;
$(function () {
    var conH = $(window).height();
    var conW = $(window).width();
    if (conW < 1190) {
        menuwidth = 47;
        $("body").removeClass("main-nav-opened").addClass("main-nav-closed");
    } else {
        menuwidth = 200;
        $("body").removeClass("main-nav-closed").addClass("main-nav-opened");
    }
    $('.menus').css('width', menuwidth);
    var ul_H = $('.menus_ul').height();
    $('.menus').css('height', conH - 48);
    $('.main_tab').css('width', conW - menuwidth - 1);
    $('.pages').css('width', conW - menuwidth-1);
    $('.pages').css('height', conH - 76);
    $("#basic-modal_frame1").css({ height: conH, width: menuwidth+1 });
    $("#basic-modal_frame2").css({ height: 76, width: conW - menuwidth, left: menuwidth });
    if (ul_H > conH - 48) {
        $('.pre_next').removeClass('hide');
        $('.pre_next').addClass('show');
    } else {
        $('.pre_next').removeClass('show');
        $('.pre_next').addClass('hide');
    }
    myfunction();
    $(window).resize(function () {
        var conH = $(window).height();
        var conW = $(window).width();
        if (conW < 1190) {
            menuwidth = 47;
            $("body").removeClass("main-nav-opened").addClass("main-nav-closed");
        } else {
            menuwidth = 200;
            $("body").removeClass("main-nav-closed").addClass("main-nav-opened");
        }
        $('.menus').css('width', menuwidth);
        var ul_H = $('.menus_ul').height();
        $('.menus').css('height', conH - 48);
        $('.main_tab').css('width', conW - menuwidth - 1);
        $('.pages').css('width', conW - menuwidth-1);
        $('.pages').css('height', conH - 76);
        $("#basic-modal_frame1").css({ height: conH, width: menuwidth+1 });
        $("#basic-modal_frame2").css({ height: 76, width: conW - menuwidth, left: menuwidth });
        if (ul_H > conH - 48) {
            $('.pre_next').removeClass('hide');
            $('.pre_next').addClass('show');
        } else {
            $('.pre_next').removeClass('show');
            $('.pre_next').addClass('hide');
        }
        myfunction();
    });
});


function myfunction() {
    var index = 0;
    var size = $(".menus .menus_ul .menus_li").size();//li的总个数
    var itemheight = $(".menus .menus_ul .menus_li").height() + 1;//显示的li的高度
    var menusH = $(window).height() - 48;
    var maxMove = menusH / itemheight;
    $(".pre").unbind("click").click(function () {
        if (size - index <= maxMove) {
            return false;
        }
        index++;

        $(".menus_ul").animate({
            top: "-=" + itemheight + "px"
        }, 200)
    });
    $(".next").unbind("click").click(function () {
        if (index <= 0) {
            return false;
        }
        index--;
        $(".menus_ul").animate({
            top: "+=" + itemheight + "px"
        }, 200)
    });
};


$(function () {
    $('.search input').focus(function () {
        $('.search').addClass('search_bg2');
    });
    $('.search input').blur(function () {
        $('.search').removeClass('search_bg2');
    });
});

$(function () {
    $('.head_menus').mouseover(function () {
        $(this).find('.detailed').removeClass('hide');
        $(this).find('.detailed').addClass('show');
        $(this).addClass('head_menus_hover');
    });
    $('.head_menus').mouseout(function () {
        $(this).find('.detailed').removeClass('show');
        $(this).find('.detailed').addClass('hide');
        $(this).removeClass('head_menus_hover');
    });
});

$(function () {
    $(".slid").mouseover(function () {
        $("ul.links1").show();//you can give it a speed
        $("ul.links img").css("border-bottom", "1px solid #ecf4ff")
    });
    $(".slid").mouseleave(function () {
        $("ul.links1").hide();//you can give it a speed
    });
});

$(function () {
    $('.skins').click(function () {
        $(this).siblings('.skins').find('.yes_no').removeClass('.show').addClass('hide');
        $(this).find('.yes_no').addClass('show').removeClass('hide');
        $('#skinCss').attr("href", "css/" + (this.id) + ".css");
    });
});

/**
* 模态框架背景
* @method showbgmodel
* @for fmfrmaejs
*/
function showbgmodel() {
    $("#basic-modal_frame1").show();
    $("#basic-modal_frame2").show();
}

/**
* 关闭
* @method closebgmodel
* @for fmfrmaejs
*/
function closebgmodel() {
    $("#basic-modal_frame1").hide();
    $("#basic-modal_frame2").hide();
}

(function ($) {
    $.extend({
        frame:function(opstions){
            var ops = $.extend(true, {
                requestUrl: "",
                customMenuUrl: "",
                editCustomMenuUrl: "",
                ucenterUrl: "",
                newMsgUrl: "",
                msgDetailUrl: "",
                delMsgUrl: "",
                tagMsgUrl:""
            }, opstions);
            var menudata = new Array();
            var menulist = new Array();
            var msglist;
            var userinfo;
            var quickMenu;
            var datamenu;
            $.getJSON(ops.requestUrl, function (data) {
                try {
                    if (data.IsLogin == false) {
                        _showLoginModel();
                        return;
                    }
                } catch (e) {

                }
                frameres.reqdata = data;
                datamenu = data.menu;
                userinfo = data.userinfo;
                //quickMenu = data.quickMenu;
                var temp_menu = $("#menutemplatex").html();
                if (datamenu.length > 0) {
                    var html = fileMenuView(datamenu);
                    $("#menus_ul").html(html);
                    bingEvent();
                }
                if (userinfo) {
                    var h = new Date().getHours();
                    var s = h < 12 ? "上午好！" : "下午好！"
                    $("#userlogin_info").html("<a href='javascript:' style='color:white;' >" + userinfo.realName + "</a> " + s);
                }
                fillCustomMenu(quickMenu);
                /**
                * 快捷启动
                */
                $("#div_autoc").autoComplete({
                    data: menudata,
                    key: "menuName",
                    text: "menuName",
                    change: function (row) {
                        if (row) {
                            if (addMenu(row.menuId, row.menuName, row.parentId)) {
                                addframe(row.menuId, row.target);
                                //$("#menuleft1").find("li").children("span.active").addClass("change_bg").removeClass("active")
                                $("#span_left_menu_" + row.parentId).removeClass("change_bg").addClass("active")
                            }
                        }
                    }
                });
                if (frameres.reqdata.config.search) {
                    $("#span_config_search").show();
                } else {
                    $("#span_config_search").hide();
                }
                //菜单加载完成后才请求消息
                msglist = $("#messagebox").Grid({
                    url: ops.newMsgUrl,
                    afterCreate: function (data) {
                        if (data.Data) {
                            $("#noticesNum").text(data.Data.length);
                            if (data.Data.length > 0) {
                                $("#noticesNum").show();
                            } else {
                                $("#noticesNum").hide();
                            }
                        } else {
                            $("#noticesNum").hide();
                        }
                    }
                });
                
            });

            

            //显示详细信息
            $("#messagebox").on("click", ".showmsg", function () {
                var id = $(this).attr("data-id")
                var url = "";
                try { url = frameres.reqdata.config.pcUrl; } catch (e) { }
                $("#messagetemplate").modal({
                    title: "详细消息",
                    buttons: [
                        {
                            text: "删除",
                            click: function () {
                                $.getJSON(url + ops.delMsgUrl, { msgID: id, psessionid: userinfo.psessionid }, function (data) {
                                    if (data.flag) {
                                        infomsg("删除成功！");
                                        msglist.reload();
                                        $("#messagetemplate").close();
                                    } else {
                                        errormsg("删除失败！");
                                    }
                                });
                            }
                        },
                        {
                            text: "标记未读",
                            click: function () {
                                $.getJSON(url + ops.tagMsgUrl, { msgID: id, psessionid: userinfo.psessionid }, function (data) {
                                    if (data.flag) {
                                        infomsg("标记成功！");
                                        msglist.reload();
                                        $("#messagetemplate").close();
                                    } else {
                                        errormsg("标记失败！");
                                    }
                                });
                            }
                        }
                    ],
                    afterCreate: function () {
                        var qu = ops.msgDetailUrl;
                        if (ops.msgDetailUrl.indexOf("?") == -1) {
                            qu=qu + "?1=1";
                        }
                        $("#message_box").Detail({
                            url: url + qu + "&msgID=" + id + "&psessionid=" + userinfo.psessionid,
                            afterCreate: function () {
                                msglist.reload();
                            }
                        });
                    }
                });
            });

            ///递归填充菜单栏
            function fileMenuView(d,isChild)
            {
                var html = "";
                if (d) {
                    $.each(d, function (i, n) {
                        html += "<li><a " + (n.childs ? "class='dropdown-collapse'" : "class='menu_goto_frame dropdown-collapse'") + " data-menuId='" + n.menuId + "' data-menuName='" + n.menuName + "' data-parentId='" + n.parentId + "' data-href='" + n.target + "' href='javascript:'>";
                        if (n.menuicon) {
                            html += "<i class='" + n.menuicon + "'></i>";
                        } else if(isChild){
                            html += "<i class='icon-caret-right'></i>";
                        }
                        html += "<span>" + n.menuName + "</span>";
                        if (n.childs) {
                            html += "<i class='icon-angle-down angle-down'></i>";
                        }
                        html += "</a>";
                        if (n.childs) {
                            html += "<ul class='nav nav-stacked'>" + fileMenuView(n.childs,true) + "</ul>";
                        }
                        html += "</li>";
                    });
                }
                return html;
            }

            ///绑定折叠事件
            function bingEvent()
            {
                var body, content, nav, nav_closed_width, nav_open, nav_toggler;

                nav_toggler = $("header .toggle-nav");
                nav = $("#main-nav");
                content = $("#content");
                body = $("body");
                nav_closed_width = 50;
                nav_open = body.hasClass("main-nav-opened") || nav.width() > nav_closed_width;
                $("#main-nav .dropdown-collapse").on("click", function(e) {
                    var link, list;
                    if ($(this).hasClass("menu_goto_frame")) {
                        var url = $(this).attr("data-href");
                        var memuid = $(this).attr("data-menuid");
                        var menuName = $(this).attr("data-menuName");
                        var parentId = $(this).attr("data-parentId");

                        if (addMenu(memuid, menuName, parentId)) {
                            addframe(memuid, url);
                            $("#menuleft1").find("li").children("span.active").addClass("change_bg").removeClass("active")
                            $("#span_left_menu_" + parentId).removeClass("change_bg").addClass("active")
                        }
                    }

                    e.preventDefault();
                    link = $(this);
                    list = link.parent().find("> ul");
                    if (list.is(":visible")) {
                        if (body.hasClass("main-nav-closed") && link.parents("li").length === 1) {
                            false;
                        } else {
                            link.removeClass("in");
                            list.slideUp(300, function() {
                                return $(this).removeClass("in");
                            });
                        }
                    } else {
                        link.addClass("in");
                        list.slideDown(300, function() {
                            return $(this).addClass("in");
                        });
                    }
                    return false;
                });
        
                nav_toggler.on("click", function() {
                    if (nav_open) {
                        $(document).trigger("nav-close");
                    } else {
                        $(document).trigger("nav-open");
                    }
                    return false;
                });
                $(document).bind("nav-close", function(event, params) {
                    body.removeClass("main-nav-opened").addClass("main-nav-closed");
                    return nav_open = false;
                });
                return $(document).bind("nav-open", function(event, params) {
                    body.addClass("main-nav-opened").removeClass("main-nav-closed");
                    return nav_open = true;
                });
            }

            menulist.push("home");
            //菜单显示
            $('.frame_main').on("mouseenter", "span", function () {
                $(this).addClass('on');
                $(".menu2_con").hide();
                $(this).find(".menu2_con").show();
            });
            //菜单隐藏
            $('.frame_main').on("mouseleave", "span", function () {
                $(this).removeClass('on');
                $(this).find(".menu2_con").hide();
            });

            $("#morenotice").click(function () {
                showusercenter();
            });

            //菜单跳转
            $('#menus_ul').on("click", ".menu_goto_frame", function () {
                
            });

            //快捷菜单跳转
            $('#menumore').on("click", ".menu_goto_frame", function () {
                var url = $(this).attr("data-href");
                var memuid = $(this).attr("data-menuid");
                var menuName = $(this).attr("data-menuName");
                var parentId = $(this).attr("data-parentId");

                if (addMenu(memuid, menuName, parentId)) {
                    addframe(memuid, url);
                    $("#menuleft1").find("li").children("span.active").addClass("change_bg").removeClass("active")
                    $("#span_left_menu_" + parentId).removeClass("change_bg").addClass("active")
                }
            });
            //刷新
            $(".refresh>a").click(function () {
                var iframe = $("#frame_list").find("iframe:visible").eq(0);
                var url = $(iframe).attr("src")
                $(iframe).attr("src", url);
            });
            //关系
            $(".closeA>a").click(function () {
                $(".home_menu").addClass("click").siblings("li").remove();
                $("#div_iframe_home").show().siblings("div").html("").remove();
                $("#frame_home").show();
                menulist = new Array();
                menulist.push("home");
                $(".active").removeClass("active").addClass("change_bg");
            });
            //选中标签
            $("#ul_menu").on("click", ".t_style", function () {
                var tparent = $(this).parent("li");
                if (!$(tparent).hasClass("click")) {
                    var menuid = $(tparent).attr("data-menuId");
                    var parentid = $(tparent).attr("data-parentid");
                    ActiveMenuId(menuid);
                    setActiveMenu(menuid);

                }
            })
            //关闭标签
            $("#ul_menu").on("click", ".close", function () {
                var tparent = $(this).parent("li");
                var menuid = $(tparent).attr("data-menuId");
                var parentid = $(tparent).attr("data-parentid");
                $("#div_iframe_" + menuid).html("").remove();
                //$("#frame_" + menuid).remove();
                $(tparent).remove();
                RemoveMenuId(menuid);
                var lastid = GetLastMenuId();
                setActiveMenu(lastid);
            });

            $("#btncustom").click(function () {
                $("#customTemplate").modal({
                    title: "快捷菜单设置",
                    afterCreate: function () {
                        var customtree = $("#div_tree_box").Tree({
                            data: datamenu,
                            key: "menuId",
                            text: "menuName",
                            child:"childs",
                            expand: true,
                            multiple: true,
                            onlyleaf: true
                        });
                        $("#tablecustom").Grid({
                            data: quickMenu
                        });
                        $("#customtoright").click(function () {
                            var ds = customtree.getSelected();
                            var html = "";
                            $.each(ds, function (i, n) {
                                if (!document.getElementById("tr_cu_" + n.key)) {
                                    var temp = "<tr id='tr_cu_{{key}}' ><td width=\"80%\">{{text}}</td><td width=\"20%\"><a href=\"javascript:\" class=\"customadel\" data-id=\"{{key}}\" style=\"color:#f00;\">X</a></td></tr>";
                                    html += temp.fill(n);
                                }
                            });
                            $("#tablecustom").append(html);

                            var trcount = $("#tablecustom").find("tr").size();
                            if (trcount > 8) {
                                alertui("自定义菜单不能超过8个！");
                                $("#tablecustom").find("tr").each(function (i, n) {
                                    if (i > 7) {
                                        $(n).remove();
                                    }
                                });
                                return false;
                            }

                        });
                        $("#tablecustom").on("click", ".customadel", function () {
                            $(this).parents("tr").remove();
                        });
                    },
                    buttons: [
                       {
                           text: "确定",
                           click: function () {
                               var t = new Array();
                               $("#tablecustom").find("a.customadel").each(function (i, n) {
                                   t.push($(n).attr("data-id"));
                               });
                               if (t.length > 8) {
                                   alertui("自定义菜单不能超过8个！");
                                   return false;
                               }
                               if (t.length > 0) {
                                   var url = "";
                                   try { url = frameres.reqdata.config.pcUrl; } catch (e) { }
                                   $.getJSON(url+ops.editCustomMenuUrl, { menuID: t.join(','), psessionid: userinfo.psessionid }, function (data) {
                                       if (data.flag) {
                                           infomsg("修改成功！");
                                           $.getJSON(ops.requestUrl, function (data) {
                                               quickMenu = data.quickMenu;
                                               fillCustomMenu(quickMenu);
                                           });
                                           $("#customTemplate").close();
                                       } else {
                                           errormsg("修改失败！");
                                       }
                                   });
                               } else {
                                   infomsg("请添加菜单！");
                               }
                           }
                       }, {
                           text: "取消",
                           click: function () {
                               $(this).close();
                           }
                       }
                    ]
                });
            });

            $("#btnloginout").click(function () {
                location.href = frameres.reqdata.config.logoutUrl;
            });

            //点击名称
            $("#userlogin_info").click(function () {
                showusercenter();
            });


            /**
            * 隐藏消息盒子
            * @method HasMenuid
            * @param {string} id 菜单id
            * @for fmfrmaejs
            * @孔黎明
            */
            function hidediv() {
                if (testI <= 0) {
                    $("#messagebox").hide();
                }
            }
            var testI = 0;

            /**
            * 判断菜单是否已存在
            * @method HasMenuid
            * @param {string} id 菜单id
            * @for fmfrmaejs
            */
            function HasMenuid(id) {
                for (var i = 0; i < menulist.length; i++) {
                    if (menulist[i] == id) {
                        return true;
                    }
                }
                return false;
            }

            /**
            * 获取最后一个菜单
            * @method GetLastMenuId
            * @for fmfrmaejs
            */
            function GetLastMenuId() {
                return menulist[menulist.length - 1];
            }

            /**
            * 移除一个菜单
            * @method RemoveMenuId
            * @param {string} id 菜单id
            * @for fmfrmaejs
            */
            function RemoveMenuId(id) {
                for (var i = 0; i < menulist.length; i++) {
                    if (menulist[i] == id) {
                        menulist.splice(i, 1);
                        break;
                    }
                }
            }
            /**
            * 激活一个菜单
            * @method ActiveMenuId
            * @param {string} id 菜单id
            * @for fmfrmaejs
            */
            function ActiveMenuId(id) {
                RemoveMenuId(id);
                AddMenuId(id);
            }
            /**
            * 添加一个菜单
            * @method AddMenuId
            * @param {string} id 菜单id
            * @for fmfrmaejs
            */
            function AddMenuId(id) {
                menulist.push(id);
            }
            /**
            * 设置激活菜单显示
            * @method setActiveMenu
            * @param {string} id 菜单id
            * @for fmfrmaejs
            */
            function setActiveMenu(id) {
                $("#li_menu_" + id).addClass("click").siblings("li").removeClass("click");
                $("#div_iframe_" + id).show().siblings("div").hide();
                var parentid = $("#li_menu_" + id).attr("data-parentid");
                $(".active").removeClass("active").addClass("change_bg");
                $("#span_left_menu_" + parentid).removeClass("change_bg").addClass("active");
            }
            /**
            * 添加一个ifream
            * @method setActiveMenu
            * @param {string} memuid 菜单id
            * @param {string} url 菜单url
            * @for fmfrmaejs
            */
            function addframe(memuid, url) {
                
                $("#frame_list").children("div").hide();
                var temp = "<div id='div_iframe_" + memuid + "' style='height:100%' ><iframe id=\"frame_" + memuid + "\" src=\"" + url + "\" frameborder=\"0\" width=\"100%\" height=\"100%\"></iframe>";
                $("#frame_list").append(temp);
            }

            /**
            * 在tab中添加相应的菜单项 最多12项
            * @method addMenu
            * @param {string} memuid 菜单id
            * @param {string} menuName 菜单name
            * @param {string} parentId 父菜单id
            * @for fmfrmaejs
            */
            //在tab中添加相应的菜单项 最多12项
            function addMenu(menuId, menuName, parentId) {
                if (document.getElementById("li_menu_" + menuId)) {
                    ActiveMenuId(menuId);//激活按钮排序
                    setActiveMenu(menuId);
                    return false;
                }
                AddMenuId(menuId);
                $("#ul_menu").children("li").removeClass("click");
                var temp = "<li class=\"click\" id=\"li_menu_{{menuId}}\" data-menuId=\"{{menuId}}\" data-parentid=\"{{parentId}}\" ><a class=\"t_style\">{{menuName}}</a><a class=\"close\"></a></li>";
                if ($("#ul_menu").children("li").size() >= 13) {
                    $.msg.alert("标签数量不能超过12个!");
                    //$("#ul_menu>.home_menu").prev("li").remove();
                    return false;
                }
                $("#ul_menu").children("li").eq(0).before(temp.replaceo("menuId", menuId).replaceo("menuId", menuId).replaceo("menuName", menuName).replaceo("parentId", parentId));
                return true;
            }

            /**
            * 快速收索
            * @param {object} data 菜单数据
            */
            function resortData(data) {
                if (data) {
                    $.each(data, function (i, n) {
                        if (n.url && n.url != "") {
                            menudata.push(n);
                        }
                    });
                }
            }
            /**
            * 填充菜单子项
            * @param {object} data 菜单数据
            */
            function fillchild(data) {
                if (!data) return "";
                var html = "";
                $.each(data, function (j, d) {
                    var temp = $("#menutemplatec").html();
                    var ht = temp.fill(d);
                    html += ht;
                });
                return html;
            }

            /**
            * 填充自定义菜单
            * @method fillCustomMenu
            * @param {object} d 需要填充的数据
            * @for fmfrmaejs
            */
            function fillCustomMenu(d) {
                if (d) {
                    var more = "";
                    $.each(d, function (i, n) {
                        more += "<li ><a class=\"menu_goto_frame\" data-menuid=\"{{menuId}}\" data-parentid=\"{{parentId}}\"  data-menuname=\"{{menuName}}\" data-href=\"{{url}}\" target=\"frame\" >{{menuName}}</a></li>"
                            .fill(n);
                    });
                    $("#menumore").html(more);
                }
            }

            //打开个人中心
            function showusercenter() {
                $.each(menudata, function (i, n) {
                    if (n.url.indexOf(ops.ucenterUrl) > 0) {
                        if (addMenu(n.menuId, n.menuName, n.parentId)) {
                            addframe(n.menuId, n.url);
                            //$("#menus_ul").find("li").children("span.active").addClass("change_bg").removeClass("active")
                            $("#span_left_menu_" + n.parentId).removeClass("change_bg").addClass("active")
                        }
                    }
                });
            }

            var frameres = {
                reqdata: {},
                setsearch: function (isshow) {
                    if (isshow) {
                        $("#span_config_search").show();
                    } else {
                        $("#span_config_search").hide();
                    }
                }
            }
            return frameres;
        }
    });
    

})(jQuery);

_showLoginModel = function () {
    var issuccess = false;
    $("#templateUserLoginmodel").modal({
        title: "用户登录",
        buttons: [
            {
                text: "确定", click: function () {
                    $("#frameloginform").submit();
                }
            },
            {
                text: "关闭", click: function () {
                    location.reload();
                }
            }
        ],
        afterCreate: function () {
            var form = $("#frameloginform").Form({
                success: function (d) {
                    if (d.error == 0) {
                        issuccess = true;
                        $("#templateUserLoginmodel").close();
                        var iframe = $("#frame_list").find("iframe:visible").eq(0);
                        var url = $(iframe).attr("src");
                        $(iframe).attr("src", url);
                        closebgmodel();
                    } else {
                        $("#login_show_error_msg").text(d.msg);
                    }
                }
            });
        },
        beforeClose: function () {
            if (!issuccess) {
                location.reload();
            }
        }
    });
}