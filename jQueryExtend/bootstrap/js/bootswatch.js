
$('a[rel=tooltip]').tooltip({
	'placement': 'bottom'
});

// fix sub nav on scroll
var $win = $(window)
  , $nav = $('.subnav')
  , navTop = $('.subnav').length && $('.subnav').offset().top - 40
  , isFixed = 0

processScroll()

$win.on('scroll', processScroll)

function processScroll() {
    var i, scrollTop = $win.scrollTop()
    if (scrollTop >= navTop && !isFixed) {
        isFixed = 1
        $nav.addClass('subnav-fixed')
    } else if (scrollTop <= navTop && isFixed) {
        isFixed = 0
        $nav.removeClass('subnav-fixed')
    }
}

function setlabel(obj, tagobj) {
    var oldlabel = $(tagobj).val();
    if (oldlabel != "" && oldlabel != $(tagobj).attr("placeholder")) {
        $(tagobj).val(oldlabel + "," + $(obj).html());
    } else {
        $(tagobj).val($(obj).html());
    }
    $(obj).remove();
}

$(document).ajaxError(function (event, request, settings) {
    showerror("��������ʧ�ܣ�");
});

$(window).bind("scroll", function () {
    if ($(document).scrollTop() > 200) {
        $("#backtotop").show();
    } else {
        $("#backtotop").hide();
    }
});
$("#backtotop").click(function () {
    $('html, body').animate({ scrollTop: 0 }, 500);
});

function setmainmenu(tag) {
    $("#main-menu-left [tag=" + tag + "]").addClass("active").siblings().removeClass("active");
}

function createEditor(control) {
    var editor;
    KindEditor.basePath = '/js/KindEditor/';
    editor = KindEditor.create(control, {
        filterMode: true,
        uploadJson: '/Tool/Upload/',
        newlineTag: "p",
        items: [
                    'source', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
                    'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
                    'insertunorderedlist', 'link', 'unlink', '|', 'clearhtml', 'quickformat', 'selectall', 'fullscreen',
        '|', 'image', 'flash', 'map'],
        htmlTags: {
            font: ['color', 'size', 'face', '.background-color'], span: ['.color', '.background-color', '.font-size', '.font-family', '.background',
            '.font-weight', '.font-style', '.line-height'],
            a: ['href', 'target', 'name'],
            div: ['style'], p: ['style'],
            img: ["src", "alt", "width", "height"], embed: ['src', 'width', 'height', 'type', 'loop', 'autostart', 'quality', '.width', '.height', 'align', 'allowscriptaccess'],
            'br,,strong,b,sub,sup,em,i,u,strike,s,del': []
        },
        resizeType: 1,
        afterChange: function () {
            this.sync();
        }
    });
    return editor;
}