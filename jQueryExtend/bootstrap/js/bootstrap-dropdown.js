﻿/*
* fmtxDropdown 1.4.2 - jQuery Plugin
* fmtxc@qq.com
* Copyright (c) 2011 qinnailin.cn
* Licensed under MIT and GPL
* Date:2012-03-20
* http://qinnailin.cn
*/
(function ($) {
    $.fn.fmtxDropdown = function (options) {
        var ops = $.extend({
			url: "",
			value: null,
			param: null,
			paramkey: null,
			keyword: "keyword",
			change: function () { }
		}, options || {});
        var objthis = $(this);
        $(this).parent().removeClass("dropdown").addClass("dropdown");
        $(objthis).parent().append("<ul class=\"typeahead dropdown-menu\"></ul>");
		$(this).attr("autocomplete","off");
        if (ops.value != null && ops.url != "") {
            $(objthis).keyup(function () {
                var name = $(objthis).val();
                if (name == "") {
                    $("input[name=" + ops.value + "]").val(0);
                    $(objthis).siblings("ul:first").hide();
                    $(objthis).siblings("ul:first li").remove();
                    return;
                }
                $.get(ops.url + "?" + ops.keyword + "=" + name + (ops.param ? "&"+ops.paramkey + "=" + $(ops.param).val() : ""), function (data) {
                    if (data) {
                        var html = "";
                        $.each(data, function (i, n) {
                            html += "<li style='display:inline'><a aid='{0}' href='javascript:' name='{1}' >{1}</a></li>".format(n.Id, n.Name);
                        });
                        $(objthis).siblings("ul:first").html(html).show();
                        $(objthis).siblings("ul:first").find("li a").bind("click", function () {
                            var name = $(this).text();
                            var id = $(this).attr("aid");
                            $(objthis).val(name);
                            $("input[name=" + ops.value + "]").val(id);
                            $(objthis).siblings("ul:first").hide();
                            $(objthis).focus();
                            ops.change(id,name);
                        });
                    }
                });
            });
        }
    }
})(jQuery);