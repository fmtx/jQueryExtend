﻿/*
* fmtxValidation 1.4.2 - jQuery Plugin
* fmtxc@qq.com
* Copyright (c) 2011 qinnailin.cn
* Licensed under MIT and GPL
* Date:2012-03-20
* http://qinnailin.cn
*/
(function ($) {
    $.fmtxValidation = function (ops) {
        $("input[req=check]").not(":radio").not(":checkbox").blur(function () {
            validation($(this), false);
        });
        $("input[req=check]").not(":radio").not(":checkbox").keyup(function () {
            validation($(this), false);
        });
        $("input[req=check]").not(":radio").not(":checkbox").change(function () {
            validation($(this), false);
        });
        $("input[req=check]").not(":text").click(function () {
            radname = "";
            validation($(this), false);
        });
        $("select[req=check]").change(function () {
            validation($(this), false);
        });
        $("textarea[req=check]").blur(function () {
            validation($(this), false);
        });
        $("textarea[req=check]").keyup(function () {
            validation($(this), false);
        });
        var radname;
        validation = function (objthis, framcheck) {
            var inputsize = ops.inputsize;
            var inputformat = ops.inputformat;
            var opserrormsg = ops.errormsg;
            var objname = $(objthis).attr("name");
            var objval = $(objthis).val();
            if (inputsize != null && notinputbox(objthis)) {
                $.each(inputsize, function (i, n) {
                    if (i.toString() == objname) {
                        if (objval.length >= parseInt(n[0]) && objval.length <= parseInt(n[1])) {
                            functions.success(objthis);
                            andcheck(objthis, n[2], objname, opserrormsg[objname]);
                        } else {
                            orcheck(objthis, n[2], objname, opserrormsg[objname]);
                        }
                        if (!framcheck) return;
                    }
                });
            }
            if (inputformat != null && notinputbox(objthis)) {
                $.each(inputformat, function (i, n) {
                    if (i.toString() == objname) {
                        var r = RegExp(n[0] == null ? n : n[0]);
                        if (r.test(objval)) {
                            functions.success(objthis);
                        } else {
                            functions.showMsg(opserrormsg[i.toString()], objthis);
                        }
                        if (!framcheck) return;
                    }
                });
            }
            if ($(objthis).is(":radio")) {
                if (radname != objname) {
                    radname = objname;
                    var radobj = $("input[req=check][name=" + objname + "]:last");
                    var vals = $("input[req=check][name=" + objname + "]:checked").val();
                    var paobj = $(radobj).parent();
                    if (paobj != null) radobj = paobj;
                    if (vals == null) {
                        functions.showMsg(opserrormsg[objname], radobj);
                    } else {
                        functions.success(radobj);
                    }
                }
            }
            if ($(objthis).is(":checkbox")) {
                if (radname != objname) {
                    radname = objname;
                    var radobj = $("input[req=check][name=" + objname + "]:last");
                    var vals = $("input[req=check][name=" + objname + "]:checked").length;
                    var paobj = $(radobj).parent();
                    if (paobj != null) radobj = paobj;
                    if (vals >= inputsize[objname][0] && vals <= inputsize[objname][1]) {
                        functions.success(radobj);
                    } else {
                        functions.showMsg(opserrormsg[objname], radobj);
                    }
                }
            }
            if ($(objthis).is("select")) {
                var vals = $(objthis).val().replace(/(^\s*)|(\s*$)/g, "");
                if (vals != null && vals != inputsize[objname][0]) {
                    functions.success(objthis);
                } else {
                    functions.showMsg(opserrormsg[objname], objthis);
                }
            }
        }
        notinputbox = function (objthis) {
            if (!$(objthis).is(":radio") && !$(objthis).is(":checkbox") && !$(objthis).is("select")) return true;
        };

        andcheck = function (objthis, data, name, msg) {
            if (data != null) {
                var d = eval(data);
                var boook = true;
                for (var x in d) {
                    var ax = data[x];
                    var andinput = $("input[name=" + x + "]").val();
                    if (andinput == null) continue;
                    switch (ax[0]) {
                        case "andnot":
                            if (ax[1] == andinput) {
                                boook = false;
                                break;
                            }
                            break;
                        case "and":
                            if (ax[1] != andinput) {
                                boook = false;
                                break;
                            }
                            break;
                        case "eq":
                            var eqinput = $("input[name=" + ax[0] + "]").val();
                            if (eqinput == null) continue;
                            if (andinput != eqinput) {
                                boook = false;
                                break;
                            }
                            break;
                        default:
                    }
                }
                if (boook) {
                    functions.success(objthis);
                } else {
                    functions.showMsg(msg, objthis);
                }
            }
            
        }

        orcheck = function (objthis, data, name, msg) {
            var boook = false;
            if (data != null) {
                var d = eval(data);
                for (var x in d) {
                    var ax = data[x];
                    var andinput = $("input[name=" + x + "]").val();
                    if (andinput == null) continue;
                    switch (ax[0]) {
                        case "ornot":
                            if (ax[1] != andinput) {
                                boook = true;
                                break;
                            }
                            break;
                        case "or":
                            if (ax[1] == andinput) {
                                boook = true;
                                break;
                            }
                            break;
                        default:
                    }
                }
            }
            if (boook) {
                functions.success(objthis);
            } else {
                functions.showMsg(msg, objthis);
            }
            
        }
        var functions = {
            showMsg: function (msg, objthis) {
                $(objthis).parent().parent().removeClass("success");
                if (msg != null && msg != "") {
                    $(objthis).next("span.help-inline").hide();
                    $(objthis).siblings(".temp-help").remove();
                    $(objthis).after("<span class=\"help-inline temp-help\">" + msg + "</span>");
                }
                $(objthis).parent().parent().addClass("error");
            },
            success: function (objthis) {
                $(objthis).parent().parent().removeClass("error");
                $(objthis).next("span.temp-help").remove();
                $(objthis).next("span.help-inline").show();
                $(objthis).parent().parent().addClass("success");
            },
            somvalidation: function (options) {
                if (options != null) {
                    $.each(options, function (i, n) {
                        var objthis = $("[req=check][name=" + i + "]");
                        if ($(objthis).is(":radio") || $(objthis).is(":checkbox")) {
                            objthis = $("input[req=check][name=" + i + "]:last");
                            var paobj = $(objthis).parent();
                            if (paobj != null) objthis = paobj;
                        }
                        functions.showMsg(n, objthis);
                    });
                }
            }
        }
        return functions;

        $("form.fmtxvalidation").submit(function () {
            radname = "";
            $(this).contents().find("[req=check]").each(function () {
                validation($(this), true);
            });
            if ($(this).find("div.error").length > 0) {
                return false;
            }
            return true;
        });
    };
})(jQuery);