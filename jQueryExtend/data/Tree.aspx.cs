﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jQueryExtend.data
{
    public partial class Tree : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = Request["id"]??"0";
            var lv=Request["lv"];
            if (id == "0")
            {
                var data = "[{ Id: 1,  Name: '张三',isParent:false }," +
                "{ Id: 2, Name: '李思',isParent:true }," +
                "{ Id: 3, Name: '王五',isParent:true }]";
                var jdata = JsonConvert.DeserializeObject(data);
                Response.ContentType = "application/json";
                Response.Write(JsonConvert.SerializeObject(jdata));
                Response.End();
            }
            else {
                var data = "[{ Id: " + id + "1,  Name: '张三" + id + "',isParent:true }," +
                 "{ Id: " + id + "3, Name: '李思" + id + "',isParent:true }," +
                 "{ Id: " + id + "3, Name: '王五" + id + "',isParent:false,checked:true }]";
                var jdata = JsonConvert.DeserializeObject(data);
                Response.ContentType = "application/json";
                Response.Write(JsonConvert.SerializeObject(jdata));
                Response.End(); 
            }
        }

    }
}