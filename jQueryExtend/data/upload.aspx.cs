﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Newtonsoft.Json;

namespace jQueryExtend.data
{
    public partial class upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var filename = Request["qqfilename"];
            var gis = Request.Files["qqfile"];
            if (gis != null && filename != null)
            {
                try
                {
                    gis.SaveAs(Server.MapPath(@"\upfiles\")+ filename);
                    Response.Write(JsonConvert.SerializeObject(new { success = true, msg="",file= filename }));
                }
                catch (Exception exx)
                {
                    Response.Write(JsonConvert.SerializeObject(new { success = false, msg = exx.Message }));
                }

            }
            else
            {
                Response.Write(JsonConvert.SerializeObject(new { success = false, msg = "上传失败" }));
            }
            //Response.ContentType = "text/json";
            Response.End();
        }
    }
}