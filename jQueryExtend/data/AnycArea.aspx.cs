﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using fmtx.Framework.SqlHelper;
using System.Collections;
using Newtonsoft.Json;

namespace jQueryExtend.data
{
    public partial class AnycArea : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var plist = FmtxHelper.Query<Province>("select * from S_Province");
            var clist = FmtxHelper.Query<City>("select * from S_City");
            var dlist = FmtxHelper.Query<District>("select * from S_District");
            Hashtable hash = new Hashtable();
            hash["Province"] = plist;
            hash["City"] = clist;
            hash["District"] = dlist;

            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(hash));
            Response.End();
        }
    }
}