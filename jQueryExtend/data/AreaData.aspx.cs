﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using fmtx.Framework.SqlHelper;
using fmtx.Framework.Attribute;
using fmtx.Framework.Model;
using Newtonsoft.Json;
using System.Collections;

namespace jQueryExtend.data
{
    public partial class AreaData : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            var plist = FmtxHelper.Query<Province>("select * from S_Province");
            var clist = FmtxHelper.Query<City>("select * from S_City");
            var dlist = FmtxHelper.Query<District>("select * from S_District");
            Hashtable hash = new Hashtable();
            hash["Province"] = plist;
            hash["City"] = clist;
            hash["District"] = dlist;

            Response.Write("var areadate="+JsonConvert.SerializeObject(hash));
            Response.End();
        }
    }

    [TableName("S_Province")]
    public class Province:BaseBean{
        public long ProvinceID { get; set; }

        public string ProvinceName { get; set; }
    }

    [TableName("S_City")]
    public class City : BaseBean
    {
        public long CityID { get; set; }
        public string CityName { get; set; }

        public string ZipCode { get; set; }

        public long ProvinceID { get; set; }
    }

    [TableName("S_District")]
    public class District : BaseBean
    {
        public long DistrictID { get; set; }

        public string DistrictName { get; set; }

        public long CityID { get; set; }

        public string ZipCode { get; set; }
    }
}