﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jQueryExtend.data
{
    public partial class uploadswf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var app = Request.Files["Filedata"];
            app.SaveAs(Server.MapPath("~/upfiles/")  + app.FileName);
            Response.Write("{success:true,file:'" + app.FileName + "'}");
            Response.ContentType = "text/json";
            Response.End();
        }
    }
}