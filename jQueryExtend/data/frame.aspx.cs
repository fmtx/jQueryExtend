﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace jQueryExtend.data
{
    public partial class frame : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Menu> list = new List<Menu>();
            Menu menu = new Menu();
            menu.menuId = "12345";
            menu.menuName = "子站";
            menu.parentId = "12345";
            menu.url = "http://10.0.2.41:8088/";
            menu.icon = "img/sys.png";

            Menu menuc1 = new Menu();
            menuc1.menuId = "123451";
            menuc1.menuName = "子站1";
            menuc1.parentId = "123451";
            menuc1.url = "http://10.0.2.41:8088/";

            Menu menuc2 = new Menu();
            menuc2.menuId = "123452";
            menuc2.menuName = "子站c";
            menuc2.parentId = "123452";
            menuc2.url = "http://10.0.2.41:8088/basejs.html";
            

            menu.childs = new List<Menu>();
            menu.childs.Add(menuc1);
            menu.childs.Add(menuc2);
            list.Add(menu);
            

            Menu menu1 = new Menu();
            menu1.menuId = "123451";
            menu1.menuName = "子站2";
            menu1.parentId = "123451";
            menu1.url = "http://10.0.2.41:8088/";
            menu1.icon = "img/bg_menu2.png";

            list.Add(menu1);

            List<Menu> quick = new List<Menu>();
            quick.Add(menuc1);
            quick.Add(menuc2);

            UserInfo bean = new UserInfo();
            bean.realName = "张三";
            bean.psessionid = "sdfsdf";

            Hashtable hash = new Hashtable();
            hash["menu"] = list;
            hash["userinfo"] = bean;
            hash["quickMenu"] = quick;
            hash["config"] = new { pcUrl = "http://10.0.2.41:8088/", logoutUrl = "http://10.0.2.40:9022/cas/logout?service=http://10.0.2.40:8010/sys/userLogin/franmeIndex.do", search = true };

            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(hash));
            Response.End();
        }

        public class Menu
        {
            public string menuId { get; set; }

            public string menuName { get; set; }

            public string parentId { get; set; }

            public string url { get; set; }

            public string icon { get; set; }

            public List<Menu> childs { get; set; }
        }

        public class UserInfo {
            public string realName { get; set; }

            public string psessionid { get; set; }
        }

    }
}