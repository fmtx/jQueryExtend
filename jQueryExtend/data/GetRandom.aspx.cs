﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jQueryExtend.data
{
    public partial class GetRandom : System.Web.UI.Page
    {
        private int idcount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            var ac = Request["ac"];
            switch (ac)
            {
                case "1":
                    WriteModal1();
                    break;
                case "2":
                    WriteModal2();
                    break;
                default:
                    break;
            }
            
        }

        private void WriteModal1()
        {
            List<Hashtable> list = new List<Hashtable>();
            for (int i = 0; i < 2; i++)
            {
                Hashtable hash = new Hashtable();
                Random a = new Random(i + (int)DateTime.Now.Ticks);
                hash["a"] = a.Next(100);
                Random b = new Random(i + 5 + (int)DateTime.Now.Ticks);
                hash["b"] = b.Next(100);
                list.Add(hash);
            }
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(list));
            Response.End();
        }

        private void WriteModal2()
        {
            List<Hashtable> list = new List<Hashtable>();
            for (int i = 0; i < 2; i++)
            {
                Hashtable hash = new Hashtable();
                Random a = new Random(i + (int)DateTime.Now.Ticks);
                hash["value"] = a.Next(100);
                hash["key"] = DateTime.Now.ToString("HH:mm:ss");
                hash["Id"] = idcount++;
                list.Add(hash);
            }
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(list));
            Response.End();
        }
    }
}