﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jQueryExtend.data
{
    public partial class msglist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Msg> list = new List<Msg>();
            list.Add(new Msg("1","测试手机里的房间里","张三","2014-10-09","sjdflkj速率可达见风使舵飞机法律手段飞机"));
            list.Add(new Msg("2", "测试手机里的房间里1", "张三", "2014-10-09", "sjdflkj速率可达见风使舵飞机法律手段飞机"));
            list.Add(new Msg("3", "测试手机里的房间里2", "张三", "2014-10-09", "sjdflkj速率可达见风使舵飞机法律手段飞机"));
            list.Add(new Msg("4", "测试手机里的房间里3", "张三", "2014-10-09", "sjdflkj速率可达见风使舵飞机法律手段飞机"));
            list.Add(new Msg("5", "测试手机里的房间里4", "张三", "2014-10-09", "sjdflkj速率可达见风使舵飞机法律手段飞机"));
            list.Add(new Msg("6", "测试手机里的房间里5", "张三", "2014-10-09", "sjdflkj速率可达见风使舵飞机法律手段飞机"));
            Hashtable hash = new Hashtable();
            hash["Data"] = list;
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(hash));
            Response.End();
        }

        public class Msg {

            public Msg(string messageID,string title, string sendName, string sendDateStr, string content)
            {
                this.messageID = messageID;
                this.title = title;
                this.sendName = sendName;
                this.sendDateStr = sendDateStr;
                this.content = content;
            }
            public string messageID { get; set; }
            public string title { get; set; }

            public string sendName { get; set; }

            public string sendDateStr { get; set; }

            public string content { get; set; }
        }
    }
}