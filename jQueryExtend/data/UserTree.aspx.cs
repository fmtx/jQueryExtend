﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jQueryExtend.data
{
    public partial class UserTree1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var list = new List<OrganizaVM>();
            var bean1 = new OrganizaVM();
            bean1.ID = Guid.NewGuid();
            bean1.Name = "经理";
            bean1.isParent = true;
            bean1.Child = GetOrg(bean1.ID,0);
            list.Add(bean1);
            var bean2 = new OrganizaVM();
            bean2.ID = Guid.NewGuid();
            bean2.Name = "经理2";
            bean2.isParent = true;
            bean2.Child = GetOrg(bean2.ID, 0);
            list.Add(bean2);
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(list));
            Response.End();
        }

        public List<OrganizaVM> GetOrg(Guid pid,int count)
        {
            count++;
            if (count == 5) return null;
            var list = new List<OrganizaVM>();
            for (int i = 0; i < 3; i++)
            {
                var id = Guid.NewGuid();
                var bean = new OrganizaVM() {
                    ID = id,
                    Name = "开发"+i+"部",
                    ParentID=pid,
                    Child=GetOrg(id,count)
                };
                list.Add(bean);
            }
            return list;
        }
    }

    /// <summary>
    /// 组织结构树
    /// </summary>
    public class OrganizaVM
    {
        public Guid ID { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 上级级部门
        /// </summary>
        public Guid ParentID { get; set; }
        public string ParentName { get; set; }
        public bool isParent { get; set; }

        /// <summary>
        /// 子节点列表
        /// </summary>
        public List<OrganizaVM> Child { get; set; }
    }
}