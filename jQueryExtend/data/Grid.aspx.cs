﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Collections;
using fmtx.Framework;

namespace jQueryExtend.data
{
    
    public partial class Grid : System.Web.UI.Page
    {
        public static List<Model> list;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (list == null || list.Count == 0)
            {
                list = new List<Model>();
                for (int i = 0; i < 100; i++)
                {
                    Model bean = new Model();
                    bean.Id = i;
                    bean.Name = "测试" + i;
                    Random random = new Random(DateTime.Now.Second);
                    try
                    {
                        bean.Age = random.Next(10, 100);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex.ToString());
                    }
                    bean.Intime = DateTime.Now;
                    list.Add(bean);
                }
            }
            var ac=Request.QueryString["ac"].ToLower();
            switch (ac)
            {
                case "pager":
                    var index = Convert.ToInt32(Request.QueryString["index"]);
                    var size = Convert.ToInt32(Request.QueryString["size"]);
                    var name=Request["name"];
                    GetPager(index,size);
                    break;
                case "postpager":
                    var indexp = Convert.ToInt32(Request.QueryString["index"]);
                    var sizep = Convert.ToInt32(Request.QueryString["size"]);
                    var namep = Request["name"];
                    PostPager(indexp, sizep);
                    break;
                case "form":
                    var uid = Convert.ToInt32(Request.QueryString["uid"]);
                    GetForm(uid);
                    break;
                case "autocom":
                    var keyword = Request.QueryString["keyword"];
                    var maxSize = Convert.ToInt32(Request.QueryString["maxSize"]);
                    AutoComplate(keyword, maxSize);
                    break;
                case "select":
                    SelectBox();
                    break;
                case "line":
                    ChartLine();
                    break;
                default:
                    break;
            }
            
        }

        private void GetPager(int index, int size)
        {
            var data=Request["sort"];
            var sort = new Hashtable();
            if (data != null)
            {
                sort = JsonConvert.DeserializeObject<Hashtable>(data);
            }
            IEnumerable<Model> olist = list;

            try
            {
                olist = list.OrderBy(o => o.Id);

                var k = "";
                foreach (string key in sort.Keys)
                {
                    k = key;
                }

                if (Convert.ToInt32(sort[k]) == 0 && k == "Name")
                {
                    olist = list.OrderBy(o => o.Name);
                }
                else if (Convert.ToInt32(sort[k]) == 1 && k == "Name")
                {
                    olist = list.OrderByDescending(o => o.Name);
                }
            }
            catch (Exception)
            { }

            var items = olist.Skip((index - 1) * size).Take(size);

            Hashtable hash = new Hashtable();
            hash["Data"] = items;
            hash["Count"] = list.Count;
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(hash));
            Response.End();
        }

        private void PostPager(int index, int size)
        {

            var data = Request["sort"];
            var sort = new Hashtable();
            if (data != null)
            {
                sort = JsonConvert.DeserializeObject<Hashtable>(data);
            }
            var olist = list.OrderBy(o => o.Id);

            var k = "";
            foreach (string key in sort.Keys)
            {
                k = key;
            }

            if (Convert.ToInt32(sort[k]) == 0 && k == "Name")
            {
                olist = list.OrderBy(o => o.Name);
            }
            else if (Convert.ToInt32(sort[k]) == 1 && k == "Name")
            {
                olist = list.OrderByDescending(o => o.Name);
            }

            var items = olist.Skip((index - 1) * size).Take(size);

            Hashtable hash = new Hashtable();
            hash["Data"] = items;
            hash["Count"] = list.Count;
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(hash));
            Response.End();
        }

        private void GetForm(int uid)
        {
            Hashtable hash = new Hashtable();
            hash["Name"] = "张三";
            hash["area"] = 3;
            hash["uid"] = uid;
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(hash));
            Response.End();
        }

        private void AutoComplate(string key,int size)
        {
            var items = list.Where(w=>w.Name.Contains(key)).Take(size);
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(items));
            Response.End();
        }

        private void SelectBox()
        {
            var items = list.Take(15);
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(items));
            Response.End();
        }

        private void ChartLine()
        {
           Response.ContentType = "application/json";
           var data = "{"+
                "category: [\"衬衫\", \"羊毛衫\", \"雪纺衫\", \"裤子\", \"高跟鞋\", \"袜子\"],"+
                "values: ["+
                    "{ name:'销量',value: [5, 20, 40, 10, 10, 20] },"+
                    "{ name:'成本',value: [4, 19, 20, 15, 9, 19] }"+
                "]}";
           var dd = JsonConvert.DeserializeObject(data);
           Response.Write(JsonConvert.SerializeObject(dd));
           Response.End();
        }
    }

    public class Model
    {
        public int Id;
        public string Name;
        public int Age;
        public DateTime Intime;
    }
}