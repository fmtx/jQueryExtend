﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Collections;
using fmtx.Framework.Cache;

namespace jQueryExtend.data
{
    public partial class GridTree : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var num = Convert.ToInt32(Request.QueryString["num"]);
            var id = Request.QueryString["Id"];
            var cache = new DataCache("cache_"+num);

            if (!string.IsNullOrEmpty(id))
            {
                WriteNode(100);
            }
            else
            {
                List<UserTree> list = cache.Get() as List<UserTree>;
                if (list == null)
                {
                    list = new List<UserTree>();
                    for (int i = 0; i < num; i++)
                    {
                        var bean = new UserTree();
                        bean.Id = Guid.NewGuid().ToString();
                        bean.Name = Guid.NewGuid().ToString();
                        bean.Age = i;
                        bean.Time = DateTime.Now.ToString("yyyy-MM-dd");
                        bean.Extend = DateTime.Now.ToString("yyyy-MM-dd") + Guid.NewGuid().ToString();
                        bean.Open = true;
                        list.Add(bean);
                    }
                }
                Hashtable hash = new Hashtable();
                hash["Data"] = list;

                Response.Write(JsonConvert.SerializeObject(hash));
                Response.End();
            }
        }

        public void WriteNode(int num)
        {
            var cache = new DataCache("cache_" + num);
            List<UserTree> list = cache.Get() as List<UserTree>;
            if (list == null)
            {
                list = new List<UserTree>();
                for (int i = 0; i < num; i++)
                {
                    var bean = new UserTree();
                    bean.Id = Guid.NewGuid().ToString();
                    bean.Name = Guid.NewGuid().ToString();
                    bean.Age = i;
                    bean.Time = DateTime.Now.ToString("yyyy-MM-dd");
                    bean.Extend = DateTime.Now.ToString("yyyy-MM-dd") + Guid.NewGuid().ToString();
                    bean.Open = (i%2==0);
                    list.Add(bean);
                }
            }
            Hashtable hash = new Hashtable();
            hash["Data"] = list;

            Response.Write(JsonConvert.SerializeObject(hash));
            Response.End();
        }

        public List<UserTree>GetChild(int num)
        {
            List<UserTree> list = new List<UserTree>();
            for (int i = 0; i < num; i++)
            {
                var bean = new UserTree();
                bean.Id = Guid.NewGuid().ToString();
                bean.Name = Guid.NewGuid().ToString();
                bean.Age = i;
                bean.Time = DateTime.Now.ToString("yyyy-MM-dd");
                bean.Extend = DateTime.Now.ToString("yyyy-MM-dd") + Guid.NewGuid().ToString();
                bean.Child = GetChildA(num);
                list.Add(bean);
            }
            return list;
        }

        public List<UserTree> GetChildA(int num)
        {
            List<UserTree> list = new List<UserTree>();
            for (int i = 0; i < num; i++)
            {
                var bean = new UserTree();
                bean.Id = Guid.NewGuid().ToString();
                bean.Name = Guid.NewGuid().ToString();
                bean.Age = i;
                bean.Time = DateTime.Now.ToString("yyyy-MM-dd");
                bean.Extend = DateTime.Now.ToString("yyyy-MM-dd") + Guid.NewGuid().ToString();
                bean.Child = GetChildB(num);
                list.Add(bean);
            }
            return list;
        }

        public List<UserTree> GetChildB(int num)
        {
            List<UserTree> list = new List<UserTree>();
            for (int i = 0; i < num; i++)
            {
                var bean = new UserTree();
                bean.Id = Guid.NewGuid().ToString();
                bean.Name = Guid.NewGuid().ToString();
                bean.Age = i;
                bean.Time = DateTime.Now.ToString("yyyy-MM-dd");
                bean.Extend = DateTime.Now.ToString("yyyy-MM-dd") + Guid.NewGuid().ToString();
                list.Add(bean);
            }
            return list;
        }

    }

    public class UserTree 
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public string Time { get; set; }

        public string Extend { get; set; }

        public bool Open { get; set; }

        public List<UserTree> Child { get; set; }
    }
}