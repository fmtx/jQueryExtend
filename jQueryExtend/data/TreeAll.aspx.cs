﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jQueryExtend.data
{
    public partial class TreeAll : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var data = "[{ Id: 1,  Name: '张三',Child:[{ Id: 11, Name: '李思11' },{ Id: 12, Name: '李思12' },{ Id: 13, Name: '李思13' }] }," +
                "{ Id: 2, Name: '李思', Child:[{Id: 21, Name: '李思21',checked:true,Child:[{ Id: 211, Name: '李思211' },{ Id: 212, Name: '李思212' },{ Id: 213, Name: '李思213' }]}]}," +
                "{ Id: 3, Name: '王五'}]";
            var jdata = JsonConvert.DeserializeObject(data);
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(jdata));
            Response.End();
        }
    }
}