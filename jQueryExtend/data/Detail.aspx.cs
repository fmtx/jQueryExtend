﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jQueryExtend.data
{
    public partial class Detail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var ac = Request["ac"];
            switch (ac)
            {
                case "1":
                    Get();
                    break;
                case "2":
                    GetEmpty();
                    break;
                default:
                    break;
            }
            
        }

        private void Get()
        {
            var bean = new { Name = "张三", ID = 1 };

            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(bean));
            Response.End();
        }

        private void GetEmpty()
        {
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(null));
            Response.End();
        }
    }
}