﻿
/*!
 * GridTree表格树控件
 * 创建人：qinnailin
 * 创建时间：2015/1/5 
 */

!(function ($) {
    "use strict"
    var GridTree = function (element, options) {
        this.init("GridTree", element, options);
    }//对象
    GridTree.prototype = {
        init: function (type, element, options) {
            var ops;
            this.type = type,
            this.$element = $(element),
            this.$id = $(element).attr("id"),
            this.options = ops = this.getoptions(options),
            this.tempBox = this.getTempBox(),
            this.template = this.getTemp();
            this.tempBox.addClass("fm-gtree");
            this.tempBox.html("");
            this.loadData();
            var $this = this;
            this.tempBox.on("click", "span.switch", function () {
                var obj = $(this).parents("tr").eq(0);
                var chk = $(this).siblings("span.chk").hasClass("checkbox_true_full_focus");
                if (obj.hasClass("gtclose")) {
                    var data = obj.data("rowdata");
                    if (data[$this.options.dataFormat.open]) {
                        var pk = "";
                        if (ops.param) {
                            pk = "{";
                            var array = new Array();
                            $.each(ops.param, function (i, n) {
                                array.push("'" + ops.param[i] + "':'" + data[ops.param[i]] + "'");
                            });
                            pk += array.join(",");
                            pk += "}";
                        }
                        $.getJSON(ops.url, stringToJSON(pk), function (d) {
                            var rd;
                            if (ops.source != "") {
                                var temp = d;
                                var item = ops.source.split('.');
                                $.each(item, function (i, n) {
                                    if (n != "") {
                                        temp = temp[n];
                                    }
                                });
                                rd = temp;
                            }
                            $this.fillAjaxNode(obj, data, rd, chk);
                            $this.open(obj);
                        });
                    } else if (data[$this.options.dataFormat.child]) {
                        $this.fillNode(obj, data, chk);
                        $this.ckopen(obj);
                    } else {
                        $this.ckopen(obj);
                    }
                } else {
                    $this.fold(obj);
                }
                return false;
            });
            //this.tempBox.on("click", "span.chk", function () {
            //    var obj = $(this);
            //    $this.checked(obj);
            //    return false;
            //});
            this.tempBox.on("click", "tr", function () {
                $this.selected($(this));
                return false;
            });
        },//参数
        getoptions: function (options) {
            return $.extend({}, $.fn[this.type].defaults, options);
        },//盒子
        getTempBox: function () {
            this.$element.addClass("container-fluid").addClass("layout-box")
                         .append("<div style='height:30px; background:#f7f7f7; padding-right:17px;'><table class='utable' ><thead fm-head></thead></table></div>")
                         .append("<div style='overflow-y: scroll;' ><table class=\"utable scroll_table table_hover table_ch_c\" ><tbody fm-body></tbody></table></div>");
            var $head = this.$element.children("div").eq(0)
            $head.addClass("row-fluid").addClass("layout-top").addClass("table_th").addClass("utable-hasbutton");
            var $center = this.$element.children("div").eq(1);
            $center.addClass("row-fluid").addClass("layout-center");
            var $body = $center.find("tbody[fm-body]").eq(0);
            return $body;
        },//模板
        getTemp: function () {
            var ops = this.options;
            var $this = this;
            var iconTemp = "<span class=\"line\" {{layer}} ></span><span class=\"button switch center\"></span>"
            if (ops.treeModel == "checkbox") {
                iconTemp += "<span  class=\"button chk checkbox_false_full\" ></span>";
            }
            iconTemp += "<span {{diyicon}} class=\"button {{icon2}}\"></span>";
            var temp = "<tr class='gtclose' >";
            var htemp = "<tr>";
            if (!ops.header) {
                alert("gridtree标头不可为空！");
                return false;
            }
            $.each(ops.header, function (i, n) {
                if (n) {
                    var html;
                    var istree = null;
                    try {
                        istree = n["tree"];
                    } catch (e) { }
                    if (istree) {
                        html = "<td>" + iconTemp + "<span class='text'>{{" + n.key + "}}</span></td>";
                    } else {
                        html = "<td>{{" + n.key + "}}" + (n.html ? n.html : "") + "</td>";
                    }
                    if (n.width) {
                        temp += $(html).css("width", n.width + ops.unit).get(0).outerHTML;
                        htemp += $("<th>" + n.name + "</th>").css("width", n.width + ops.unit).get(0).outerHTML;
                    } else {
                        temp += $(html).get(0).outerHTML;
                        htemp += $("<th>" + n.name + "</th>").get(0).outerHTML;
                    }
                }
            });
            temp += "</tr>";
            htemp += "</tr>";
            $this.$element.find("thead[fm-head]").append(htemp);
            return temp;
        },//加载数据
        loadData: function () {
            var ops = this.options,
            $this = this;
            if (ops.data != null) {
                $this.fillRoot(ops.data);
                if (ops.afterCreate) {
                    ops.afterCreate(ops.data);
                }
            } else if (ops.url != "") {
                $.getJSON(ops.url, { _r_t: (ops.cache ? 0 : Math.random()) }, function (data) {
                    var resd = data;
                    if (ops.source != "") {
                        var temp = data;
                        var item = ops.source.split('.');
                        $.each(item, function (i, n) {
                            if (n != "") {
                                temp = temp[n];
                            }
                        });
                        resd = temp;
                    }
                    $this.fillRoot(resd);
                    if (ops.afterCreate) {
                        ops.afterCreate(data);
                    }
                });
            }
        },//root数据
        fillRoot: function (data) {
            if (!data) return;
            var ops = this.options,
                $this = this;
            $.each(data, function (i, n) {
                n["tag_id"] = i;
                var child = n[ops.dataFormat.child];
                n["icon2"] = "ico";
                var html = $this.filltrRootData(n), d = n, count = 0, $tr, $chk;
                count = n["tag_id"].toString().split('-').length - 1;
                count = count == -1 ? 0 : count;
                if (ops.expand != 0) {
                    d = $this.copyobject(n);
                }
                var $html = $(html);
                $html.attr("pid", i);
                if (n[ops.dataFormat.selected]) {
                    $html.addClass("checked_tr");
                }
                $html.remove().appendTo($this.tempBox);
                $tr = $this.tempBox.find("tr:last");

                $chk = $tr.find(".chk");
                if ($chk && n[ops.dataFormat.checked]) {
                    $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                }
                ops.bindNode(n, $tr);
                $this.fillChild(child, $tr, i);
                d[ops.dataFormat.child] = null;
                $tr.data("rowdata", d);
                $html.removeClass("gtclose").addClass("gtopen");
            });
        },
        //子数据
        fillChild: function (data, element, pid) {
            if (!data) return;
            var ops = this.options,
                $this = this, $tr;
            for (var i = 0; i < data.length ; i++) {
                var n = data[i];
                n["tag_id"] = pid + "-" + i;
                var child = n[ops.dataFormat.child];
                n["icon2"] = (child && child.length > 0) ? "ico" : "docu";
                var html = $this.filltrData(n);
                var d = n;
                var count = 0;
                count = n["tag_id"].toString().split('-').length - 1;
                count = count == -1 ? 0 : count;
                var $html = $(html);
                $html.attr("pid", pid + "-" + i);
                if (n[ops.dataFormat.selected]) {
                    $html.addClass("checked_tr");
                }
                $tr = $html;
                var $last = element.nextAll("[pid^=" + pid + "]:last");
                if ($last.get(0)) {
                    $last.after($html);
                } else {
                    element.after($html);
                }
                var $docu = $tr.find(".switch").eq(0), $chk = $tr.find(".chk").eq(0);
                if (n["icon2"] == "docu" && !$docu.hasClass("docu")) {
                    $docu.addClass("docu");
                }
                if (n["icon2"] == "docu") {
                    $html.removeClass("gtclose").addClass("gtopen");
                }
                if ($chk && n[ops.dataFormat.checked]) {
                    $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                }
                ops.bindNode(n, $tr);
                $this.fillChild(child, $tr, n["tag_id"]);//
                $this.open($html);
                d[ops.dataFormat.child] = null;
                $tr.data("rowdata", d);
            }
        },//数据
        fillNode: function (element, d, chk) {
            return false;
            var ops = this.options,
                $this = this, pid = d.tag_id, data = d[$this.options.dataFormat.child];
            element.data("rowdata", $this.copyobject(d));
            element.append("<tr ></tr>");
            var $ul = element.next("tr").eq(0);
            for (var i = 0; i < data.length ; i++) {
                var n = data[i];
                n["tag_id"] = pid + "-" + i;
                var child = n[ops.dataFormat.child];
                n["icon2"] = (child && child.length > 0) || (n[ops.dataFormat.open]) ? "ico" : "docu";
                var html = $this.filltrData(n);
                var d = n;
                html = '<li>' + html + '</li>';
                var $html = $(html);
                if (chk && ops.multiple) {
                    $html.find(".chk").addClass("checkbox_true_full_focus");
                }
                $ul.append($html);
                var $li = $ul.children("li:last"), $chk;
                var $docu = $li.find(".switch").eq(0);
                if (n["icon2"] == "docu" && !$docu.hasClass("docu")) {
                    $docu.addClass("docu");
                }
                $chk = $li.find(".chk").eq(0);
                if ($chk && n[ops.dataFormat.checked]) {
                    $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                }
                $li.data("rowdata", d);
            }
        },//数据
        fillAjaxNode: function (element, pd, d, chk) {
            return false;
            var ops = this.options,
                $this = this, pid = pd.tag_id, data = d;
            pd[$this.options.dataFormat.open] = false;
            element.data("rowdata", pd);
            element.append("<ul ></ul>");
            var $ul = element.children("ul").eq(0);
            for (var i = 0; i < data.length ; i++) {
                var n = data[i];
                n["tag_id"] = pid + "-" + i;
                var child = n[ops.dataFormat.child];
                n["icon2"] = (child && child.length > 0) || (n[ops.dataFormat.open]) ? "ico" : "docu";
                var html = $this.filltrData(n);
                var d = n;
                var $html = $("<li>" + html + "</li>");
                if (chk && ops.multiple) {
                    $html.find(".chk").addClass("checkbox_true_full_focus");
                }
                $ul.append($html);
                var $li = $ul.children("li:last"), $chk;
                var $docu = $li.find(".switch").eq(0);
                if (n["icon2"] == "docu" && !$docu.hasClass("docu")) {
                    $docu.addClass("docu");
                }
                $chk = $li.find(".chk").eq(0);
                if ($chk && n[ops.dataFormat.checked]) {
                    $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                }
                $li.data("rowdata", d);
            }
        },
        copyobject: function (data) {
            var obj = {};
            for (var i in data) {
                if (i.toString() != this.options.dataFormat.child)
                    obj[i] = data[i];
            }
            return obj;
        },//填充
        filltrRootData: function (data) {
            var ops = this.options, $this = this, count = 0;
            if (data) {
                var html = $this.template.fill(data)
                    .replaceo("layer", "style=''")
                    .replaceo("diyicon", ops.dataFormat.icon != null ? ((data[ops.dataFormat.icon] && data[ops.dataFormat.icon] != "") ? "style=\"background: url(" + data[ops.dataFormat.icon] + ") 0 0 no-repeat;\"" : "") : "")
                    .replaceo("selected", data[ops.dataFormat.selected] ? "fm-sel=\"selected\"" : "")
                    .replaceo("data-id", data.tag_id).fillEmpty();
                var cnodeicon = false;
                if (ops.clazz > 0 && ops.expand && ops.clazz <= (count + 1)) {
                    cnodeicon = true;
                }
                return html;
            } else {
                return "";
            }
        },//填充
        filltrData: function (data) {
            var ops = this.options, $this = this, count = 0;
            count = data["tag_id"].toString().split('-').length - 1;
            count = count == -1 ? 0 : count;
            if (data) {
                var html = $this.template.fill(data)
                    .replaceo("diyicon", ops.dataFormat.icon != null ? ((data[ops.dataFormat.icon] && data[ops.dataFormat.icon] != "") ? "style=\"background: url(" + data[ops.dataFormat.icon] + ") 0 0 no-repeat;\"" : "") : "")
                    .replaceo("selected", data[ops.dataFormat.selected] ? "fm-sel=\"selected\"" : "")
                    .replaceo("data-id", data.tag_id)
                    .replaceo("layer", "style='padding-left:" + (count * 18) + "px;'").fillEmpty();;
                var cnodeicon = false;
                if (ops.clazz > 0 && ops.expand && ops.clazz <= (count + 1)) {
                    cnodeicon = true;
                }
                return html;
            } else {
                return "";
            }
        },
        serialize: function (data, pid) {
            var items = new Array(),
                $this = this,
                ops = this.options;
            $.each(data, function (i, d) {
                var child = d[ops.dataFormat.child];
                var n = $this.copyobject(d);
                n["tag_id"] = pid ? pid + "-" + i : i;
                items.push(n);
                if (child && child.length > 0) {
                    var temp = $this.serialize(child, n["tag_id"]);
                    $.each(temp, function (j, x) {
                        items.push(x);
                    });
                }
                n["icon2"] = (child && child.length > 0 || !pid) ? "ico" : "docu";
            });
            return items;
        },//展开
        ckopen: function (element) {
            element.removeClass("gtclose").addClass("gtopen");
            var pid = element.attr("pid");
            var len = element.nextAll("[pid^=" + pid + "]").length;
            for (var i = 0; i < len; i++) {
                var cbean = element.nextAll("[pid=" + pid + "-" + i + "]");
                if (cbean.get(0)) {
                    cbean.show(200);
                    cbean.nextAll("[pid^=" + pid + "-" + i + "]").hide();
                    cbean.removeClass("gtopen").addClass("gtclose");
                } else {
                    break;
                }
            }

        },
        open: function (element) {
            element.removeClass("gtclose").addClass("gtopen");
            var pid = element.attr("pid");
            var len = element.nextAll("[pid^=" + pid + "-]").length;
            for (var i = 0; i < len; i++) {
                var cbean = element.nextAll("[pid=" + pid + "-" + i + "]");
                if (cbean.get(0)) {
                    cbean.show(200);
                } else {
                    break;
                }
            }

        },//折叠
        fold: function (element) {
            element.removeClass("gtopen").addClass("gtclose");
            var pid = element.attr("pid");
            element.nextAll("[pid^=" + pid + "-]").hide(200);
        },//查找
        findParent: function (element) {
            var items = element.prevAll("tr");
            var id = element.attr("id");
            var last = id.lastIndexOf("_");
            id = id.substring(0, last);
            for (var i = 0; i < items.length; i++) {
                if ($(items[i]).attr("id") == id) {
                    return $(items[i]);
                }
            }
            return element;
        },//选中
        checkChild: function (element) {
            var $cli = element.children("ul").children("li");
            $cli.each(function (i, n) {
                $(n).find(".chk").addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
            });
        },//不选
        noCheckChild: function (element) {
            var $cli = element.children("ul").children("li");
            $cli.each(function (i, n) {
                $(n).find(".chk").addClass("checkbox_false_full").removeClass("checkbox_true_full_focus");
            });
        },//选中
        selected: function (element) {
            var data = element.data("rowdata");
            var ops = this.options;
            var issel = true;
            if (ops.beforeSelect) {
                ops.beforeSelect(data, element);

            }
            if (element.hasClass("checked_tr")) {
                element.removeClass("checked_tr");
                issel = false;
            } else {
                if (!ops.multiple) {
                    this.$element.find("tr").removeClass("checked_tr");
                    element.addClass("checked_tr");
                } else {
                    element.addClass("checked_tr");
                }
                ops.selected(data);
            }
            if (ops.click) {
                if (issel) {
                    ops.click(data);
                } else {
                    ops.click(null);
                }

            }
        },//选中
        checked: function (element) {
            var li = element.parents("li").eq(0);
            var data = element.parents("li").data("rowdata");
            var ops = this.options;
            var bfs = true;
            if (ops.beforeCheck) {
                bfs = ops.beforeCheck(data, element);
                if (bfs == undefined) bfs = true;
            }
            if (bfs) {
                if (!this.options.multiple) {
                    this.$element.find(".checkbox_true_full_focus").addClass("checkbox_false_full")
                    .removeClass("checkbox_true_full_focus");
                }
                if (element.hasClass("checkbox_false_full")) {
                    element.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                    if (!this.options.multiple) {
                        return false;
                    }
                    this.checkChild(li);
                } else {
                    element.addClass("checkbox_false_full").removeClass("checkbox_true_full_focus");
                    this.noCheckChild(li);
                }
            }
        },//获取选中列表
        getCheckNodes: function () {
            var array = new Array();
            this.$element.find(".checkbox_true_full_focus").each(function (i, n) {
                var tr = $(n).parents("li").eq(0);
                array.push(tr.data("rowdata"));
            });
            return array;
        },//获取选中行列表
        getSelectNodes: function () {
            var array = new Array();
            this.$element.find(".selected").each(function (i, n) {
                array.push($(n).data("rowdata"));
            });
            return array;
        },//设置为选中行
        setSelected: function (element) {
            element.addClass("selected");
        },//设置为选中
        setChecked: function (element) {
            var $chk = $li.find(".chk").eq(0);
            if ($chk && n[ops.dataFormat.checked]) {
                $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
            }
        },
        reload: function () {
            this.$element.find(".fm-gtree").html("");
            this.loadData();
        }
    };

    //函数入口
    $.fn.GridTree = function (options) {
        var data = new GridTree(this, options)
        return data;
    }
    //默认值
    $.fn.GridTree.defaults = {
        url: "",
        data: null,
        expand: -1,//-1全部展开，0，不展开，num展开层数
        dataFormat: { child: "Child", checked: "Checked", selected: "Selected", icon: "Icon", open: "Open" },
        param: null,//附带参数['Id','Pid']
        source: "Data",
        cache: false,
        treeModel: "default",//默认模式
        multiple: false,//是否多项
        selected: function (data) { },
        checked: function (data) { },
        click: function (data) { },
        afterCreate: null,
        beforeSelect: function (node, element) { },
        beforeCheck: function (node, element) { },
        bindNode: function (node, element) { },
        unit: '%',
        header: null
    };
})(window.jQuery);
