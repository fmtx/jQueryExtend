/*!
* 自动完成控件
* 创建人：qinnailin
* 创建时间：2014/8/1
*
*
*
*/

(function ($) {
    $.fn.autoComplete = function (options) {
        var ops = $.extend({
            url: "",
            data: null,
            source: "",
            value: null,
            key: "id",
            text: "name",
            code: "code",
            maxSize: 10,
            width: "auto",
            exclude: null,
            tabEnable: true,
            change: function (obj) { },
            offset: null
        }, options || {});
        var objthis = $(this);
        var basebox = objthis.parent(".dropdown");
        if (!basebox)
            basebox = objthis.parent();
        var data;
        var oldval;
        objthis.parent().append("<ul class=\"typeahead dropdown-menu\"></ul>");
        $(this).attr("autocomplete", "off");
        if (ops.width == "auto") {
            $(".dropdown-menu").width(objthis.width());
        } else if (!isNaN(ops.width)) {
            $(".dropdown-menu").width(ops.width);
        }
        if (ops.offset != null) {
            objthis.next(".dropdown-menu").css({ top: ops.offset.Top + "px", left: ops.offset.Left + "px" });
        }

        if (ops.exclude) {
            var d = new Array();
            $.each(ops.data, function (i, n) {
                $.each(ops.exclude, function (j, m) {
                    if (m != n[ops.code]) {
                        d.push(n);
                    }
                });
            });
            ops.data = d;
        }

        function init() {
            var code = "";
            if (ops.value) {
                code = $("input[name=" + ops.value + "]").val();
            } else {
                code = objthis.next(":hidden").val();
            }
            if (code != "" && ops.data) {
                $.each(ops.data, function (i, n) {
                    if (code == n.code) {
                        objthis.val(n.value);
                    }
                });
                data = ops.data;
            }
        }

        objthis.keyup(function (e) {
            if (e.keyCode == 37 || e.keyCode == 39) return;
            var keyword = objthis.val();
            if (keyword == "") {
                if (ops.value) {
                    $("input[name=" + ops.value + "]").val("");
                } else {
                    objthis.next(":hidden").val("");
                    objthis.siblings("ul:first").html("");
                }
                objthis.siblings("ul:first").hide();
                objthis.siblings("ul:first li").remove();
                oldval = "";
                return;
            }
            if (oldval != keyword) {
                oldval = keyword;
            } else {
                return false;
            }
            if (ops.data) {
                var d = new Array();
                if (e.keyCode == 32 && $.trim(keyword) == "") {
                    data = ops.data;
                    loadData(ops.data);
                } else {
                    $.each(ops.data, function (i, n) {
                        var size = keyword.length;
                        var nd = n[ops.text];
                        var t = nd.substring(0, size);
                        if (t === keyword) {
                            d.push(n);
                        }
                    });
                    if (d.length == 0) {
                        $.each(ops.data, function (i, n) {
                            var size = keyword.length;
                            var nd = n[ops.key];
                            var t = nd.substring(0, size);
                            if (t === keyword) {
                                d.push(n);
                            }
                        });
                    }
                    data = d;
                    loadData(d);
                    if(data.length==0){
                        objthis.next(":hidden").val("")
                        objthis.val("");
                    }
                }
            } else if (ops.url != "") {
                var vld = "{" + objthis.attr("name") + ":'" + keyword + "'" + ",maxSize:" + ops.maxSize + "}";
                $.get(ops.url, stringToJSON(vld), function (datas) {
                    data = datas;
                    if (ops.source != "") {
                        var temp = datas;
                        var item = ops.source.split('.');
                        $.each(item, function (i, n) {
                            if (n != "") {
                                temp = temp[n];
                            }
                        });
                        data = temp;
                    }
                    loadData(data);
                });
            }
        });

        function loadData(data) {
            if (data && data.length > 0) {
                var html = "";
                var temp = "<li style='display:inline'><a data-id='{{" + ops.key + "}}' data-code='{{" + ops.code + "}}' href='javascript:' >{{" + ops.text + "}}</a></li>";
                for (var i = 0; i < data.length && i < 10; i++) {
                    var n = data[i];
                    html += temp.fill(n);
                }
                objthis.siblings("ul:first").html(html).show();
                if (ops.value) {
                    $("input[name=" + ops.value + "]").val("");
                } else {
                    objthis.next(":hidden").val("");
                }
            } else {
                objthis.siblings("ul:first").html("");
                objthis.siblings("ul:first").hide();
            }
        }

        $(document).ready(function () {
            init();
            basebox.on("click", "ul li a", function () {
                var name = $(this).attr("data-id");
                var id = $(this).attr("data-code");
                objthis.val(name);
                if (ops.value) {
                    $("input[name=" + ops.value + "]").val(id);
                } else {
                    objthis.next(":hidden").val(id);
                }
                objthis.siblings("ul:first").hide();
                objthis.focus();
                var row;
                if (data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i][ops.key] == id) {
                            row = data[i];
                            break;
                        }
                    }
                }

                oldval = "";
                ops.change(row);
                objthis.focus();
            });

            basebox.on("mousemove", "ul", function () {
                if (basebox.find(".active").get(0)) {
                    basebox.find(".active").removeClass("active");
                }
            });

            objthis.keyup(function (event) {
                if (event.keyCode == 40) {
                    if (basebox.find(".active").get(0)) {
                        basebox.find(".active").removeClass("active").next("li").addClass("active");
                        if (!basebox.find(".active").get(0)) {
                            basebox.find("li:first").addClass("active");
                        }
                    } else {
                        basebox.find("li:first").addClass("active");
                    }
                } else if (event.keyCode == 38) {
                    if (basebox.find(".active").get(0)) {
                        basebox.find(".active").removeClass("active").prev("li").addClass("active");
                        if (!basebox.find(".active").get(0)) {
                            basebox.find("li:last").addClass("active");
                        }
                    } else {
                        basebox.find("li:last").addClass("active");
                    }
                } else if (event.keyCode == 13) {
                    var row;
                    var objx
                    if (basebox.find(".active").get(0)) {
                        objx = basebox.find(".active>a").eq(0);
                    } else {
                        objx = basebox.find("li:first>a");
                    }
                    var name = $(objx).attr("data-id");
                    var id = $(objx).attr("data-code");
                    objthis.val(name);
                    if (ops.value) {
                        $("input[name=" + ops.value + "]").val(id);
                    } else {
                        objthis.next(":hidden").val(id);
                    }
                    objthis.siblings("ul:first").hide();
                    objthis.focus();
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i][ops.key] == id) {
                                row = data[i];
                                break;
                            }
                        }
                    }
                    oldval = "";
                    ops.change(row);
					if (ops.tabEnable && objthis.attr("tabIndex") != undefined) {
                        var form = basebox.parentsUntil("form");
                        var inputs = form.find("input:not([disabled]):not([readonly])");
                        var thistabindex = objthis.attr("tabIndex");
                        for (var i = 0; i < inputs.length; i++) {
                            var input = inputs[i];
                            var index = $(input).attr("tabIndex");
                            if (parseInt(index) > parseInt(thistabindex)) {
                                input.focus();
                                break;
                            }
                        }
                    }
                    objthis.siblings("ul:first").html("");
                    return false;
                }
            });
        });
    }
})(jQuery);