﻿

$(document).ready(function () {
    var data = [
        {
            name: '前端插件拓展', menus: [
            { name: '插件使用说明', url: "/Setup.html" },
            { name: 'basejs', url: "/basejs.html" },
            { name: 'gridjs', url: "/gridjs.html" },
            { name: 'gridTreejs', url: "/gridTreejs.html" },
            { name: 'UTree', url: "/UTree.html" },
            { name: 'form', url: "/form.html" },
            { name: 'modal', url: "/modal.html" },
            { name: 'autoComplete', url: "/autoComplete.html" },
            { name: 'treejs', url: "/treejs.html" },
            { name: 'selectbox', url: "/selectbox.html" },
            { name: 'select', url: "/select.html" },
            { name: 'Datepicker', url: "/Datepicker.html" },
            { name: 'chartsjq', url: "/chartsjq.html" },
            { name: 'jsUpload', url: "/jsUpload.html" },
            { name: 'swfupload', url: "/swfupload.html" },
            { name: 'Login', url: "/Login.html" },
            { name: 'Frame', url: "/Frame.html" },
            { name: 'Layout', url: "/Layout.html" },
            { name: 'uTable', url: "/uTable.html" },
            { name: 'cascade', url: "/cascade.html" },
            { name: 'tab', url: "/tab.html" },
            { name: 'Tooltip', url: "/Tooltip.html" },
            { name: 'splitter', url: "/splitter.html" },
            { name: 'Detail', url: "/Detail.html" },
            { name: 'UserTree', url: "/UserTree.html" }
        ]}
    ];
    
    fillmenu(data);
});

function fillmenu(data) {
    var temp = "<li class='dropdown'>" +
             "<a class='dropdown-toggle' data-toggle='dropdown' href='#'>{{name}}<b class='caret'></b></a>" +
             "<ul class='dropdown-menu'>{{body}}</ul></li>";
    
    var html = "";
    $.each(data, function (i, n) {
        html += temp.replaceo("body", childfillmenu(n.menus)).fill(n);
    });
    $(".nav-collapse>.nav").html(html);
}

function childfillmenu(data) {
    var templi = "<li><a href='{{url}}'>{{name}}</a></li>";
    var html = "";
    
    $.each(data, function (i, n) {
        html += templi.fill(n);
    });
    return html;
}