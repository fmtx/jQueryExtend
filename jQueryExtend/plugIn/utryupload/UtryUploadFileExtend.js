﻿(function ($) {
    $.fn.UtryUploadFile = function (ctr, sObj,refresh) {
        var func = {
            isRefresh:refresh,
            CtrUpload: ctr,
            Files: null,
            ShowedObj: sObj,
            //获取所需的被选择文件数据
            GetFileNames: function (strFiles) {
                if (func.isRefresh)
                    func.ClearAll();
                if (strFiles != "")
                    func.Files = eval("(" + strFiles + ")");
                else
                    func.Files = null;
                func.ShowSelectedFiles(func.Files);
            },
            //删除被选择的文件（删除flash里的）
            DoDeleted: function (ids) {
                var fus = $("#" + func.CtrUpload).get(0);
                if (!fus.DoDeleteFunction) {
                    fus = $("#" + func.CtrUpload + "2").get(0);
                }
                fus.DoDeleteFunction(ids);
            },
            //初始化列表
            InitShowFile: function (divObj) {
             
              
            },
            //显示已经被选的文件
            ShowSelectedFiles: function (files) {
                if (func.Files != null) {
                    for (var fileInfo in func.Files) {
                        var newFile = func.Files[fileInfo];
                        func.CreatNewFileShow(newFile);
                    }
                }

            },
            //根据文件信息创建显示
            CreatNewFileShow: function (newFile) {
               
                var ulFiles = $("#" + func.ShowedObj );
                var ilFileInfo = $("<div id='" + newFile.FileID + "' title='" +
                           newFile.FileName + "' class='bar_bg' />");
                ulFiles.append(ilFileInfo);
                var fileDiv = $("<div id='fileInfoDiv' class='bar_file' ></div>");
               
                var fileName = $("<span id='fileName' title='" + newFile.FileName.su +
                   "' >" + newFile.FileName + "</span>");
                fileDiv.append(fileName);
                var pathId = $("<input id ='pathId' type='hidden' value=''  />");
                fileDiv.append(pathId)
                pathId.val(newFile.resultID)
                ilFileInfo.append(fileDiv);

                var img = $("<div class='bar_btn' title='删除' />");
                var a = $("<a href='javascript:void(0)'>[删除]</a>");
                a.click(function (e) {
                    var cImg = e.currentTarget;
                    var li = $(cImg.parentElement.parentElement);
                    var id = li.attr("id");
                    func.FileDeleted(id);
                    var f = $("#" + func.ShowedObj + " #" + id);
                    if (f != null) {
                        f.remove();
                    }
                    func.DoDeleted(id);
                    return false;
                });
                img.append(a);
                ilFileInfo.append(img);
               
                var persent = newFile.FileSize == 0 ? 100 : Math.round(newFile.FileUploded * 100 / newFile.FileSize);
               

                var progressBg = $("<div id='progressBg' class='bar' title='" + persent + "%'></div>");
                ilFileInfo.append(progressBg);
               
                var totleWidth = progressBg.width();
                var width = Math.round(persent * totleWidth / 100);
                var progressBar = $("<div id='progress' class='bar_out'>&nbsp;</div>");
                progressBar.css("width", width + "px");
                progressBg.append(progressBar);
                //var progressText = $("<span id='progressText'>" + persent + "%</span>");
                //ilFileInfo.append(progressText);
            },
            //显示文件上传进度进行
            ShowProgress: function (id, uploadSize) {
                var fileDiv = $("#" + func.ShowedObj + " #" + id);
                var file = null;
                if (func.Files != null) {
                    for (var fileInfo in func.Files) {
                        if (func.Files[fileInfo].FileID == id) {
                            file = func.Files[fileInfo];
                            break;
                        }
                    }
                }

                if (file != null && fileDiv != null) {

                    var persent = file.FileSize == 0 ? 100 : Math.round(uploadSize * 100 / file.FileSize);
                    
                    var bg = fileDiv.find("#progressBg");
                    bg.attr("title", persent + "%");
                    var prg = fileDiv.find("#progress");
                    var totalWidth = bg.width();

                    var width = Math.round(persent * totalWidth / 100);
                    prg.css("width", width + "px")
                    var prgText = fileDiv.find("#progressText");
                    prgText.html(persent + "%");
                    file.FileUploded = uploadSize;
                }
            },

            FileComplit: function (id, pathNamr) {
                var fileDiv = $("#" + func.ShowedObj + " #" + id);
                var file = null;
                if (func.Files != null) {
                    for (var fileInfo in func.Files) {
                        if (func.Files[fileInfo].FileID == id) {
                            file = func.Files[fileInfo];
                            break;
                        }
                    }
                }

                if (file != null && fileDiv != null) {
					file.resultID = pathNamr;
                    var path = fileDiv.find("#pathId");
                    path.val(pathNamr);
                }
            },
            //清空文件数据
            ClearAll: function () {
                var ulFiles = $("#" + func.ShowedObj ).find("div");
                ulFiles.remove();
                func.Files = null;

            },

            //删除被选的文件（单个）
            FileDeleted: function (id) {
                if (func.Files != null) {
                    for (var fileInfo in func.Files) {
                        if (func.Files[fileInfo] == id) {
                            func.Files.splice(fileInfo, 1);
                            break;
                        }
                    }
                }

            },

            //显示错误信息
            AlertError: function (strError) {
                if (strError != null)
                    alert(strError);
            },
            //执行上传操作
            DoUploaded: function () {

                var fus = $("#" + func.CtrUpload).get(0);
                if (!fus.DoUploadFunction) {
                    fus = $("#" + func.CtrUpload + "2").get(0);
                }
                fus.DoUploadFunction();
            },

            //执行清除文件
            DoClear: function () {

                var fus = $("#" + func.CtrUpload).get(0);
                if (!fus.DoFilesClearFunction) {
                    fus = $("#" + func.CtrUpload + "2").get(0);
                }
                fus.DoFilesClearFunction();
            },
            //执行取消上传
            DoDeUploading: function () {
                var fus = $("#" + func.CtrUpload).get(0);
                if (!fus.DoFileDeUploadingFunction) {
                    fus = $("#" + func.CtrUpload + "2").get(0);
                }
                fus.DoFileDeUploadingFunction();
            }
        }
        func.InitShowFile(sObj);
        
        return func;
    }
})(jQuery);

