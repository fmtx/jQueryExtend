﻿/*!
 *上传控件文件选择后展示文件信息及进度信息
 *创建人：孔黎明 
 * 创建时间：2014/4/11 
 * 
 */

var UtryUploadFile = {
    CtrUpload: "FileUploadShow",
    CtrUpload2: "FileUploadShow2",
    Files: null,
    ShowedObj: "",
    isShowFiles: true,//是否显示文件列表
    /**
     * 获取被选择文件的数据
     * @method GetFileNames
     * @for UtryUploadFile
     * @param {字符串} strFiles 从flash中传出来的被选文件信息
     *
     */
    GetFileNames: function (strFiles) { 
        
        if (strFiles != "")
            UtryUploadFile.Files = eval("(" + strFiles + ")");
        else
            UtryUploadFile.Files = null;
        // debugger;
        if (UtryUploadFile.isShowFiles)
            UtryUploadFile.ShowSelectedFiles(UtryUploadFile.Files);
    },
    
   
    /**
    * 移除被选择的文件（移除flash里的）
    * @method DoDeleted
    * @for UtryUploadFile
    * @param {字符串} ids 需要移除的文件id
    * 
    */
    DoDeleted: function (ids) {
        var fus = $("#" + UtryUploadFile.CtrUpload).get(0);
        if (!fus.DoDeleteFunction) {
            fus = $("#" + UtryUploadFile.CtrUpload2).get(0);
        }
        fus.DoDeleteFunction(ids);
    },
    
    /**
    *初始化列表
    * @method InitShowFile
    * @for UtryUploadFile
    * @param {字符串} divObj 列表所在容器的id
    * @param {bool} isShowFiles 是否显示文件列表
    */
    InitShowFile: function (divObj, isShowFiles) {
        UtryUploadFile.isShowFiles = isShowFiles;
        if (UtryUploadFile.isShowFiles) {
            var ulFiles = $("<ul id='files' />");
            $("#" + divObj).append(ulFiles);
        }
    },
 

   /**
   *显示已经被选的文件
   * @method ShowSelectedFiles
   * @for UtryUploadFile
   * @param {数组} files 文件信息列表
   * 
   */
    ShowSelectedFiles: function (files) {
        var ulFiles = $("#files").find("li");
        ulFiles.remove();
        if (UtryUploadFile.Files != null) {
            for (var fileInfo in UtryUploadFile.Files) {
                var newFile = UtryUploadFile.Files[fileInfo];
                UtryUploadFile.CreatNewFileShow(newFile);
            }
        }

    },
   
    /**
   *根据文件信息创建显示
   * @method CreatNewFileShow
   * @for UtryUploadFile
   * @param {文件对象} newFile 文件信息
   * 
   */
    CreatNewFileShow: function (newFile) {
        var ulFiles = $("#files");
        var ilFileInfo = $("<li id='" + newFile.FileID + "' title='" +
                   newFile.FileName + "' />");
        ulFiles.append(ilFileInfo);
        var fileDiv = $("<div id='fileInfoDiv'></div>");
        ilFileInfo.append(fileDiv);

        var fileProgress = $("<div id='fileProgDiv'></div>");
        ilFileInfo.append(fileProgress);
        var fileName = $("<span id='fileName' title='" + newFile.FileName +
            "' >" + newFile.FileName + "</span>");
        fileDiv.append(fileName);
        var img = $("<input type='button' value='' title='删除' />");
        img.click(function (e) {

            var cImg = e.currentTarget;
            var li = $(cImg.parentElement.parentElement);
            var id = li.attr("id");
            UtryUploadFile.FileDeleted(id);
            var f = $("#files #" + id);
            if (f != null) {
                f.remove();
            }
            UtryUploadFile.DoDeleted(id);
        });
        fileDiv.append(img);
        var persent = newFile.FileSize == 0 ? 100 : Math.round(newFile.FileUploded * 100 / newFile.FileSize);
        var width = Math.round(persent * 52 / 100);
        var progressBg = $("<div id='progressBg' title='" + persent + "%'></div>");
        fileProgress.append(progressBg);
        var progressBar = $("<div id='progress'></div>");
        progressBar.css("width", width + "px");
        progressBg.append(progressBar);
        var progressText = $("<span id='progressText'>" + persent + "%</span>");
        progressBg.append(progressText);
    },
    /**
    *显示文件上传进度进行
    * @method ShowProgress
    * @for UtryUploadFile
    * @param {字符串} id 文件ID
    * @param {整数} uploadSize 已上传的字节数
    */
    ShowProgress: function (id, uploadSize) {
        if (UtryUploadFile.isShowFiles) {
            var fileDiv = $("#files #" + id);
            var file = null;
            if (UtryUploadFile.Files != null) {
                for (var fileInfo in UtryUploadFile.Files) {
                    if (UtryUploadFile.Files[fileInfo].FileID == id) {
                        file = UtryUploadFile.Files[fileInfo];
                        break;
                    }
                }
            }

            if (file != null && fileDiv != null) {

                var persent = file.FileSize == 0 ? 100 : Math.round(uploadSize * 100 / file.FileSize);
                var width = Math.round(persent * 52 / 100);
                var bg = fileDiv.find("#progressBg");
                bg.attr("title", persent + "%");
                var prg = fileDiv.find("#progress");
                prg.css("width", width + "px")
                var prgText = fileDiv.find("#progressText");
                prgText.html(persent + "%");
                file.FileUploded = uploadSize;
            }
        }
    },

    /**
    *清空文件列表显示数据
    * @method ClearAll
    * @for UtryUploadFile
    */
    ClearAll: function () {
        if (UtryUploadFile.isShowFiles) {
            var ulFiles = $("#files").find("li");
            ulFiles.remove();
            UtryUploadFile.Files = null;
        }
    },

    /**
    *在JS的Files文件集合中移除被选的文件（单个）
    * @method FileDeleted
    * @for UtryUploadFile
    * @param {字符串} id 需删除的文件ID
    */
    FileDeleted: function (id) {
        if (UtryUploadFile.Files != null) {
            for (var fileInfo in UtryUploadFile.Files) {
                if (UtryUploadFile.Files[fileInfo] == id) {
                    UtryUploadFile.Files.splice(fileInfo, 1);
                    break;
                }
            }
        }

    },

    /**
    *显示提示或错误信息（用于显示flash中的错误）
    * @method AlertError
    * @for UtryUploadFile
    * @param {字符串} 错误或提示信息
    */
    AlertError: function (strError) {
        if (strError != null)
            alert(strError);
    },
   
    /**
    *执行上传操作（调用flash中的方法）
    * @method DoUploaded
    * @for UtryUploadFile
    */
    DoUploaded: function () {

        var fus = $("#" + UtryUploadFile.CtrUpload).get(0);
        if (!fus.DoUploadFunction) {
            fus = $("#" + UtryUploadFile.CtrUpload2).get(0);
        }
        fus.DoUploadFunction();
    },
    /***
      *下载
    ***/
    doDownLoad:function(path){
             debugger
            //$.post("http://localhost:8089/fileUploadDemo/servlet/FileDownLoadServlet",{},function(result){
            //   debugger
           // }); 
          var tempForm = $("<form></form>");
          tempForm.attr('action',path+"/servlet/FileDownLoadServlet");
          tempForm.attr('method','post');
          tempForm.attr('id','tempDownLoadId');
          tempForm.appendTo("body");
          tempForm.css('display','none')
          tempForm.submit(); 
          
           
                       
    },
    /**
    *执行空文件（调用flash中的方法）
    * @method DoUploaded
    * @for UtryUploadFile
    */
    DoClear: function () {

        var fus = $("#" + UtryUploadFile.CtrUpload).get(0);
        if (!fus.DoFilesClearFunction) {
            fus = $("#" + UtryUploadFile.CtrUpload2).get(0);
        }
        fus.DoFilesClearFunction();
    },

    /**
    *执行取消上传（调用flash中的方法）
    * @method DoUploaded
    * @for UtryUploadFile
    */
    DoDeUploading: function () {
        var fus = $("#" + UtryUploadFile.CtrUpload).get(0);
        if (!fus.DoFileDeUploadingFunction) {
            fus = $("#" + UtryUploadFile.CtrUpload2).get(0);
        }
        fus.DoFileDeUploadingFunction();
    }
};