﻿
var UTRYUpload;


if (UTRYUpload == undefined) {
    UTRYUpload = function () {
    };
}

UTRYUpload.GetFileNames = function (objid,strFiles) {
    $("#" + objid).getfiles(strFiles);
};

UTRYUpload.subUploadComplete = function () {
    console.debug("完成");
};

UTRYUpload.ShowProgress = function (id, uploadSize) {
    console.debug(id);
    console.debug(uploadSize);
};


(function ($) {
    $.fn.extend({
        utryUpload: function (opstions) {
           var ops= $.extend({
               url: "/data/uploadswf.aspx",
                flashUrl: "FileUploadShow.swf",
                postParams: {},
                upbuttonText: "上传",
                multiple: true,
                fileUploadProgress: function () { },
                fileUploadSuccess: function (file, data) { },
                fileUploadComplete: function () { },
                fileUploadError: function () { },
                uploadStart: function () { },
                uploadProgress: function () { },
                uploadComplete: function () { }
            }, opstions || {});
            var objthis = $(this);
            var objectid = "jquery-ui-upload-flash-button-" + parseInt((Math.random() * 1000));
            var objectid2 = "jquery-ui-upload-flash-button2-" + parseInt((Math.random() * 1000));
            createuploadbox();
            //
            function objectFlash() {
                var temp = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' class='UTRYUpload' " +
                "id='" + objectid + "' width='80' height='28'codebase='http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab'>" +
                "<param name='movie' value='" + ops.flashUrl + "' /><param name='quality' value='high' />" +
                "<param name='wmode' value='transparent' /><param name='allowScriptAccess' value='sameDomain' />" +
                "<param name='FlashVars' value='&OEID=" + $(objthis).attr("id") + "&completeFunction=UTRYUpload.uploadComplete()&ObjName=UTRYUpload&fileTypes=*.*%7c*.*&fileTypeDescription=ALL%7cxml&ShowBtnInFlash=true&singleUpload=true&uploadPage=" + ops.url + "&totalUploadSize=25000000000&fileSizeLimit=10000000000' />" +
                "<embed id='" + objectid2 + "' src='FileUploadShow.swf' quality='high' wmode='transparent' class='UTRYUpload'" +
                "width='80' height='28' name='FileUploadShow' align='middle'"+
                "flashvars='&OEID=" + $(objthis).attr("id") + "&completeFunction=UTRYUpload.subUploadComplete()&ObjName=UTRYUpload&fileTypes=*.*%7c*.*&fileTypeDescription=ALL%7cxml&ShowBtnInFlash=true&singleUpload=true&uploadPage=" + ops.url + "&totalUploadSize=25000000000&fileSizeLimit=10000000000'" +
                "play='true'"+
                "loop='false'"+
                "quality='high'"+
                "allowscriptaccess='sameDomain'"+
                "type='application/x-shockwave-flash'"+
                "pluginspage='http://www.adobe.com/go/getflashplayer'></embed>"+
                "</object>";
                return temp;
            }
            //
            function createuploadbox() {
                var temp = "<div class='fm-upload-flash-box' style='width: 80px;float: left;margin-right: 5px;'>" +
                    objectFlash() + "</div><button class='btn static-upload'>" + ops.upbuttonText + "</button><ul class='qq-upload-list'></ul>";
                $(objthis).append(temp);
            }
            //绑定上传按钮
            $(objthis).on("click", ".static-upload", function () {
                var fus = $("#" + objectid).get(0);
                if (!fus.DoUploadFunction) {
                    fus = $("#" + objectid2).get(0);
                }
                fus.DoUploadFunction();
            });
            //绑定删除按钮
            $(objthis).on("click", ".qq-upload-cancel", function () {
                var id = $(this).attr("data-id");
                var removeFile = self.getFile(id);
                self.removeFromQueue(removeFile);
                swfu.cancelUpload(id);
                $("#li_" + id).remove();
            });

            var self = new UTRYUpload();

        },
        getfiles: function (strfile) {
            var objthis = $(this);
            var files = stringToJSON(strfile);
            $(objthis).find(".qq-upload-list").html("");
            $.each(files, function (i, file) {
                var temp = "<li id='li_" + file.FileID + "' >" +
            "<span class='qq-upload-file'>" + file.FileName + "</span>" +
            "<span class='qq-upload-size' style='display: inline;'>" + self.convertSize(file.FileSize) + "</span>" +
            "<a class='qq-upload-cancel' data-id='" + file.FileID + "' href='javascript:' >[删除]</a>" +
            "<div class='qq-progress-bar-box'>" +
            "<span class='qq-progress-bar' style='width: 0%;'></span>" +
            "</div></li>";
                $(objthis).find(".qq-upload-list").append(temp);
            });
            
        }
    });
    var self = {
        //文件类型转化
        concatTypes: function (types) {
            if (typeof types == 'string') {
                return types;
            } else {
                var typesCont = '';
                $.each(types, function (i, value) {
                    typesCont += '*.' + value + '; ';
                });
                return typesCont.substring(0, typesCont.length - 2);
            }
        },
        //文件大小转化
        convertSize: function (n) {
            var s = ['B', 'KB', 'MB', 'GB'];
            if (!Array.indexOf) {
                Array.prototype.indexOf = function (obj) {
                    for (var i = 0; i < this.length; i++) {
                        if (this[i] == obj) {
                            return i;
                        }
                    }
                    return -1;
                }
            }
            if (typeof n == 'string') {
                var sizeArray = n.split(' ');
                var indexUnit = s.indexOf(sizeArray[1]);
                return Math.round(sizeArray[0] * (Math.pow(1024, indexUnit)));

            } else {
                if (n) {
                    var e = Math.floor(Math.log(n) / Math.log(1024));
                    var converted = (n / Math.pow(1024, Math.floor(e))).toFixed(2);
                    return Math.ceil(converted, 2) + ' ' + s[e];
                }
                else {
                    return 0;
                }
            }


        },
        //文件截取
        fileSplit: function (word, len) {
            var trunc = word;
            m = trunc.match(/([^\/\\]+)\.(\w+)$/);
            if (m[1].length > len) {
                trunc = m[1].substring(0, len);
                trunc = trunc.replace(/w+$/, '');
                trunc += '(...)';
                return trunc + '.' + m[2];
            }
            return trunc;

        },
        //获取文件类型
        getFileType: function (file) {
            if (file.type == "") {
                return file.name.match(/([^\/\\]+)\.(\w+)$/)[2];
            } else {
                return file.type.toLowerCase();
            }
        },
        //获取文件
        getFile: function (id) {
            var newFile = swfu.getFile(id);
            var fileType = this.getFileType(newFile);
            newFile.type = fileType;
            return newFile;
        },
        //过滤文件
        queueFiles: function (file) {
            var self = this;
            var rules = ops.allowedExtensions;
            var hasfile = false;
            $.each(rules, function (j, value) {
                if (file.type == "." + value) {
                    hasfile = true;
                }
            });
            if (!hasfile) {
                alert(file.name + " 文件类型不允许！");
                swfu.cancelUpload(file.id);
            } else {
                if (ops.maxsize != 0 && file.size > ops.maxsize) {
                    alert(file.name + " " + self.convertSize(file.size) + " 文件过大！");
                    swfu.cancelUpload(file.id);
                } else if (ops.minsize != 0 && file.size < ops.minsize) {
                    alert(file.name + " " + self.convertSize(file.size) + " 文件过小！");
                    swfu.cancelUpload(file.id);
                } else {
                    self.files.push(file);
                    addlitoitemlist(file);
                    self.progress.total += file.size;
                }
            }
            //self.updateCounter();
        },
        //移除文件
        removeFromQueue: function (file) {
            var index = -1;
            $.each(this.files, function (i, obj) {
                if (obj.id == file.id) {
                    index = i;
                }
            });

            this.files.splice(index, 1);
        },
        //进度条
        progress: {
            file: null,
            fileSize: 0,
            current: 0,
            percent: 0,
            total: 0
        },
        files: [],
        fileData: [],
        medias: {},
        startUpload: function (file) {
            swfu.startUpload();
        }
    };
})(jQuery);