﻿/*!
 * Tooltip 仿bootstrap-tooltip
 * 创建人：qinnailin
 * 创建时间：2014/11/18
 *
 *
 * 
 */


!(function ($) {
    "use strict"
    //Tooltip对象
    var Tooltip = function (element, options) {
        this.init("Tooltip", element, options);
    }
    //Tooltip属性
    Tooltip.prototype = {
        constructor: Tooltip,
        init: function (type,element,options) {
            this.type = type;
            this.$element = $(element);
            this.options = this.getOptions(options);
            this.setContent();
            var eventIn = this.options.trigger == 'hover' ? 'mouseenter' : 'focus'
            var eventOut = this.options.trigger == 'hover' ? 'mouseleave' : 'blur'
            this.$element.on(eventIn, $.proxy(this.enter, this));
            this.$element.on(eventOut, $.proxy(this.leave, this));

        },
        enter: function (e) {
            var self = $(e.currentTarget)[this.type](this.options).data(this.type);
            self.show();
        },
        leave:function(e){
            var self = $(e.currentTarget)[this.type](this.options).data(this.type);
            self.hide();
        },
        getOptions:function(options){
            options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data());
            return options;
        },
        show: function () {
            var $tip, pos, tp, placement = this.options.placement, actualWidth, actualHeight;
            $tip = this.tip();
            $tip.remove().css({ top: 0, left: 0, display: 'block' }).appendTo(document.body);
            var title = this.getTitle();
            if (title == "") return false;
            $tip.find(".tooltip-inner").html(title);
            pos = this.getPosition();
            actualWidth = $tip[0].offsetWidth;
            actualHeight = $tip[0].offsetHeight;
            switch (placement) {
                case "top":
                    tp = { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth/2 };
                    break;
                case "left":
                    tp = { top: pos.top - actualHeight/2+pos.height/2, left: pos.left - actualWidth};
                    break;
                case "right":
                    tp = { top: pos.top -actualHeight / 2 + pos.height / 2, left: pos.left + pos.width   };
                    break;
                case "bottom":
                    tp = { top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2 };
                    break;
                default:
            }
            $tip.addClass("fade").addClass(placement).addClass("in").css(tp);
        },
        hide: function () {
            $(".tooltip").remove();
        },
        getPosition: function () {
            return $.extend({}, this.$element.offset(), {
                width: this.$element[0].offsetWidth,
                height: this.$element[0].offsetHeight
            });
        },
        setContent: function () {
            var $e = this.$element;
            $e.attr("data-title", $e.attr("title")).removeAttr("title");
        },
        tip: function () {
            return $(this.options.template);
        },
        getTitle: function () {
            var $e = this.$element;
            return $e.attr("data-title");
        }
    };
    //入口
    $.fn.Tooltip = function (options) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("Tooltip");
            if (!data) $this.data("Tooltip", (data = new Tooltip(this, options)));
            //if (typeof options == 'string') data[options];
            if (typeof options == 'string' && options!="") {
                data.options.placement = options;
            }
        });
    }
    //默认值
    $.fn.Tooltip.defaults = {
        placement: 'top',
        trigger:'hover',
        template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
    }
})(window.jQuery);