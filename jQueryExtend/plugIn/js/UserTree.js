﻿/*!
 * 用户数控件
 *创建人：qinnailin
 * 创建时间：2016/2/29
 */

!(function ($) {
    "use strict"
    //UserTree对象
    var UserTree = function (element, options) {
        this.init("UserTree", element, options);
    }
    //UserTree属性
    UserTree.prototype = {
        constructor: UserTree,
        Tree: null,
        init: function (type, element, options) {
            this.type = type;
            this.$element = $(element);
            this.options = this.getOptions(options);
            var objthis = this;
            this.$element.attr("autocomplete", "off");
            this.$element.attr("readonly", "readonly");
            var ops = this.options;
            if (!this.$element.hasClass("seluser")) {
                this.$element.addClass("seluser");
            }
            if (this.options.hiddenId == null) {
                alert("请提供结果容器！hiddenId");
                return;
            }
            this.$element.click(function () {
                var ps = ops.param;
                var reqdata = null;
                if (ps) {
                    var jsonstr = "";
                    var str = new Array();
                    $.each(ps, function (i,n) {
                        str.push("\""+n.p+"\"" + ":\"" + $("#" + n.id).val()+"\"");
                    });
                    jsonstr = "{" + str.join(',') + "}";
                    reqdata = jQuery.parseJSON(jsonstr);
                }
                $.get(ops.url,reqdata, function (d) {
                    objthis.showTree(d);
                });
            });
            if (this.options.checkedMulti) {
                var ids = $("#" + this.options.hiddenId).val();
                if (ids != ""&&this.options.showName==false) {
                    var arr = ids.split(",");
                    this.$element.val("已选择了" + arr.length + "项");
                }
            } 
        },
        showTree: function (tdata) {
            var ops = this.options;
            var objthis = this;
            var element = this.$element;
            var setting = $.extend(true, {
                //async: {
                //    enable: false,
                //    url: ops.url
                //},
                data: {
                    Nodes: tdata,
                    key: {
                        name: "Name"
                    },
                    simpleData: {
                        idKey: "ID"
                    }
                },
                check: {
                    enable: true,
                    chkboxType: ops.chkboxType,
                    chkParent: ops.chkParent
                },
                view: {
                    checkedMulti: ops.checkedMulti,
                    expand: ops.expand
                },
                callback: {
                    afterCreate: function () {
                        var ids = $("#" + ops.hiddenId).val();
                        if (ids) {
                            objthis.Tree.setSelected($("#" + ops.hiddenId).val());
                        }
                        objthis.afterCreate();
                    }
                }
            }, ops.tree);
            this.setting = setting;
            if (!$("#user-tree-fmtx-control-template").get(0)) {
                $("body").append(ops.template);
            }
            $("#user-tree-fmtx-control-template").modal({
                width:ops.width,
                title: ops.title,
                buttons: [
                    {
                        text: "确定", click: function () {
                            var nodes = objthis.Tree.getCheckedNodes();
                            var arr = new Array();
                            var namearr = new Array();
                            if (ops.checkedMulti) {
                                if (nodes) {
                                    for (var i in nodes) {
                                        var sel = true;
                                        if (ops.onlyChild && nodes[i].isParent) {
                                            sel=false;
                                        }
                                        var n = nodes[i][setting.data.simpleData.idKey];
                                        var m = nodes[i][setting.data.key.name];
                                        if (n && sel) {
                                            arr.push(n);
                                        }
                                        if (m && sel)
                                        {
                                            namearr.push(m);
                                        }
                                    }
                                }
                            } else {
                                if (nodes&&nodes.length>0) {
                                    var n = nodes[0][setting.data.simpleData.idKey];
                                    arr.push(n);
                                } else {
                                    element.val("");
                                    $("#" + ops.hiddenId).val("");
                                }
                            }
                            $("#" + ops.hiddenId).val(arr.join(","));
                            if (ops.checkedMulti) {
                                if (ops.showName) {
                                    element.val(namearr.join(","));
                                } else {
                                    element.val("已选择了" + arr.length + "项");
                                }
                            } else if (nodes.length > 0) {
                                element.val(nodes[0][setting.data.key.name]);
                            } else {
                                element.val("");
                                $("#" + ops.hiddenId).val("");
                            }
                            $(this).close();
                            if (ops.selected)
                            {
                                ops.selected(nodes);
                            }
                        }
                    },
                    { text: "关闭", click: function () { $(this).close(); } }
                ],
                afterCreate: function () {
                    if (!ops.showSearch) {
                        $("#user-tree-fmtx-control-search").hide();
                    } else {
                        $("#user-tree-fmtx-control-search").show();
                    }
                    objthis.Tree = $("#user-tree-fmtx-control").uTree(setting);
                }
            });
        },
        getOptions: function (options) {
            options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data());
            return options;
        },
        afterCreate: function () {
            var objthis = this;
            var setting = this.setting;
            var ops = this.options;
            $("#btnutreefind").click(function () {
                fineName();
            });
            $("#txtutreefindName").keyup(function (k) {
                if (k.keyCode == 13)
                {
                    fineName();
                }
            });
            function fineName() {
                objthis.Tree.destroy();
                var name = $("#txtutreefindName").val();
                $.get(ops.url, { name: name }, function (d) {
                    setting.data.Nodes = d;
                    objthis.Tree = $("#user-tree-fmtx-control").uTree(setting);
                });
            }
        }
    };

    

    //入口
    $.fn.UserTree = function (options) {
        return this.each(function () {
            var data = new UserTree(this, options);
        });
    }
    //默认值
    $.fn.UserTree.defaults = {
        title:"人力树",
        template: '<script type="text/template" id="user-tree-fmtx-control-template" ><div id="user-tree-fmtx-control-search" ><input id="txtutreefindName" style="width:80%" /><a href="javascript:" id="btnutreefind" ><i class="icon-search" ></i></a></div><div style="height:400px;overflow:auto;min-width:250px"><ul id="user-tree-fmtx-control"></ul></div></script>',
        hiddenId: null,
        param: null,
        checkedMulti: false,//是否多选
        showName:true,
        url: "/SystemUser/All",
        chkParent: false,
        onlyChild:true,
        chkboxType: { "Y": "", "N": "" },
        expand: 1,
        showSearch: true,
        selected:null
    }
})(window.jQuery);
