﻿/*!
 * js上传控件
 * 创建人：qinnailin
 * 创建时间：2014/9/10
 *
 * 
 */

(function ($) {
    $.fn.extend({
        jsUpload: function (opstion) {
            var objthis = $(this);
            if (!$("#qq-template-gallery").get(0))
            {
                var html = '<script type="text/template" id="qq-template-gallery">' +
                '<div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="选择文件">' +
                '<div class="qq-upload-button-selector qq-upload-button btn">' +
                '<div>浏览</div>' +
                '</div>' +
                '<div class="qq-upload-button-selector qq-upload-button qq-doupload-button btn hide">' +//hide
                '<div>上传</div>' +
                '</div>' +
                '<ul class="qq-upload-list-selector qq-upload-list hide" role="region" aria-live="polite" aria-relevant="additions removals">' +
                '<li>' +
                '<span  class="qq-upload-file-selector" ></span>' +
                '<span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>' +
                '<div class="qq-progress-bar-container-selector qq-progress-bar-container">' +
                '<div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>' +
                '</div>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</script>';
                $("body").append(html);
            }
            var ops = $.extend(true, {
                element: $(objthis).get(0),
                showStart: false,
                autoUpload: false,
                template: 'qq-template-gallery',
                thumbnails: {
                    placeholders: {
                        waitingPath: '/plugIn/File-uploader/placeholders/waiting-generic.png',
                        notAvailablePath: '/plugIn/File-uploader/placeholders/not_available-generic.png'
                    }
                },
                request: {
                    endpoint: '/data/upload.aspx',
                    method: 'POST'
                },
                validation: {
                    sizeLimit: 2 * 1024 * 1024,
                    minSizeLimit: 0
                },
                showMessage: function (message) {
                    if (typeof $.msg.alert == "function") {
                        $.msg.alert(message);
                    } else {
                        alert(message);
                    }
                },
                cors: {
                    allowXdr: true
                },
                messages: {
                    typeError: "{file} 文件类型不正确！: {extensions}.",
                    sizeError: "{file} 太大，最大文件大小是 {sizeLimit}.",
                    minSizeError: "{file} 太小，最小文件大小 {minSizeLimit}.",
                    emptyError: "{file} 是空的，请再次选择文件.",
                    noFilesError: "没有上传文件.",
                    tooManyItemsError: "太多的项目 ({netItems}) 项目的限制 {itemLimit}.",
                    maxHeightImageError: "图片太大.",
                    maxWidthImageError: "图片太宽.",
                    minHeightImageError: "图片高度不够.",
                    minWidthImageError: "图片宽度不够.",
                    retryFailTooManyItems: "重试失败-您已经达到了您的文件限制.",
                    onLeave: "正在上传的文件，如果你现在离开，上传将被取消."
                },
                callbacks: {
                    onError: function (id, fileName, reason) {
                        $.msg.error(fileName+" 上传失败！");
                    },
                    onComplete: function (id, fileName, responseJSON) {
                        var successfn = opstion.callbacks.onSuccess;
                        if (successfn)
                        {
                            var resdata;
                            if (typeof (responseJSON) == "string") {
                                resdata = stringToJSON(responseJSON);
                            } else {
                                resdata = responseJSON;
                            }
                            
                            if (resdata)
                            {
                                if (resdata.success) {
                                    successfn(id, fileName, resdata);
                                } else {
                                    if (typeof $.msg.alert == "function") {
                                        $.msg.alert(resdata.msg);
                                    } else {
                                        alert(resdata.msg);
                                    }
                                }
                            }
                        }
                    }
                }
            }, opstion);
            if (location.hash.substring(1).length > 0)
                document.domain = location.hash.substring(1);
            if (!ops.showStart) {
                var oufun = ops.callbacks.onUpload;
                ops.callbacks.onUpload = function (id, fileName, xhr) {
                    loading(true);
                    if (oufun) oufun(id, fileName, xhr);
                }
                var oucp = ops.callbacks.onComplete;
                ops.callbacks.onComplete = function (id, fileName, data) {
                    if (oucp) oucp(id, fileName, data);
                    loading(false);
                }
            }

            var uploader = new qq.FineUploader(ops);
            $(document).ready(function () {
                $(objthis).on("click", ".qq-doupload-button", function () {
                    uploader.uploadStoredFiles();
                });
            });
            if (ops.showStart) {
                $(".qq-upload-list-selector").removeClass("hide");
            }
            if (ops.autoUpload == false) {
                $(".qq-doupload-button").removeClass("hide");
            }
            return uploader;
        }
    });
})(jQuery);
