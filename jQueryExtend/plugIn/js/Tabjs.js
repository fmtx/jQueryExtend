﻿/*!
 * utab
 * 创建人：qinnailin
 * 创建时间：2014/11/18
 *
 *
 * 
 */

(function ($) {
    $.fn.utab = function (opstions) {
        var ops = $.extend(true, {
            click: null,
            openNext: true,
            ondisable:null,
            afterCreate:null
        }, opstions);
        var objthis = $(this);
        var idname = objthis.attr("id");
        if ($("[id=" + idname + "]").length > 1) {
            alert("检测到id重复了！");
        }
        objthis.ready(function () {
            objthis.removeClass("fm-tab").addClass("fm-tabs");
            $(objthis).find(".tab-content>div:first").addClass("tab-pane").addClass("active").siblings("div").addClass("tab-pane");
            $("<ul class=\"nav nav-tabs \"></ul>").insertBefore($(objthis).find(".tab-content"));
            $(objthis).find(".tab-pane").each(function (i, n) {
                var tag = "tab-div-box-fm-"+i;
                $(n).attr("data-tag", tag);
                var name = $(n).attr("data-name");
                $(objthis).find("ul.nav-tabs").append("<li><a href='javascript:' data-tag='" + tag + "' >" + name + "</a></li>");
            });
            $(objthis).find("ul>li:first").addClass("active");
            $(objthis).find("ul.nav-tabs").on("click", "li>a", function () {
                if (ops.openNext) {
                    var index = $(objthis).find("ul.nav-tabs>li").index($(this).parent("li"));
                    $(this).parent("li").addClass("active").siblings("li").removeClass("active");
                    $(objthis).find(".tab-content>div").eq(index).addClass("active").siblings("div").removeClass("active");
                    if ($(objthis).find(".tab-content>div").eq(index).attr("hasShow") == "true")
                        return;
                    if (ops.click) {
                        ops.click(index);
                    }
                    $(objthis).find(".tab-content>div").eq(index).attr("hasShow", "true");
                } else {
                    if (ops.ondisable)
                    {
                        ops.ondisable();
                    }
                }
            });
            if (ops.afterCreate) {
                ops.afterCreate();
            }
        });
        var func = {
            setOpenNext: function (statu) {
                ops.openNext = statu;
            }
        }
        return func;
    }
})(jQuery);