﻿/*!
 * 页面数据填充控件
 * 创建人：qinnailin
 * 创建时间：2014/7/5 
 */

(function ($) {
    $.fn.detail = function (options) {
        var ops = $.extend({
            data: null,
            url: "",
            source: "",
            idname: "id",
            afterCreate: null
        }, options || {});
        var objthis = $(this);
        objthis.addClass("detail");
        var requrl = "";
        if (queryString[ops.idname]) {
            var urlparam = ops.idname + "=" + queryString[ops.idname];
            if (ops.url.indexOf('?') > -1) {
                requrl = ops.url + "&" + urlparam;
            } else {
                requrl = ops.url + "?" + urlparam;
            }
            loadData();
        } else if (ops.url != "" && ops.data == null) {
            requrl = ops.url;
            loadData();
        } else {
            loadData();
        }
        
        /**
         * 数据加载
         * @method loadData
         * @for Detailjs
         */
        function loadData() {
            if (requrl != "") {
                $.get(requrl, function (rd) {
                    var data;
                    if (ops.source == "") {
                        data = rd;
                    } else {
                        var temp = rd;
                        var item = ops.source.split('.');
                        $.each(item, function (i, n) {
                            if (n != "") {
                                temp = temp[n];
                            }
                        });
                        data = temp;
                    }
                    $(objthis).find('[name]').each(function (i, item) {
                        var name = $(item).attr("name");
                        try {
                            $(item).html(data[name]);
                        } catch (e) {

                        }
                    });
                    converfunc();
                    if (ops.afterCreate) {
                        ops.afterCreate(rd);
                    }
                });
            } else if (ops.data) {
                $(objthis).find('[name]').each(function (i, item) {
                    var name = $(item).attr("name");
                    $(item).html(ops.data[name]);
                });
                converfunc();
            }

        }

        /**
        * 转换控制
        * @method converfunc
        * @for Detailjs
        */
        function converfunc() {
            var clist = $(objthis).find("[fm-conver]");
            $.each(clist, function (i, n) {
                var txt = $(n).html()
                var convfn = $(n).attr("fm-conver");
                var fn = eval(convfn)
                var cvres = fn(txt);
                $(n).html(cvres);
            });
        }

        /**
        * 方法组
        * @for Detailjs
        */
        var func = {
            load: function (obj) {
                if (obj instanceof Object) {
                    requrl = "";
                    ops.data = obj;
                    loadData();
                } else {
                    requrl = obj;
                    loadData();
                }
            },
            reload: function () {
                loadData();
            }
        }
        return func;
    }
})(jQuery);