/*!
 * 基础类
 * 创建人：qinnailin
 * 创建时间：2014/7/16 
 *
 *
 * 
 */

var isparent = false;
var inloading = 0;
var extendAjaxLoading = true;
var reqApplicationPath = "/";

if (top.location == self.location) {
    isparent = true;
}

//注册message事件
if ('addEventListener' in document) {
    window.addEventListener("message", function (d) {
        var func = d.data;
        if (func && func != "") {
            try {
                window.eval(func);
            } catch (e) {
                window.parent.postMessage(func, "*");
            }
        }
    }, false)
} else if ('attachEvent' in document) {
    window.attachEvent('onmessage', function (d) {
        var func = d.data;
        if (func && func != "") {
            try {
                window.eval(func);
            } catch (e) {
                window.parent.postMessage(func, "*");
            }
        }
    });
}

try {
    console.log("Copyright 2015 By qinnailin");
    console.log("http://qinnailin.cn");
} catch (e) {

}

/**
* 调用跨域函数
* @method crossdomain
* @param {string} funcn 函数名称
* @param {string} val 函数参数
* @for basejs
*/
function crossdomain(funcn, val) {
    if (val != null) {
        var t = typeof (val);
        if (t == "string" && val != "") {
            val = "'" + val + "'";
        } else {
            val = val + "";
        }
    } else {
        val = "";
    }
    window.parent.postMessage("" + funcn + "(" + val + ")", "*");
}


$(document).ajaxSend(function (evt, request, settings) {
    var paramurl = reqApplicationPath;
    if (paramurl == "/") {
        paramurl = "";
    } else if (settings.url.indexOf("/") != 0 && paramurl.length > 1) {
        paramurl += "/";
    }
    settings.url = paramurl + settings.url;
    if (settings.url.indexOf("noloading=1") == -1 && settings.url.indexOf(".html") == -1) {
        try {
            if (loading && extendAjaxLoading) {
                inloading++;
                loading(true);
            }
        } catch (e) { }
    }
});

$.ajaxSetup({
    cache: false //关闭AJAX缓存
});

$(document).ajaxComplete(function (e, req, s) {
    try {
        if (loading && s.url.indexOf("noloading=1") == -1 && s.url.indexOf(".html") == -1) {
            setTimeout(function () {
                inloading--;
                if (inloading <= 0) {
                    loading(false);
                }
            }, 300);
        }
    } catch (e) { }
    if (req.status !=200) {
        var data = eval('(' + req.responseText + ')');
        if (data.isLogin == 1) {
            //展开登录窗口
            if (!isparent) {
                crossdomain("_showLoginModel", "");
            } else {
                $.msg.alert("请登录！");
            }
        } else if (data.error && data.error == 99) {
            $.msg.alert(data.msg);
            return false;
        } else if (data.Message) {
            $.msg.alert(data.Message + "</br>" + data.StackTrace);
            return false;
        }
    }
});

//页面加载
$(document).ready(function () {
    var html = '<div style="display:none;" class="row layout-detail">';
    html += '<div class="col-lg-12 col-md-12">';
    html += '<div class="utable-tool"  >';
    html += '<a class="btn btn-default layout-goback">返回</a>';
    html += '</div>';
    html += '<div id="div_layout_body_detail" class="layout-body">';
    html += '</div>';
    html += '</div></div>';
    $(".layout-list").after(html);
    $.layout = $.extend({
        detail: function (url, fn) {
            $(".layout-list").hide(200);
            $(".layout-detail").show(200, function () {
                $('html, body').animate({ scrollTop: 0 }, 500);
            });
            $(".layout-body").load(url,fn);
        },
        back: function () {
            $(".layout-list").show(200);
            $(".layout-detail").hide(200, function () {
                $('html, body').animate({ scrollTop: 0 }, 500);
            });
            $(".layout-body").html("");
        }
    });
    $(".layout-detail").on("click", ".layout-goback", function () {
        $(".layout-list").show(200);
        $(".layout-detail").hide(200, function () {
            $('html, body').animate({ scrollTop: 0 }, 500);
        });
        $(".layout-body").html("");
        //$('html, body').animate({ scrollTop: 0 }, 500);
    });
});

//计算边距
function getborder(obj) {
    return getotborder(obj) + getodborder(obj);
}
//技术部分边距
function getborderSm(obj) {
    var mt = $(obj).css("margin-top");
    var mb = $(obj).css("margin-bottom");
    var bt = $(obj).css("border-top-width");
    var bb = $(obj).css("border-bottom-width");
    var val = 0;
    if (mt) {
        var v = parseFloat(mt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (mb) {
        var v = parseFloat(mb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (bt) {
        var v = parseFloat(bt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (bb) {
        var v = parseFloat(bb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    return val;
}
//计算左右边框
function getwborder(obj) {
    return getolwborder(obj) + getorwborder(obj);
}

//计算上下边框
function gethborder(obj) {
    return getotborder(obj) + getodborder(obj);
}

//计算左边框
function getolwborder(obj) {
    var pt = $(obj).css("padding-left");
    var mt = $(obj).css("margin-left");
    var bt = $(obj).css("border-left-width");
    var val = 0;
    if (pt) {
        var v = parseFloat(pt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (mt) {
        var v = parseFloat(mt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (bt) {
        var v = parseFloat(bt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    return val;
}

//计算右边框
function getorwborder(obj) {
    var pb = $(obj).css("padding-right");
    var mb = $(obj).css("margin-right");
    var bb = $(obj).css("border-right-width");
    var val = 0;
    if (pb) {
        var v = parseFloat(pb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (mb) {
        var v = parseFloat(mb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (bb) {
        var v = parseFloat(bb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    return val;
}

//计算边距
function getotborder(obj) {
    var pt = $(obj).css("padding-top");
    var mt = $(obj).css("margin-top");
    var bt = $(obj).css("border-top-width");
    var val = 0;
    if (pt) {
        var v = parseFloat(pt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (mt) {
        var v = parseFloat(mt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (bt) {
        var v = parseFloat(bt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    return val;
}

//计算边距
function getontborder(obj) {
    var mt = $(obj).css("margin-top");
    var bt = $(obj).css("border-top-width");
    var val = 0;
    if (mt) {
        var v = parseFloat(mt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (bt) {
        var v = parseFloat(bt.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    return val;
}

//计算边距
function getodborder(obj) {
    var pb = $(obj).css("padding-bottom");
    var mb = $(obj).css("margin-bottom");
    var bb = $(obj).css("border-bottom-width");
    var val = 0;
    if (pb) {
        var v = parseFloat(pb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (mb) {
        var v = parseFloat(mb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (bb) {
        var v = parseFloat(bb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    return val;
}

//计算边距
function getondborder(obj) {
    var mb = $(obj).css("margin-bottom");
    var bb = $(obj).css("border-bottom-width");
    var val = 0;
    if (mb) {
        var v = parseFloat(mb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    if (bb) {
        var v = parseFloat(bb.replace("px", ""));
        if (!isNaN(v))
            val += v;
    }
    return val;
}

/**
* 数组删除
* @method fillData
* @param {object} data 数据对象
* @for basejs
*/
Array.prototype.remove = function (dx) {
    if (isNaN(dx) || dx > this.length) {
        return false;
    }
    for (var i = 0, n = 0; i < this.length; i++) {
        if (this[i] != this[dx]) {
            this[n++] = this[i];
        }
    }
    this.length -= 1;
};

/**
* 填充数据源
* @method fillData
* @param {object} data 数据对象
* @for basejs
*/
String.prototype.fill = function (data) {
    if (typeof data === "object" && data) {
        return this.replace(/{{(.+?)}}/g, function (e1, e2) {
            var res = data[e2];
            if (res == undefined || res == null) {
                return e1;
            } else {
                if (typeof (res) == "object") {
                    return stringify(res);
                } else if (res == "0001-01-01 00:00:00") {
                    return "";
                } else {
                    return res;
                }
            }
        });
    }
};

/**
* 填充空对象
* @method fillEmpty
* @for basejs
*/
String.prototype.fillEmpty = function () {
    return this.replace(/{{(.+?)}}/g, function (e1, e2) {
        return "";
    });
};

/**
* 替换指定项
* @method fillData
* @param {string} key 键
* @param {object} value 数据对象
* @for basejs
*/
String.prototype.replaceo = function (key, value) {
    return this.replace("{{" + key + "}}", value);
}

/**
* 获取url键值对
* @method queryString
* @for basejs
*/
var queryString = {
    __init__: function () {
        var paraList = window.location.search.slice(1).split(/\&/g);
        for (var i = 0; i < paraList.length; i++) {
            var pattern = /^(.+)[?=\\=](.+)/g,
              mp = pattern.exec(paraList[i]);
            if (mp) {
                this[mp[1]] = mp[2];
            }
        }
    }
};
queryString.__init__();

function getrequestparam(key) {
    var kv;
    var paraList = window.location.search.slice(1).split(/\&/g);
    for (var i = 0; i < paraList.length; i++) {
        var pattern = /^(.+)[?=\\=](.+)/g,
          mp = pattern.exec(paraList[i]);
        if (mp) {
            kv[mp[1]] = mp[2];
        }
    }
    return kv[key];
}

/**
* json字符串转json对象
* @method stringToJSON
* @param {string} obj 字符串
* @for basejs
*/
function stringToJSON(obj) {
    try {
        return eval('(' + obj + ')');
        //return jQuery.parseJSON(obj);
    } catch (e) {
        alert("你的数据不符合规范！请检查！");
    }
}

/**
* 将json转为字符串
* @method stringify
* @param {object} obj 数据对象
* @for basejs
*/
var stringify = function (obj) {
    //如果是IE8+ 浏览器(ff,chrome,safari都支持JSON对象)，使用JSON.stringify()来序列化
    if (window.JSON) {
        return JSON.stringify(obj);
    }
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"' + obj + '"';
        return String(obj);
    } else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);

        // fix.
        var self = arguments.callee;

        for (n in obj) {
            v = obj[n];
            t = typeof (v);
            if (obj.hasOwnProperty(n)) {
                if (t == "string") v = '"' + v + '"'; else if (t == "object" && v !== null)
                    // v = jQuery.stringify(v);
                    v = self(v);
                json.push((arr ? "" : '"' + n + '":') + String(v));
            }
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

(function ($) {
    $.date = $.extend({
        //* 获取日期格式 */
        getDate: function (d) {
            var date = ConvertJSONDate(d);
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var day = date.getDate();
            return year + "-" + month + "-" + day;
        },
        //* 获取日期时间格式*/
        getDateTime: function (d) {
            var date = ConvertJSONDate(d);
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var day = date.getDate();
            var hh = date.getHours();
            var mm = date.getMinutes();
            var ss = date.getSeconds();
            return year + "-" + month + "-" + day + " " + hh + ":" + mm + ":" + ss;
        }
    });
    //===========json时间转换==================
    function ConvertJSONDate(jsondate) {
        var date = new Date(parseInt(jsondate.replace("/Date(", "").replace(")/", ""), 10));
        return date;
    }
    //将数据填充到form表单中
    $.copy = $.extend({
        form: function (data, element) {
            var objthis;
            if (typeof element == 'string' && element != "") {
                objthis = $("#" + element);
            } else {
                objthis = element;
            }
            $(objthis).find('[name]').each(function (i, item) {
                var name = $(item).attr("name");
                if (($(item).is("input[type=checkbox]") || $(item).is("input[type=radio]")) && $(item).val() == data[name]) {
                    $(item).attr("checked", true);
                } else if ($(item).is("input")) {
                    $(item).val(data[name]);
                } else if ($(item).is("select")) {
                    $(item).find("option[value=" + data[name] + "]").attr("selected", true);
                } else {
                    $(item).html(data[name]);
                }
            });
        }
    });
})(jQuery);
