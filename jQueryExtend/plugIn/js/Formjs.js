/*!
 * form表单验证及初始化
 *创建人：qinnailin
 * 创建时间：2014/7/16 
 *
 *
 标签说明：
 fm-form="ajax" form使用ajax提交数据 不加入标签为普通提交
 fm-null="用户名不能为空！" 空验证
 fm-len="4,20,用户名长度必须大于4小于20！"  字符长度验证，
 fm-ajax="/user/has,用户名已存在!"  ajax验证
 fm-eq="pwd,两次输入密码不一致！" 相等验证 pwd为对比标签的name
 fm-reg="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*,请输入正确的邮箱格式！" 正则表达式验证
 fm-group="radio,请选择性别！"  radio组验证 fm-group在radio外层
 fm-group="checkbox,2,4,请任选2项！最多4项！" checkbox 组验证 fm-group在checkbox外层
 fm-neq="-1,请选择地区!" 非验证 值不等于 -1
 fm-norreg="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*,请输入正确的格式"//非必填项
 fm-norgt="0,,请输入大于0的数值"//非必填项，可定义正则表达式
 fm-norlen="10,请输入10个字节的字符串"//非必填指定长度
 * 
 */


(function($){
	$.fn.Form=function(options){
		var ops=$.extend(true, {
			url:"",
			data:null,
			source:"",
			idname:"id",//url  id
			success: function (data) { $.msg.info("数据提交成功！"); },
			error: function (data) { $.msg.error("数据提交失败!") },
			offset: { top: 0, left: 0 },
			placement: "bottom",
			afterCreate: null,
			beforBind: null,
            disabled:false
		}, options);
		var objthis = $(this);
		var idname = objthis.attr("id");
		if ($("[id=" + idname + "]").length > 1) {
		    alert("检测到id重复了！");
		}
		if (objthis.attr("placement") == undefined) {
		    objthis.attr("placement",ops.placement);
		}
		var browser = navigator.appName;
		var b_version = navigator.appVersion;
		var version = b_version.split(";");
		if (version.length > 1) {
		    var trim_Version = parseInt(version[1].replace(/[ ]/g, "").replace(/MSIE/g, ""));
		    if (trim_Version > 7) {
		        $(objthis).find("input[placeholder]").each(function (i, n) {
		            var ph = $(n).attr("placeholder");
		            if ($(n).val() == "" && $(this).attr("type") != "password") {
		                $(n).val(ph);
		                $(n).css("color", "rgb(204, 204, 204)");
		            }
		        });
		        $(objthis).on("focus", "input[placeholder]", function (i, n) {
		            var ph = $(this).attr("placeholder");
		            if ($(this).val() == ph) {
		                $(this).val("");
		                $(this).css("color", "#000000");
		            }
		        });//获取焦点的时候
		        $(objthis).on("blur", "input[placeholder]", function (i, n) {
		            var ph = $(this).attr("placeholder");
		            if ($(this).val() == "" && $(this).attr("type") != "password") {
		                $(this).val(ph);
		                $(this).css("color", "rgb(204, 204, 204)");
		            }
		        });//失去焦点的时候
		    }
		}
        var requrl = "";
        if (queryString[ops.idname]) {
            var urlparam = ops.idname + "=" + queryString[ops.idname];
            if (ops.url.indexOf('?') > -1) {
                requrl = ops.url + "&" + urlparam;
            } else {
                requrl = ops.url + "?" + urlparam;
            }
            loadData();
        } else if (ops.url != "" && ops.data == null) {
            requrl = ops.url;
            loadData();
        } else if (ops.url == "" && ops.data != null)
        {
            loadData();
        }else {
            if (ops.afterCreate) {
                ops.afterCreate(null);
            }
        }
        setDisabled();

        /**
	     * 数据加载
	     * @method loadData
	     * @for Formjs
	     */
        function loadData() {
	        if (requrl != "") {	          
	            $.get(requrl, function (rd) {
		            var data;
		            if (ops.source == "") {
		                data = rd;
		            } else {
		                var temp=rd;
		                var item = ops.source.split('.');
		                $.each(item, function (i, n) {
		                    if (n != "") {
		                        temp = temp[n];
		                    }
		                });
		                data = temp;
		            }
		           
		            if (data) {
		                if (ops.beforBind)
		                {
		                    ops.beforBind();
		                }
		                $(objthis).find('[name]').each(function(i,item){
		                    var name = $(item).attr("name");
		                    if (($(item).is("input[type=checkbox]") || $(item).is("input[type=radio]"))) {
		                        if ($(item).val() == data[name]) {
		                            $(item).attr("checked", true);
		                        } else {
		                            $(item).removeAttr("checked");
		                        }
		                    } else if ($(item).is("input")) {
		                        $(item).css("color", "#000000");
		                        if (data[name] == "0001-01-01 00:00:00") {
		                            // $(item).val("");		                           
		                        } else {
		                            var aname = $(item).attr("name");
		                            if (aname.indexOf('.') > 0 && aname.indexOf('[') > 0) {
		                                var arrn = aname.split('.');
		                                var bname = arrn[0].split('[');
		                                var num = bname[1].split(']')[0];
		                                if (data[bname[0]]) {
		                                    $(item).val(data[bname[0]][num][arrn[1]]);		                                  
		                                }		                                
		                            } else {
		                                $(item).val(data[name]);		                              
		                            }

		                        }
		                    } else if ($(item).is("select")) {
		                        $(item).find("option[value=" + data[name] + "]").attr("selected", true);
		                    } else {
		                        $(item).html(data[name]);		                       
		                    }
		                });
		            }
		            converfunc();
		            if (ops.afterCreate) {
		                ops.afterCreate(rd);
		            }

		            objthis.find(".calendar").each(function (i, n) {
		                var date = $(n).val();
		                if (date && date.length > 4 && date.substring(0,4) == "1900") {
		                    $(n).val("");
		                }
		            });
	                try {
	                    $(".upload-control").CQUploadShow();
	                } catch (e) {
	                }
		        });
	        } else {
	    	    if (ops.data) {
	    	        var data = ops.data;
	    	        if (ops.beforBind) {
	    	            ops.beforBind();
	    	        }
	    	     
	                $(objthis).find('[name]').each(function(i,item){
	                    var name = $(item).attr("name");
	                    if (($(item).is("input[type=checkbox]") || $(item).is("input[type=radio]")) && $(item).val() == data[name]) {
	                        $(item).attr("checked", true);
	                    } else if ($(item).is("input")) {
	                        $(item).css("color", "#000000");
	                        if (data[name] == "0001-01-01 00:00:00") {
	                            //$(item).val("");
	                        } else {
	                            var aname = $(item).attr("name");
	                            if (aname.indexOf('.') > 0 && aname.indexOf('[') > 0) {
	                                var arrn = aname.split('.');
	                                var bname = arrn[0].split('[')[0];
	                                if (data[bname]) {
	                                    $(item).val(data[bname][arrn[1]]);	                                   
	                                }
	                            } else {
	                                $(item).val(data[name]);	                               
	                            }
	                        }
	                    } else if ($(item).is("select")) {
	                        $(item).find("option[value=" + data[name] + "]").attr("selected", true);
	                    } else {
	                        $(item).html(data[name]);	                        
	                    }
	                });
	            }
	    	    converfunc();
	    	    if (ops.afterCreate) {
	    	        ops.afterCreate(ops.data);
	    	    }
	    	    objthis.find(".calendar").each(function (i, n) {
	    	        var date = $(n).val();
	    	        if (date && date.length > 4 && date.substring(0,4) == "1900") {
	    	            $(n).val("");
	    	        }
	    	    });
	            try {
	                $(".upload-control").CQUploadShow();
	            } catch (e) {
	            }
	        }
	    }
        //设置为只读
	    function setDisabled()
	    {
	        if (ops.disabled)
	        {
	            $(objthis).find("input").attr("disabled", true);
	            $(objthis).find("select").attr("disabled", true);
	            $(objthis).find("textarea").attr("disabled", true);
	            $(objthis).find("button").attr("disabled", true);
	            $(objthis).find("a").attr("disabled", true);
	            $(objthis).find(".btn").attr("disabled", true);
	            $(objthis).removeAttr("action");
	        }
	    }

	    /**
	    * 转换控制
	    * @method converfunc
	    * @for Formjs
	    */
	    function converfunc() {
	        var clist = $(objthis).find("[fm-conver]");
	        $.each(clist, function (i, n) {
	            if ($(n).is("input")) {
	                var txt = $(n).val();
	                var convfn = $(n).attr("fm-conver");
	                var fn = eval(convfn);
	                var cvres = fn(txt);
	                $(n).val(cvres);
	            } else {
	                var txt = $(n).html()
	                var convfn = $(n).attr("fm-conver");
	                var fn = eval(convfn)
	                var cvres = fn(txt);
	                $(n).html(cvres);
	            }
	        });
	    }

	    /**
	     * 验证非空
	     * @method vnull
	     * @for Formjs
	     */
	    $.fn.vnull=function () {
	        if ($(this).val() == "") {
	            $(this).showError($(this).attr("fm-null"));
	        } else {
	            $(this).showSuccess();
	        }
	    }

	    /**
	     * 验证长度
	     * @method vlen
	     * @for Formjs
	     */
	    $.fn.vlen = function () {
	        var v = $(this).attr("fm-len").split(",");
	        var min = v[0] ? parseInt(v[0]) : 0;
	        var len = $(this).val().length;
	        if (len < min || (v[1] != "" && len > parseInt(v[1]))) {
	            $(this).showError(v[2]);
	        } else {
	            $(this).showSuccess();
	        }
	    }

	    /**
	     * 验证等于
	     * @method veq
	     * @for Formjs
	     */
	    $.fn.veq = function () {
	        var v = $(this).attr("fm-eq").split(",");
	        var tagv = $("[name=" + v[0] + "]").val();
	        if ($(this).val() != tagv) {
	            $(this).showError(v[1]);
	        } else {
	            $(this).showSuccess();
	        }
	    }

	    /**
	     * 验证组
	     * @method vgroup
	     * @for Formjs
	     */
	    $.fn.vgroup = function () {
	        var v = $(this).attr("fm-group").split(",");
	        if (v[0] == "radio") {
	            var radio = $(this).find("input:radio:checked").val();
	            if (!radio) {
	                $(this).removeClass("success").addClass("error");
	                $(this).showError(v[1]);
	            } else {
	                $(this).removeClass("error").addClass("success");
	                $(this).showSuccess();
	            }
	        } else if (v[0] == "checkbox") {
	            var size = $(this).find("input:checked").size();
	            if ((v[1] != "" && size < parseInt(v[1])) || (v[2] != "" && size > parseInt(v[2]))) {
	                $(this).removeClass("success").addClass("error");
	                $(this).showError(v[3]);
	            } else {
	                $(this).removeClass("error").addClass("success");
	                $(this).showSuccess();
	            }
	        }
	    }

	    /**
	     * 验证不等于
	     * @method vneq
	     * @for Formjs
	     */
	    $.fn.vneq = function () {
	        var v = $(this).attr("fm-neq").split(",");
	        if ($(this).val() == v[0]) {
	            $(this).showError(v[1]);
	        } else {
	            $(this).showSuccess();
	        }
	    }

	    /**
	     * 验证正则
	     * @method vreg
	     * @for Formjs
	     */
	    $.fn.vreg = function () {
	        var v = $(this).attr("fm-reg").split(",");
	        var msg="";
	        var reqexpstr="";
	        if(v.length>2){
	        	msg=v[v.length-1];
	        	var str=$(this).attr("fm-reg");
	        	reqexpstr=str.substring(0,str.lastIndexOf(","));
	        }else{
	        	msg=v[1];
	        	reqexpstr=v[0];
	        }
	        var r = RegExp(reqexpstr);
	        if (!r.test($(this).val())) {
	            $(this).showError(msg);
	        } else {
	            $(this).showSuccess();
	        }
	    }


	    /**
	     * 验证ajax
	     * @method vajax
	     * @for Formjs
	     */
	    $.fn.vajax = function () {
	        var v = $(this).attr("fm-ajax").split(",");
	        if ($(this).attr("fm-ajax-res") == "false") {
	            var v = $(this).attr("fm-ajax").split(",");
	            $(this).showError(v[1]);
	        } else if ($(this).attr("fm-ajax-res") == "") {
	            valid = false;
	            $(this).showError(v[1]);
	        } 
	    }

	    /**
	     * 空或者指定长度
	     * @method vNOrlen
	     * @for Formjs
	     */
	    $.fn.vNOrlen = function () {
	        var v = $(this).attr("fm-norlen").split(",");
	        var len = $(this).val().length;
	        if (len > 0 && (v[0] != "" && len != parseInt(v[0]))) {
	            $(this).showError(v[1]);
	        } else {
	            $(this).showSuccess();
	        }
	    }

	    /**
	     * 空或者指定正则表达式
	     * @method vNOrlen
	     * @for Formjs
	     */
	    $.fn.vNorreg = function () {
	        if ($.trim($(this).val()) == "") {
	            $(this).showSuccess();
	            return;
	        }
	        var v = $(this).attr("fm-norreg").split(",");
	        var msg = "";
	        var reqexpstr = "";
	        if (v.length > 2) {
	            msg = v[v.length - 1];
	            var str = $(this).attr("fm-norreg");
	            reqexpstr = str.substring(0, str.lastIndexOf(","));
	        } else {
	            msg = v[1];
	            reqexpstr = v[0];
	        }
	        var r = RegExp(reqexpstr);
	        if (!r.test($(this).val())) {
	            $(this).showError(msg);
	        } else {
	            $(this).showSuccess();
	        }
	    }
	    /**
	     * 空或者大于指定值
	     * @method vNOrlen
	     * @for Formjs
	     */
	    $.fn.vNOrgt = function () {
	        var v = $(this).attr("fm-norgt").split(",");
	        var msg = v[2];
	        var val = $(this).val();
	        if ($.trim(val) == "") return;
	        if (parseFloat(val) <= parseFloat(v[0])) {
	            $(this).showError(msg);
	            return;
	        }
	        var reqexpstr = "";
	        if (v.length > 3) {
	            msg = v[v.length - 1];
	            var str = $(this).attr("fm-norgt");
	            var start = v[0].length;
	            reqexpstr = str.substring(start, str.lastIndexOf(","));
	        } else {
	            msg = v[2];
	            reqexpstr = v[1];
	        }
	        if ($.trim(reqexpstr) == "") reqexpstr = /^\d+$/;
	        var r = RegExp(reqexpstr);
	        if (!r.test($(this).val())) {
	            $(this).showError(msg);
	        } else {
	            $(this).showSuccess();
	        }
	    }

	    /**
	    * 验证用户输入合法性
	    * @method validation
	    * @for Formjs
	    */
	    function validation(){
	        valid = true;
	        //$("code").remove();
	        $(objthis).find("[fm-null]").each(function (i, n) {
	            if (!$(n).hasClass("error")) {
	                $(n).vnull();
	            }
	    	});
	        $(objthis).find("[fm-len]").each(function (i, n) {
	            if (!$(n).hasClass("error"))
	    	        $(n).vlen();
	    	});
	        $(objthis).find("[fm-eq]").each(function (i, n) {
	            if (!$(n).hasClass("error"))
	    	    $(n).veq();
	    	});
	        $(objthis).find("[fm-group]").each(function (i, n) {
	            if (!$(n).hasClass("error"))
	    	    $(n).vgroup();
	    	});
	        $(objthis).find("[fm-neq]").each(function (i, n) {
	            if (!$(n).hasClass("error"))
	    	    $(n).vneq();
	    	});

	        $(objthis).find("[fm-reg]").each(function (i, n) {
	            if (!$(n).hasClass("error"))
	    	    $(n).vreg();
	    	});

	        $(objthis).find("[fm-ajax]").each(function (i, n) {
	            if (!$(n).hasClass("error"))
	    	    $(n).vajax();
	    	});

	        $(objthis).find("[fm-norlen]").each(function (i, n) {
	            if (!$(n).hasClass("error"))
	    	    $(n).vNOrlen();
	    	});

	        $(objthis).find("[fm-norreg]").each(function (i, n) {
	            if (!$(n).hasClass("error"))
	    	    $(n).vNorreg();
	    	});

	        $(objthis).find("[fm-norgt]").each(function (i, n) {
	            if (!$(n).hasClass("error"))
	    	    $(n).vNOrgt();
	    	});

	    }

	    

	    /**
	    * 页面加载完毕
	    * @method ready
	    * @for Formjs
	    */
	    $(document).ready(function () {
	        $(objthis).submit(function () {
	            $(objthis).find("input[placeholder]").each(function (i, n) {
	                var ph = $(this).attr("placeholder");
	                if ($(this).val() == ph) {
	                    $(this).val("");
	                }
	            });///提交的时候先清空placeholder

	            validation();
	            var valid = $(objthis).find(".error").get(0);
			    if (!valid) {
			        if ($(objthis).attr("fm-form")&&$(objthis).attr("fm-form") == "ajax") {
			            $.ajax({
			                url: $(this).attr("action"),
			                type: this.method,
			                cache: false,
			                data: $(this).serialize(),
			                success: function (result) {
			                    ops.success(result);
			                },
			                error: function (result) {
			                    ops.error(result);
			                }
			            });
			            return false;
			        } else {
			            return true;
			        }
			    } else {
			        $.msg.alert("提交的数据未能通过校验，请将鼠标移动到表单中<code style='color:red;' >红色</code>边框查看错误提示！");
			        return false;
			    }
			    return false;
	        });

	        //绑定blur
	        $(objthis).on("blur", "input", function () {
	            var oth = $(this);
	            var alen = $(oth).get(0).attributes.length;
	            for (var i = 0; i < alen; i++) {
	                var name = $(oth).get(0).attributes[i].name;
	                switch (name) {
	                    case "fm-null":
	                        if (!oth.hasClass("error"))
	                        $(oth).vnull();
	                        break;
	                    case "fm-len":
	                        if (!oth.hasClass("error"))
	                        $(oth).vlen();
	                        break;
	                    case "fm-eq":
	                        if (!oth.hasClass("error"))
	                        $(oth).veq();
	                        break;
	                    case "fm-neq":
	                        if (!oth.hasClass("error"))
	                        $(oth).vneq();
	                        break;
	                    case "fm-reg":
	                        if (!oth.hasClass("error"))
	                        $(oth).vreg();
	                        break;
	                    case "fm-norlen":
	                        if (!oth.hasClass("error"))
	                        $(oth).vNOrlen();
	                        break;
	                    case "fm-norreg":
	                        if (!oth.hasClass("error"))
	                        $(oth).vNorreg();
	                        break;
	                    case "fm-norgt":
	                        if (!oth.hasClass("error"))
	                        $(oth).vNOrgt();
	                        break;
	                    default:
	                }
	            }
	            if (oth.attr("fm-ajax")) {
	                if (oth.hasClass("error")) return;
	                var v = $(oth).attr("fm-ajax").split(",");
	                var key = $(this).attr("name");
	                var val = $(this).val();
	                if (val != "") {
	                    var data = key + "=" + val;
	                    $.ajax({
	                        url: v[0],
	                        type: "get",
	                        data: data,
	                        success: function (result) {
	                            if (result) {
	                                $(oth).attr("fm-ajax-res", "true");
	                                $(this).showSuccess();
	                            } else {
	                                $(oth).showError(v[1]);
	                                $(oth).attr("fm-ajax-res", "false");
	                            }
	                        },
	                        error: function (result) {
	                            $(oth).showError("数据请求失败");
	                            $(oth).attr("fm-ajax-res", "false");
	                        }
	                    });
	                }
	            }
	        });
	        //绑定blur
	        $(objthis).on("blur", "textarea", function () {
	            var oth = $(this);
	            var alen = $(oth).get(0).attributes.length;
	            for (var i = 0; i < alen; i++) {
	                var name = $(oth).get(0).attributes[i].name;
	                switch (name) {
	                    case "fm-null":
	                        if (!oth.hasClass("error"))
	                        $(oth).vnull();
	                        break;
	                    case "fm-len":
	                        if (!oth.hasClass("error"))
	                        $(oth).vlen();
	                        break;
	                    default:
	                }
	            }
	        });
	        //绑定blur
	        $(objthis).on("blur", "select", function () {
	            var oth = $(this);
	            var alen = $(oth).get(0).attributes.length;
	            for (var i = 0; i < alen; i++) {
	                var name = $(oth).get(0).attributes[i].name;
	                switch (name) {
	                    case "fm-neq":
	                        if (!oth.hasClass("error"))
	                        $(oth).vneq();
	                        break;
	                    default:
	                }
	            }
	        });
	       
	        //绑定focus
	        $(objthis).on("focus", "input", function () {
	            $(this).removeError();
	        });
	        $(objthis).on("click", "input:radio", function () {
	            $(this).parent(".error").removeError();
	        });
	        $(objthis).on("click", "input:checkbox", function () {
	            $(this).parent(".error").removeError();
	        });
	        //绑定focus
	        $(objthis).on("focus", "textarea", function () {
	            $(this).removeError();
	        });
	        //绑定focus
	        $(objthis).on("focus", "select", function () {
	            $(this).removeError();
	        });

	        //绑定change
	        $(objthis).on("change", "input", function () {
	            $(this).removeError();
	        });
	        //绑定change
	        $(objthis).on("change", "textarea", function () {
	            $(this).removeError();
	        });
	        //绑定change
	        $(objthis).on("change", "select", function () {
	            $(this).removeError();
	        });
		});

        /**
        * 方法组
        * @for Formjs
        */
        var func = {
            load:function(opst){
            	if(opst instanceof Object){
                    ops.data=opst;
                    requrl="";
                    loadData();
                }else{
                    var urlparam = ops.idname + "=" + id;
	                if (ops.url.indexOf('?') > -1) {
	                    requrl = ops.url + "&" + urlparam;
	                } else {
	                    requrl = ops.url + "?" + urlparam;
	                }
	                loadData();
                }
            },
            reload: function () {
                window.location.reload();
            },
            refresh: function () {
                loadData();
            },
            reset:function(){
                $(objthis)[0].reset();
                $(objthis).find("input").val("");
            },
            closemsg: function () {
                $("div.u_warn").remove();
            },
            validation: function () {
                $(objthis).find(".error").removeClass("error");
                validation();
                return !$(objthis).find(".error").get(0);
            }
        }
        return func;
	}
    /**
	    * 输出错误
	    * @method showError
	    * @for Formjs
	    */
	$.fn.showError = function (msg) {
	    $(this).removeClass("error").addClass("error");
	    var placementp = $(this).parents("form").attr("placement");
	    var tplacement = $(this).attr("placement");
	    $(this).attr("data-title", msg).Tooltip({
	        placement: tplacement?tplacement:(placementp?placementp:"bottom"),
	        template: '<div class="tooltip form"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
	    });
	}
    /**
    * 显示正确信息
    * @method showSuccess
    * @for Formjs
    */
	$.fn.showSuccess = function () {
	    $(this).removeClass("error");
	    $(this).attr("data-title", "");
	    $(".tooltip").remove();
	}
    /**
    * 移除提示信息
    * @method removeError
    * @for Formjs
    */
	$.fn.removeError = function () {
	    $(this).removeClass("error");
	    $(this).attr("data-title", "");
	    $(".tooltip").remove();
	}
})(jQuery);