﻿/*!
 * jqueryui 模态窗口拓展类
 *创建人：qinnailin
 * 创建时间：2014/7/20 
 *
 *
 * 
 */

var basic_modal_content_count = 0;
(function ($) {
    $.fn.modal = function (options) {
        var ops = $.extend(true, {
            title: "",
            url: null,
            modal: true,
            resizable: false,
            width: "auto",
            timeout: null,
            afterCreate: null,
            closeText: ""
        }, options);
        if (!$(this).get(0))
        {
            $.msg.alert("请添加容器！");
            return false;
        }
        var objthis = $(this);
        var html = $(objthis).html();
        if (ops.url) {
            $.get(ops.url,{_t:Math.random()}, function (hdata) {
                html = hdata;
                showmodal();
            });
        } else {
            showmodal();
        }
        function showmodal() {

            if ($(objthis).is("script")) {
                var mun = $(objthis).attr("data-mun");
                if (!mun) {
                    mun = ++basic_modal_content_count;
                    $("body").append("<div id='basic-modal-content-" + mun + "' class='basic-modal-content' style='display:none;overflow: visible;'></div>")
                    $(objthis).attr("data-mun", mun);
                } else {
                    if (!document.getElementById("basic-modal-content-" + mun)) {
                        $("body").append("<div id='basic-modal-content-" + mun + "' class='basic-modal-content' style='display:none;overflow: visible;'></div>")
                    }
                }
                objthis = $("#basic-modal-content-" + mun);
            }
            $(objthis).html(html);
            try {
                $(objthis).dialog(ops);
            } catch (e) {
                alert("请引入jqueryui插件");
            }

            $(objthis).dialog({
                close: function (event, ui) {
                    $(objthis).dialog("destroy");
                    if ($(objthis).hasClass("basic-modal-content"))
                        $(objthis).html("");
                    if (ops.close) {
                        ops.close();
                    }
                }
            });


            if (ops.timeout) {
                setTimeout("closemodel(" + num + ")", ops.timeout);
            }
            if (ops.afterCreate) {
                ops.afterCreate();
            }
            $(objthis).siblings(".ui-dialog-titlebar").find(".ui-dialog-titlebar-close").addClass("ui-state-default");
            var btnbox = $(objthis).next("div").find(".ui-dialog-buttonset").eq(0);
            var nthing = 0;
            btnbox.find("button").each(function (i, n) {
                $(n).removeClass("ui-button").addClass("btn");
                var btn = ops.buttons[i];
                if (btn.css) {
                    $(n).addClass(btn.css);
                    nthing++;
                } else {
                    $(n).addClass("btn-default");
                }
            });
            if (!btnbox.find(".btn-primary").get(0) && btnbox.find("button").length > 1 && 0 == nthing)
            {
                btnbox.find("button").eq(0).removeClass("btn-default").addClass("btn-primary");
            }
        }
        var func = {
            close: function () {
                if ($(objthis).is("script")) {
                    var mun = $(objthis).attr("data-mun");
                    if (!mun) {
                        $(objthis).dialog("close");
                        $(objthis).html("");
                    } else {
                        closemodel(mun);
                    }
                } else {
                    $(objthis).dialog("close");
                }
            }
        }
        return func;
    }
    /**
	* 模态窗口关闭
	* @method close
	* @for modeljs
	*/
    $.fn.close = function () {
        if ($(this).is("script")) {
            var mun = $(this).attr("data-mun");
            if (!mun) {
                $(this).dialog("close");
                $(this).html("");
            } else {
                closemodel(mun);
            }
        } else {
            $(this).dialog("close");
        }
    }

    /**
    * 模态窗口关闭
    * @method closemodel
    * @param {int} num 模态窗口序号
    * @for modeljs
    */
    closemodel = function (num) {
        $("#basic-modal-content-" + num).dialog("close");
        $("#basic-modal-content-" + num).html("");
    }
})(jQuery);



(function ($) {
    $.msg = $.extend({
        //alert提示
        alert: function (html,fn) {
            if (!document.getElementById("message-basic-modal-content")) {
                $("body").append("<div id='message-basic-modal-content' style='display:none;overflow: visible;'></div>")
            }
            var showmodal = true;
            if ($(".ui-widget-overlay").get(0)) {
                showmodal = false;
            }
            var ops = $.extend(true, {
                modal: showmodal,
                resizable: false,
                width: "250",
                clear: true,
                title: "提示!",
                closeText:"",
                beforeClose: function () {
                    $("#message-basic-modal-content").html("").hide();
                },
                buttons: [
                            {
                                text: "确定",
                                click: function () {
                                    $("#message-basic-modal-content").dialog("close")
                                    if (fn) {
                                        fn();
                                    }
                                }
                            }
                ]
            }, {});
            if (!isparent && showmodal) {
                crossdomain("showbgmodel", "");
            }
            $("#message-basic-modal-content").html("<div>" + html + "</div>");
            $("#message-basic-modal-content").dialog(ops);
            $("#message-basic-modal-content").siblings(".ui-dialog-titlebar").find(".ui-dialog-titlebar-close").addClass("ui-state-default");
            return false;
        },
        //confirm扩展
        confirm: function (html, func, fune) {
            if (!document.getElementById("message-basic-modal-content")) {
                $("body").append("<div id='message-basic-modal-content' style='display:none;overflow: visible;'></div>")
            }
            var showmodal = true;
            if ($(".ui-widget-overlay").get(0)) {
                showmodal = false;
            }
            var ops = $.extend(true, {
                modal: showmodal,
                resizable: false,
                width: "350",
                clear: true,
                title: "提示!",
                closeText: "",
                beforeClose: function () {
                    $("#message-basic-modal-content").html("").hide();
                    crossdomain("closebgmodel", "");
                },
                buttons: [
                            {
                                text: "确定",
                                click: function () {
                                    if (func) {
                                        func();
                                    }
                                    $("#message-basic-modal-content").dialog("close")
                                }
                            },
                            {
                                text: "取消",
                                click: function () {
                                    $("#message-basic-modal-content").dialog("close")
                                    if (fune) {
                                        fune();
                                    }
                                }
                            }
                ]
            }, {});
            $("#message-basic-modal-content").html(html);
            $("#message-basic-modal-content").dialog(ops);
            $("#message-basic-modal-content").siblings(".ui-dialog-titlebar").find(".ui-dialog-titlebar-close").addClass("ui-state-default");
            return false;
        },
        //加载中 statu : true-显示  false-隐藏
        loading: function (statu) {
            if (isparent) {
                if (statu == true) {
                    var temp = '<div class="alert fade in loading"><span></span>数据加载中……</div>'
                    showmsghtml(temp);
                } else {
                    $.msg.close();
                }
            } else {
                crossdomain("$.msg.loading", statu);
            }
        },
        //错误提示
        error: function (msg) {
            if (isparent) {
                var temp = '<div class="alert alert-error"><strong>提示！</strong>{{msg}}</div>';
                showmsghtml(temp.replaceo("msg", "<b style='color:red;'>" + msg + "</b>"));
                setTimeout("$.msg.close()", 2500);
            } else {
                crossdomain("$.msg.error", msg);
            }
        },
        //提示信息
        info: function (msg) {
            if (isparent) {
                var temp = '<div class="alert alert-info"><strong>提示！</strong>{{msg}}</div>';
                showmsghtml(temp.replaceo("msg", msg));
                setTimeout("$.msg.close()", 2000);
            } else {
                crossdomain("$.msg.info", msg);
            }
        },
        close: function () {
            $("#msgshow-modal-content").html("");
            $("#msgshow-modal-content").hide();
        }
    });
    /**
    * 显示提示信息
    * @method showmsghtml
    * @param {string} html 内容
    * @for modeljs
    */
    function showmsghtml(html) {
        if (!document.getElementById("msgshow-modal-content")) {
            $("body").append("<div id='msgshow-modal-content' style='width:200px;' ></div>");
        }
        if (!document.getElementById("styleshowmsghtml")) {
            $("#styleshowmodal").remove();
            $("body").append("<style id='styleshowmsghtml' >#msgshow-modal-content{z-index: 9999;height:35px;display:none;}</style>");
        }
        $("#msgshow-modal-content").html(html);
        var windowWidth = document.documentElement.clientWidth;
        var windowHeight = document.documentElement.clientHeight;
        //var popupHeight = $("#msgshow-modal-content").height();
        //var popupWidth = $("#msgshow-modal-content").width();
        var top = $(document).scrollTop();
        $("#msgshow-modal-content").css({
            "position": "absolute",
            "top": top,
            "left": (windowWidth - 100) / 2
        }).animate({ top: top + 5 }, 500);
        $("#msgshow-modal-content").show();
    }
})(jQuery);

/**
* 显示loading
* @method loading
* @param {bool} statu : true-显示  false-隐藏
* @for modeljs
*/
loading = function (statu) {
    if (isparent) {
        if (statu == true) {
            var temp = '<div class="meddileloading" ><div></div></div>'
            showMiddleModalHtml(temp);
        } else {
            closeMiddleModal();
        }
    } else {
        crossdomain("loading", statu);
    }
}


//无标题弹出框
function showMiddleModalHtml(html) {
    if (!document.getElementById("show-middle-modal-content")) {
        $("body").append("<div id='show-middle-modal-content' ></div>");
    }
    if (!document.getElementById("show-middle-modal-content-bg")) {
        $("body").append("<div id='show-middle-modal-content-bg' ></div>");
    }
    if (!document.getElementById("styleshowMiddlehtml")) {
        $("#styleshowMiddlehtml").remove();
        $("head").append("<style id='styleshowMiddlehtml' >#show-middle-modal-content{position:absolute;z-index: 9999;height:35px;display:none;}#show-middle-modal-content-bg{z-index: 9998;display:none;position: fixed;top: 0;left: 0;width: 100%;height: 100%;background: #666666  50% 50% repeat;opacity: .5;filter: Alpha(Opacity=50);}</style>");
    }
    $("#show-middle-modal-content").html(html);
    $("#show-middle-modal-content").show();
    $("#show-middle-modal-content-bg").show();
    var popupHeight = $("#show-middle-modal-content").height();
    var popupWidth = $("#show-middle-modal-content").children().width();
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;
    var top = $(document).scrollTop();
    $("#show-middle-modal-content").css({
        "top": ((windowHeight - popupHeight) / 2) + top,
        "left": (windowWidth - popupWidth) / 2
    });
}

(function ($) {
    $.fn.loading = function (status) {
        if (status) {
            if (!this.children(".localloading").get(0)) {
                $(this).append("<div class='localloading' ><div ></div></div>");
            }
            var bg = this.children("div.localloading");
            var popupHeight = bg.children("div").height();
            var popupWidth = bg.children("div").width();
            var windowWidth = this.width() + getwborder(this);
            var windowHeight = this.height() + gethborder(this);
            var top = $(this).scrollTop();
            var ctop = $(document).scrollTop();
            bg.show();
            var offset = this.offsetParent();
            this.css("position", "relative");
            bg.css({
                "width": windowWidth,
                "height": windowHeight,
                "top": offset.top - ctop
            });
            bg.children("div").css({
                "top": ((windowHeight - popupHeight) / 2) + top,
                "left": (windowWidth - popupWidth) / 2
            });
        } else {
            var bg = this.children("div.localloading");
            if (bg.get(0)) {
                bg.remove();
            }
        }
    };
})(jQuery);

//关闭无标题模态窗口
function closeMiddleModal() {
    $("#show-middle-modal-content").html("").hide();
    $("#show-middle-modal-content-bg").hide();
}