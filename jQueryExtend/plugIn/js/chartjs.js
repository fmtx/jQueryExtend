﻿/*!
 * 图表插件
 * 创建人：qinnailin
 * 创建时间：2014/8/25 
 *
 *
 * 
 */

(function ($) {
    $.extend({
        //初始化
        init: function (opstions) {
            var ops = $.extend(false, {
                'echarts': 'plugIn/charts/echarts',
                'echarts/chart/bar': 'plugIn/charts/echarts-map',
                'echarts/chart/line': 'plugIn/charts/echarts-map',
                'echarts/chart/pie': 'plugIn/charts/echarts-map',
                'echarts/chart/radar': 'plugIn/charts/echarts-map'
            }, opstions);
            require.config({
                paths: ops
            });
        }
    });
    $.fn.extend({
        //柱状图
        bar: function (opstions) {
            var ops = $.extend(true, {
                title: "",
                subtitle: "",
                showtype: 'V',
                data: null,
                url: "",
                afterCreate: null,
                showtag: true,//是否显示标签
                async: {
                    enable:false,
                    url: "",
                    params: {},
                    filer: "Id",
                    name: "-",
                    sleep: 1000,
                    size: 20,
                    insert:false
                }
            }, opstions);
            var objthis = $(this);
            var myChart;
            var lastid;
            if (ops.url != "") {
                $.getJSON(ops.url, function (data) {
                    ops.data = data;
                    var option = loadData();
                    drawing(option);
                });
            } else {
                var option = loadData();
                drawing(option);
            }
            /**
            * 读取数据并渲染
            * @method loadData
            * @for chartjs
            */
            function loadData() {
                var xtype, ytype, xdata, ydata;
                if ((ops.data == null || ops.data.length == 0) && ops.async.enable) {
                    ops.data = $.initEmpty([ops.async.name], ops.async.size);
                }
                if (ops.showtype == "V" || ops.showtype == "v") {
                    xtype = 'category';
                    ytype = 'value';
                    xdata = ops.data.category;
                    ydata = null;
                } else {
                    xtype = 'value';
                    ytype = 'category';
                    xdata = null
                    ydata = ops.data.category;
                }
                var data = new Array();
                var legends = new Array();
                $.each(ops.data.values, function (i, n) {
                    data.push({ name: n.name, type: 'bar', data: n.value });
                    if (ops.async.enable) {
                        lastid = n[ops.async.filer];
                    }
                    legends.push(n.name);
                });

                var option = {
                    title: {
                        text: ops.title,
                        subtext: ops.subtitle
                    },
                    tooltip: {
                        show: true
                    },
                    legend: {
                        data: legends
                    },
                    xAxis: [
                        {
                            type: xtype,
                            data: xdata
                        }
                    ],
                    yAxis: [
                        {
                            type: ytype,
                            data: ydata
                        }
                    ],
                    series: data
                };
                if (!ops.showtag ) {
                    option.legend = null;
                }
                if (!ops.showtag && ops.title == "" && ops.subtitle == "") {
                    option["grid"] = {
                        y: 10,
                        x: 30,
                        y2: 30,
                        x2: 10
                    };
                }
                return option;
            }
            function drawing(option) {
                require(
                [
                    'echarts',
                    'echarts/chart/bar'
                ],
                function (ec) {
                    myChart = ec.init($(objthis).get(0));
                    myChart.setOption(option);
                    if (ops.afterCreate) {
                        ops.afterCreate(myChart);
                    }
                    if (ops.async.enable) {
                        var timeTicket;
                        clearInterval(timeTicket);
                        timeTicket = setInterval(function () {
                            var params = ops.async.params;
                            params["filer"] = lastid;
                            $.getJSON(ops.async.url, params, function (data) {
                                $.each(data, function (i, n) {
                                    myChart.addData([
                                       [
                                           0,        // 系列索引
                                           n.value,       // 新增数据
                                           true,     // 新增数据是否从队列头部插入
                                           ops.async.insert,     // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
                                           n.key
                                       ]
                                    ]);
                                    lastid = n.value;
                                });
                            });
                        }, ops.async.sleep);
                    }
                });
            }
            var func = {
                reload: function (opst) {
                    if (!myChart) { $.msg.alert("请等待图标加载完毕后调用！"); return false; }
                    myChart.clear();
                    if (opst instanceof Object) {
                        ops.data = opst;
                        var option = loadData();
                        myChart.setOption(option);
                        myChart.refresh();
                    } else {
                        ops.data = null;
                        ops.url = opst;
                        $.getJSON(ops.url, function (data) {
                            ops.data = data;
                            var option = loadData();
                            myChart.setOption(option);
                            myChart.refresh();
                        });
                    }
                }
            }
            return func;
        },
        //折线图
        line: function (opstions) {
            var ops = $.extend(true, {
                title: "",
                subtitle: "",
                data: null,
                showtype: 'V',
                url: "",
                symbol: '',//none//节点样式
                smooth: true,//是否平滑显示
                showtag: true,//是否显示标签
                dataZoom: {
                    show: false,
                    realtime: true,
                    start: 0,
                    end: 100
                },
                afterCreate: null,
                async: {
                    enable: false,
                    url: "",
                    params: {},
                    filer: "Id",
                    name:"-",
                    sleep: 1000,
                    size:20,
                    insert: false
                }
            }, opstions);
            var objthis = $(this);
            var myChart;
            var lastid;
            if (ops.url != "") {
                $.getJSON(ops.url, function (data) {
                    ops.data = data;
                    var option = loadData();
                    drawing(option);
                    
                });
            } else {
                var option = loadData();
                drawing(option);
            }
            /**
            * 读取数据并渲染
            * @method loadData
            * @for chartjs
            */
            function loadData() {
                var xAxis, yAxis;

                if ((ops.data == null || ops.data.length == 0) && ops.async.enable) {
                    ops.data = $.initEmpty([ops.async.name], ops.async.size);
                }

                if (ops.showtype == "V" || ops.showtype == "v") {
                    xAxis = { type: 'category', boundaryGap: false, data: ops.data.category };
                    yAxis = { type: 'value' };
                } else {
                    yAxis = { type: 'category', boundaryGap: false, data: ops.data.category };
                    xAxis = { type: 'value' };
                }
                var data = new Array();
                var legends = new Array();//legend=null为不显示
                $.each(ops.data.values, function (i, n) {
                    data.push({ name: n.name, type: 'line', symbol: ops.symbol, smooth: ops.smooth, data: n.value });
                    if (ops.async.enable) {
                        lastid = n[ops.async.filer];
                    }
                    legends.push(n.name);
                });

                var option = {
                    title: {
                        text: ops.title,
                        subtext: ops.subtitle
                    },
                    tooltip: {
                        show: true,
                        trigger: 'axis'
                    },
                    dataZoom: ops.dataZoom,
                    legend: {
                        data: legends
                    },
                    xAxis: [
                        xAxis
                    ],
                    yAxis: [
                        yAxis
                    ],
                    series: data
                };
                if (!ops.showtag ) {
                    option.legend = null;
                }
                if (!ops.showtag && ops.title == "" && ops.subtitle == "") {
                    option["grid"] = {
                        y: 10,
                        x: 30,
                        y2: 30,
                        x2: 10
                    };
                }
                return option;
            }
            function drawing(option) {
                require(
                [
                    'echarts',
                    'echarts/chart/line'
                ],
                function (ec) {
                    myChart = ec.init($(objthis).get(0));
                    myChart.setOption(option);
                    if (ops.afterCreate) {
                        ops.afterCreate(myChart);
                    }
                    if (ops.async.enable) {
                        var timeTicket;
                        clearInterval(timeTicket);
                        timeTicket = setInterval(function () {
                            var params = ops.async.params;
                            params["filer"] = lastid;
                            $.getJSON(ops.async.url, params, function (data) {
                                $.each(data, function (i, n) {
                                    myChart.addData([
                                       [
                                           0,        // 系列索引
                                           n.value,       // 新增数据
                                           true,     // 新增数据是否从队列头部插入
                                           ops.async.insert,     // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
                                           n.key
                                       ]
                                    ]);
                                    lastid = n.value;
                                });
                            });
                        }, ops.async.sleep);
                    }
                });
            }
            var func = {
                reload: function (opst) {
                    if (!myChart) { $.msg.alert("请等待图标加载完毕后调用！"); return false; }
                    myChart.clear();
                    if (opst instanceof Object) {
                        ops.data = opst;
                        var option = loadData();
                        myChart.setOption(option);
                        myChart.refresh();
                    } else {
                        ops.data = null;
                        ops.url = opst;
                        myChart.showLoading();
                        $.getJSON(ops.url, function (data) {
                            ops.data = data;
                            var option = loadData();
                            myChart.setOption(option);
                            myChart.refresh();
                            myChart.hideLoading();
                        });
                    }
                }
            }
            return func;
        },
        //饼图
        pie: function (opstions) {
            var ops = $.extend(true, {
                title: '',
                titleX: "center",
                subtitle: '',
                data: null,
                url: '',
                legendPos: "left",
                showlable: true,
                radius:"55%",
                itemStyle: {
                    normal: {
                        label: { show: true },
                        labelLine: { show: true }
                    }
                },
                afterCreate: null
            }, opstions);
            var myChart;
            var objthis = $(this);
            if (ops.url != "") {
                $.getJSON(ops.url, function (data) {
                    ops.data = data;
                    loadData();
                });
            } else {
                loadData();
            }
            
            /**
            * 读取数据并渲染
            * @method loadData
            * @for chartjs
            */
            function loadData() {
                var data = new Array();
                var legends = new Array();
                $.each(ops.data, function (i, n) {
                    legends.push(n.name);
                });

                var dataStyle = {
                    normal: {
                        label: { show: true },
                        labelLine: { show: true }
                    }
                };
                if (!ops.showlable) {
                    dataStyle = {
                        normal: {
                            label: { show: false },
                            labelLine: { show: false }
                        }
                    };
                }
                var option = {
                    title: {
                        text: ops.title,
                        subtext: ops.subtitle,
                        x: ops.titleX
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'vertical',
                        x: ops.legendPos,
                        data: legends
                    },
                    series: [
                        {
                            type: 'pie',
                            itemStyle: dataStyle,
                            radius: ops.radius,
                            center: ['50%', (ops.title==null||ops.title==""?"50%":"60%")],
                            data: ops.data
                        }
                    ]
                };
                require(
                [
                    'echarts',
                    'echarts/chart/pie'
                ],
                function (ec) {
                    myChart = ec.init($(objthis).get(0));
                    myChart.setOption(option);
                    if (ops.afterCreate) {
                        ops.afterCreate(myChart);
                    }
                });
                return option;
            }
            var func = {
                reload: function (opst) {
                    if (!myChart) { $.msg.alert("请等待图标加载完毕后调用！"); return false; }
                    myChart.clear();
                    if (opst instanceof Object) {
                        ops.data = opst;
                        var option = loadData();
                        myChart.setOption(option);
                        myChart.refresh();
                    } else {
                        ops.data = null;
                        ops.url = opst;
                        myChart.showLoading();
                        $.getJSON(ops.url, function (data) {
                            ops.data = data;
                            var option = loadData();
                            myChart.setOption(option);
                            myChart.refresh();
                            myChart.hideLoading();
                        });
                    }
                }
            }
            return func;
        },
        //雷达图
        radar: function (opstions) {
            var ops = $.extend(true, {
                title: '',
                subtitle: '',
                style: null,
                data: null,
                url: '',
                afterCreate: null
            }, opstions);
            var myChart;
            var objthis = $(this);
            if (ops.url != "") {
                $.getJSON(ops.url, function (data) {
                    ops.data = data;
                    loadData();
                });
            } else {
                loadData();
            }
            /**
            * 读取数据并渲染
            * @method loadData
            * @for chartjs
            */
            function loadData() {
                var data = new Array();
                var legends = new Array();
                $.each(ops.data.values, function (i, n) {
                    legends.push(n.name);
                });

                var areaStyle = {};
                if (ops.style == 'fill') {
                    areaStyle = {
                        areaStyle: {
                            type: 'default'
                        }
                    }
                }

                var option = {
                    title: {
                        text: ops.title,
                        subtext: ops.subtitle
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        orient: 'vertical',
                        x: 'right',
                        y: 'bottom',
                        data: legends
                    },
                    polar: [
                       {
                           indicator: ops.data.indicator
                       }
                    ],
                    series: [
                        {
                            type: 'radar',
                            itemStyle: {
                                normal: areaStyle
                            },
                            data: ops.data.values
                        }
                    ]
                };
                require(
                [
                    'echarts',
                    'echarts/chart/radar'
                ],
                function (ec) {
                    myChart = ec.init($(objthis).get(0));
                    myChart.setOption(option);
                    if (ops.afterCreate) {
                        ops.afterCreate(myChart);
                    }
                });
                return option;
            }
            var func = {
                reload: function (opst) {
                    if (!myChart) { $.msg.alert("请等待图标加载完毕后调用！"); return false; }
                    myChart.clear();
                    if (opst instanceof Object) {
                        ops.data = opst;
                        var option = loadData();
                        myChart.setOption(option);
                        myChart.refresh();
                    } else {
                        ops.data = null;
                        ops.url = opst;
                        myChart.showLoading();
                        $.getJSON(ops.url, function (data) {
                            ops.data = data;
                            var option = loadData();
                            myChart.setOption(option);
                            myChart.refresh();
                            myChart.hideLoading();
                        });
                    }
                }
            }
            return func;
        },
        //自定义模式(超级模式)
        charts: function (name, option, afterCreate) {
            var objthis = $(this);
            var myChart;
            require(
                [
                    'echarts',
                    'echarts/chart/' + name
                ],
                function (ec) {
                    myChart = ec.init($(objthis).get(0));
                    myChart.setOption(option);
                    if (afterCreate) {
                        afterCreate(myChart);
                    }
                });
            var func = {
                reload: function (opst) {
                    if (!myChart) { $.msg.alert("请等待图标加载完毕后调用！"); return false; }
                    myChart.clear();
                    myChart.setOption(opst);
                    myChart.refresh();
                }
            }
            return func;
        }
    });
    $.extend({
        initEmpty: function (names, size) {
            var titles = new Array();
            var value = new Array();
            for (var i = 0; i < size; i++) {
                titles.push("-");
                value.push(0);
            }
            var values = new Array();
            $.each(names, function (i, n) {
                values.push({ name: n, value: value });
            });
            
            var data = {
                category: titles,
                values: values
            };
            return data;
        }
    });
})(jQuery);
