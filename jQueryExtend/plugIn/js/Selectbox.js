﻿/*!
 * selectbox控件
 * 创建人：qinnailin
 * 创建时间：2014/11/13 
 *
 *
 * 
 */
!(function ($) {
    "use strict"
    var selectbox = function (element, options) {
        this.init("selectbox", element, options);
    }
    var isin = 0;

    selectbox.prototype = {
        constructor:selectbox,
        init: function (type, element, options) {
            this.type = type;
            this.$element = $(element);
            this.options = this.getOptions(options);
            if (!$(element).hasClass("fmSelectbox")) {
                $(element).addClass("fmSelectbox");
            }
            var width = $(element).width();
            var height = $(element).height();
            var objname = $(element).attr("fm-name");
            var empty = false;
            var selthis;
            var $this = this;
            if (!$(element).children("select").get(0)) {
                $(element).append("<select style='display:none;' name='" + objname + "' ></select>");
                empty = true;
                selthis = $(element).children("select");
                //if (this.options.defaults) {
                //    $(selthis).append("<option value='" + this.options.defaults[1] + "' >" + this.options.defaults[0] + "</option>");
                //}
            } else {
                selthis = $(element).children("select");
                if (!$(selthis).attr("name")) {
                    $(selthis).attr("name", $(element).attr("fm-name"));
                }
                $(selthis).hide();
            }
            this.selthis = selthis;
            //兼容旧的使用习惯
            if ($(element).children("ul").get(0)) {
                $(selthis).empty();
                $(element).find("ul>li").each(function (i, n) {
                    var val = $(this).attr("val");
                    var test = $(this).text();
                    $(selthis).append("<option value='" + val + "' >" + test + "</option>");
                });
                $(element).children("ul").remove();
            }
            //兼容旧的使用习惯
            this.defaultval = this.options.defaults ? this.options.defaults[1] : $(selthis).val();
            this.defaulttext = this.options.defaults ? this.options.defaults[0] : $(selthis).children("option[value='" + this.defaultval + "']").text();
            $(element).append("<span style='height:" + height + "px;' >" + this.defaulttext + "<em></em></span>");
            var spanthis = $(element).children("span");
            this.spanthis = spanthis;
            $(element).append("<div style='width:" + (width - 2) + "px;display:none;'><ul class='ul_sel'></ul></div>");
            var showbox = $(element).children("div");
            if (empty) {
                if (this.options.url == "" && this.options.data != null) {
                    this.loadData();
                } else if (this.options.url != "") {
                    var ops = this.options;
                    $.getJSON(ops.url, function (rd) {
                        var data;
                        if (ops.source == "") {
                            data = rd;
                        } else {
                            var temp = rd;
                            var item = ops.source.split('.');
                            $.each(item, function (i, n) {
                                if (n != "") {
                                    temp = temp[n];
                                }
                            });
                            data = temp;
                        }
                        $this.setData(data);
                        $this.loadData();
                    });
                }
            }
            $(spanthis).click(function (e) {
                if ($(element).hasClass("disabled")) {
                    return false;
                }
                if ($(showbox).find("li").length > 0) {
                    $this.hideshowbox();
                    e.stopPropagation();
                } else {
                    $this.showlist();
                    e.stopPropagation();
                }
            });
            
            $(element).on("click", "div>ul>li", function () {
                var val = $(this).attr("val");
                var text = $(this).text();
                $this.setvalue(val, text);
                $this.hideshowbox();
                if ($this.options.change) {
                    $this.options.change($(selthis).val());
                }
            });

            //body点击事件
            $(document).click(function () {
                if (isin <= 0) {
                    $this.hideshowbox();
                }
            });
            //鼠标进进出出
            $(element).hover(function () {
                isin++;
            }, function () {
                isin--;
            });
        },
        reqdata: function () {
            var $this = this;
            var ops = this.options;
            $.getJSON(ops.url, function (rd) {
                var data;
                if (ops.source == "") {
                    data = rd;
                } else {
                    var temp = rd;
                    var item = ops.source.split('.');
                    $.each(item, function (i, n) {
                        if (n != "") {
                            temp = temp[n];
                        }
                    });
                    data = temp;
                }
                $this.setData(data);
                $this.loadData();
            });
        },
        getOptions: function (options) {
            return $.extend({}, $.fn[this.type].defaults, options, this.$element.data());
        },
        loadData: function () {
            var ops = this.options;
            if (ops == undefined) return false;
            var $selthis = this.selthis;
            if (this.options.defaults) {
                $selthis.append("<option value='" + this.options.defaults[1] + "' >" + this.options.defaults[0] + "</option>");
            }
            $.each(ops.data, function (i, n) {
                $selthis.append("<option value='" + n[ops.param.key] + "'>" + n[ops.param.text] + "</option>");
            });
            if (this.defaultval != $(this.selthis).val()) {
                this.defaultval = $(this.selthis).val();
                this.defaulttext = $(this.selthis).children("option[value='" + this.defaultval + "']").text();
                this.spanthis.text(this.defaulttext).append("<em></em>");
            }

            if (this.options.initval) {
                var text = $(this.selthis).children("option[value='" + this.options.initval + "']").text();
                this.setvalue(this.options.initval, text);
            }
            if (this.options.afterCreate) {
                this.options.afterCreate();
            }
        },
        setvalue: function (val, text) {
            $(this.selthis).children("option").siblings().removeAttr("selected");
            $(this.selthis).children("option[value=" + val + "]").attr("selected", "true");
            $(this.spanthis).text(text).append("<em></em>");
        },
        showlist: function () {
            var showbox = this.$element.children("div");
            $(".fmSelectbox>div").hide();
            $(showbox).children("ul").html("");
            $(this.selthis).children("option").each(function (i, n) {
                if ($(n).val() != $(this.selthis).val()) {
                    $(showbox).children("ul").append("<li val='" + $(n).val() + "' >" + $(n).text() + "</li>");
                }
            });
            var liheight = parseFloat($(showbox).find("ul>li").eq(0).height());
            if (this.options.height > 0) {
                if ($(showbox).find("ul>li").size() * liheight > this.options.height) {
                    $(showbox).height(this.options.height);
                    $(showbox).css("overflow-y", "scroll");
                }
            } else {
                if ($(showbox).find("ul>li").size() <= 10) {
                    $(showbox).height($(showbox).find("ul>li").size() * liheight);
                    $(showbox).css("overflow-y", "hidden");
                } else {
                    $(showbox).height(10 * liheight);
                    $(showbox).css("overflow-y", "scroll");
                }
            }
            if ($(showbox).find("ul>li").size() > 0) {
                $(showbox).show();
            }
        },
        setData:function(data){
            this.options.data = data;
        },
        hideshowbox: function () {
            var showbox = this.$element.children("div");
            $(showbox).hide();
            $(showbox).children("ul").empty();
            isin = 0;
        },
        getVal: function () {
            return this.selthis.val();
        },
        getText: function () {
            return this.spanthis.text();
        },
        setSelected: function (key) {
            var text = this.selthis.children("option[value='" + key + "']").text();
            this.setvalue(key, text);
        },
        clear: function () {
            var showbox = this.$element.children("div");
            $(showbox).children("ul").html("");
            this.selthis.html("");
            this.spanthis.text("").append("<em></em>");
            this.defaultval = "";
        },
        reload: function (obj) {
            this.clear();
            if (obj instanceof Object) {
                this.options.data = obj;
                this.loadData();
                var val = this.selthis.val();
                if (this.options.change) {
                    this.options.change(val);
                }
            } else {
                var ops = this.options;
                var $this = this;
                $.getJSON(obj, function (rd) {
                    var data;
                    if (ops.source == "") {
                        data = rd;
                    } else {
                        var temp = rd;
                        var item = ops.source.split('.');
                        $.each(item, function (i, n) {
                            if (n != "") {
                                temp = temp[n];
                            }
                        });
                        data = temp;
                    }
                    $this.setData(data);
                    $this.loadData();
                    var val = $this.selthis.val();
                    if (ops.change) {
                        ops.change(val);
                    }
                });
            }
        }
    }
    //插件入口
    $.fn.selectbox = function (options) {
        var $this = $(this);
        var data = $this.data("selectbox");
        if (!data) {
            $this.data("selectbox", (data = new selectbox(this, options)));
        } else {
            data.options = $.extend(false, $this.selectbox.defaults, options);
            data.clear();
            if (data.options.data != null) {
                data.loadData();
            } else {
                data.reqdata();
            }
        }
        return data;
    }
    //初始值
    $.fn.selectbox.defaults = {
        url: "",
        data: null,
        source: "",
        defaults: ["--请选择--", -1],
        param: { key: "id", text: "name" },
        height:0,
        change: null,
        initval: null,
        afterCreate: null
    }

})(window.jQuery);