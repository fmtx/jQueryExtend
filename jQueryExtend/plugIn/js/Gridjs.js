﻿/*!
 * 列表控件 带分页的表格控件
 *创建人：qinnailin
 * 创建时间：2014/7/4 
 */

(function ($) {
    $.fn.Grid = function (options) {
        var ops = $.extend(true,{
            url: "",
            data: null,
            dataformat: {data:"Data",count:"Count"},
            click: null,
            dbclick: null,
            method: "Get",
            dataType: "json",
            sortMultiple: true,
            putJson: false,
            cache: false,
            append: false,
            template:null,
            page: {
                enable:false,
                click:null,
                pageSize: 30,
                pageIndex: 1,
                back: "上一页",
                next: "下一页",
                reqindex: "index",
                reqsize: "size",
                lazy: null
            },
            afterCreate: null,
            afterLoad: null,
            beforLoad:null
        }, options );

        var objthis = $(this);
        var thisid = $(objthis).attr("id");
        var template = ops.template;
        var ajaxData;
        if (!document.getElementById(thisid + "_template")) {
            if(!template)
                template = findTemp(objthis, ops);
            $("body").append("<script type='text/template' id='" + thisid + "_template' >" + template + "</script>");
        } else {
            template = $("#" + thisid + "_template").html();
        }
        var tempbox = findTempBox(objthis);
        tempbox.html("");
        var pagecount = 0;
        var listcount = 0;
        var pageIndex = 1;
        var requesturl = "";
        var sortdata = {};
        var rownum = 1;

        /**
         * 方法组
         */
        var func = {
            reload: function () {
                pageIndex = 1;
                if ($(objthis).find("[fm-pagerbox]").get(0)) {
                    $(objthis).find("[fm-pagerbox]").find(".pageindex").text(1);
                }
                initReqUrl();
                loaddata();
            },
            refresh: function () {
                rownum = ((pageIndex - 1) * ops.page.pageSize) + 1;
                ops.data = null;
                loaddata();
            },
            load: function (opst) {
                if (opst == null||opst==undefined) return;
                if(opst instanceof Object){
                    ops.data = opst;
                    ops.url = "";
                } else {
                    ops.data = null;
                     ops.url=opst;
                 }
                pageIndex = 1;
                if ($(objthis).find("[fm-pagerbox]").get(0)) {
                    $(objthis).find("[fm-pagerbox]").find(".pageindex").text(1);
                }
                initReqUrl();
                loaddata();
            },
            getData:function(){
                return ops.data;
            },
            getAjaxData: function () {
                return ajaxData;
            },
            loadNext: function () {
                var num = pageIndex+1;
                if (num <= pagecount) {
                    pageIndex = num;
                    if ($(objthis).find("[fm-pagerbox]").get(0)) {
                        $(objthis).find("[fm-pagerbox]").find(".pageindex").text(num);
                    }
                    initReqUrl();
                    loaddata();
                    return true;
                } else {
                    return false;
                }
            }
        }
        /**
         * 初始化请求url
         * @method initReqUrl
         * @for Gridjs
         */
        function initReqUrl(){
            var pagediv = $(objthis).find('[fm-pagerbox]').get(0);
            if(pagediv){
                var urlparam = ops.page.reqindex + "=" + ((ops.page.pageIndex - 1) + pageIndex) + "&" + ops.page.reqsize + "=" + ops.page.pageSize;
                if (ops.url.indexOf('?')>-1) {
                    requesturl = ops.url + "&" + urlparam;
                } else {
                    requesturl = ops.url + "?" + urlparam;
                }
            }else{
                requesturl=ops.url;
            }
            if (pageIndex == 1) {
                rownum = 1;
            }
            rownum = ((pageIndex - 1) * ops.page.pageSize) + 1;
        }
        initReqUrl();

        loaddata();
        /**
         * 数据加载
         * @method loaddata
         * @for Gridjs
         */
        function loaddata() {
            if (ops.url == "" && ops.data != null) {
                fillData(ops, template);
                if (ops.page.lazy) {
                    ops.url = ops.page.lazy.url;
                    listcount = ops.page.lazy.count;
                    bindPager();
                }
                converfunc();
                if (ops.afterCreate) {
                    ops.afterCreate();
                }
            } else if (ops.url != "") {
                var daval;
                if (!$(objthis).find("form[fm-search]").get(0)) {
                    $(objthis).append("<form fm-search ></form>");
                }
                if ($(objthis).find("form[fm-search]").find("input[name=sort]").get(0)) {
                    if (jsonsize(sortdata) > 0)
                        $(objthis).find("form[fm-search]").find("input[name=sort]").val(stringify(sortdata));
                } else {
                    if (jsonsize(sortdata) > 0) {
                        $(objthis).find("form[fm-search]").append("<input type='hidden' name='sort' value='" + stringify(sortdata) + "' />");
                    }
                }
                daval = $(objthis).find("[fm-search]").serialize();
                if (ops.beforLoad)
                {
                    ops.beforLoad(pageIndex,ops.page.pageSize);
                }
                $.ajax({
                    url: requesturl,
                    data: daval,
                    type: ops.method,
                    dataType: ops.dataType,
                    cache:ops.cache,
                    success: function (result) {
                        ajaxData = result;
                        listcount = result[ops.dataformat.count];
                        ops.data = result[ops.dataformat.data];
                        fillData(ops, template);
                        converfunc();
                        var pager=$(objthis).find("[fm-pagerbox]").html();
                        if (pager != undefined) {
                            pagecount = GetCountPage(listcount, ops.page.pageSize);
                            $(objthis).find("[fm-pagerbox]").find(".pagecount").text(pagecount);
                            if (pagecount == pageIndex) {
                                $(objthis).find("[fm-pagerbox]").find(".next").addClass("active");
                            } else {
                                $(objthis).find("[fm-pagerbox]").find(".next").removeClass("active");
                            }
                        }
                        if (ops.afterCreate) {
                            ops.afterCreate(result);
                        }
                        if (ops.afterLoad) {
                            ops.afterLoad(result);
                        }
                    }
                });
            }
        }

        /**
         * 分页绑定
         * @method bindPager
         * @for Gridjs
         */
        function bindPager() {
            if (ops.page.enable)
            {
                var pages = '<p class="page_num"><a href="javascript:" class="page_pre back"></a>' +
            '<span class="fir_page pageindex">1</span><span class="line_page">/</span><span class="las_page pagecount">1</span>' +
            '<a href="javascript:" class="page_nex next"></a><input type="text" class="goTo_page" />' +
            '<input type="button" class="btn gotobtn" value="跳页" /></p>';
                $(objthis).find("[fm-pagerbox]").html(pages);
            }
        }

        /**
         * 转换控制
         * @method converfunc
         * @for Gridjs
         */
        function converfunc() {
            var clist = $(objthis).find("[fm-conver]");
            $.each(clist, function (i, n) {
                var txt = $(n).html()
                var convfn = $(n).attr("fm-conver");
                var fn = eval(convfn)
                var cvres = fn(txt);
                $(n).html(cvres);
            });
        }

        /**
         * 页面加载
         * @for Gridjs
         */
        $(document).ready(function () {
            bindPager();
            if (ops.click != null) {
                $(objthis).on("click", "[fm-row]", function () {
                    var id = $(this).attr("fm-row");
                    var data = ops.data[id];
                    ops.click(data);
                });
            }
            if (ops.dbclick != null) {
                $(objthis).on("dblclick", "[fm-row]", function () {
                    var id = $(this).attr("fm-row");
                    var data = ops.data[id];
                    ops.dbclick(data);
                });
            }
            $(objthis).on("click", "[fm-pagerbox]>p>a", function () {
                if ($(this).hasClass("active")) return;
                if ($(this).hasClass("back")) {
                    if (pageIndex == 1) {
                        $(this).addClass("active");
                        return;
                    }
                    if (--pageIndex > 1) {
                        $(this).removeClass("active");
                    }
                    
                    $(objthis).find("[fm-pagerbox]").find(".next").removeClass("active");
                    $(objthis).find("[fm-pagerbox]").find(".pageindex").text(pageIndex);
                    bindPageEvent();
                } else if ($(this).hasClass("next")) {
                    if (++pageIndex < pagecount) {
                        $(this).removeClass("active");
                    }
                    $(objthis).find("[fm-pagerbox]").find(".back").removeClass("active");
                    $(objthis).find("[fm-pagerbox]").find(".pageindex").text(pageIndex);
                    bindPageEvent();
                } 
            });

            $(objthis).on("click", "[fm-pagerbox]>p>.gotobtn", function () {
                var val = parseInt($(objthis).find("[fm-pagerbox]>p>input.goTo_page").val());
                if (pageIndex != val&&val>=ops.page.pageIndex&&val<=pagecount) {
                    pageIndex = val;
                    initReqUrl();
                    loaddata();
                    if (ops.page.click) {
                        ops.page.click(pageIndex, ops.page.pageSize, listcount);
                    }
                }
                $(objthis).find("[fm-pagerbox]").find(".pageindex").text(pageIndex);
            });

            $(objthis).find("form[fm-search]").eq(0).submit(function () {
                func.reload();
                return false;
            });

            $(objthis).find("[fm-sort]").addClass("sort_head");

            $(objthis).find("[fm-sort]").each(function (i,n) {
                $(n).append("<a class='sort_pre_next' ></a>");
            });

            $(objthis).on("click", "[fm-sort]", function () {
                if (!ops.sortMultiple) {
                    $(this).siblings("[fm-sort]").children("a").removeClass("sort_pre").remove("sort_next").addClass("sort_pre_next");
                }
                var value = -1;
                var oth = $(this).children("a");
                if ($(oth).hasClass("sort_pre_next")) {
                    $(oth).removeClass("sort_pre_next").addClass("sort_pre");
                    value = 0;
                } else if ($(oth).hasClass("sort_pre")) {
                    $(oth).removeClass("sort_pre").addClass("sort_next");
                    value = 1;
                } else if ($(oth).hasClass("sort_next")) {
                    $(oth).removeClass("sort_next").addClass("sort_pre_next");
                    value = -1;
                }
                sortsetdata($(this), value);
            });

        });

        /**
         * json大小
         * @method jsonsize
         * @for Gridjs
         */
        function jsonsize(obj) {
            var count = 0;
            for (var i in obj) {
                count++;
            }
            return count;
        }

        /**
         * 设置排序参数
         * @method sortsetUrl
         * @for Gridjs
         */
        function sortsetdata(obj,val) {
            if (!ops.sortMultiple) {
                sortdata = {};
            }
            var key = $(obj).attr("fm-sort");
            sortdata["\""+key+"\""] = val;
            if (val = -1) {
                try { delete sortdata[key]; } catch (e) { }//只有在非ie和ie9以上才不会报错
            }
            pageIndex = 1;
            initReqUrl();
            if ($(objthis).find("[fm-pagerbox]").get(0)) {
                $(objthis).find("[fm-pagerbox]").find(".pageindex").text(1);
            }
            var str = stringify(sortdata).replace(/\\"/g, "");
            sortdata = stringToJSON(str);
            loaddata();
        }

        

        /**
         * 点击分页
         * @method pageChange
         * @for Gridjs
         */
        function pageChange() {
            initReqUrl();
            loaddata();
        }

        /**
        * 分页绑定事件
        * @method bindPageEvent
        * @for Gridjs
        */
        function bindPageEvent() {
            pageChange();
            if (ops.page.click != null) {
                ops.page.click(pageIndex, ops.page.pageSize, listcount);
            }
        }
        /**
    * 计算页数
    * @method GetCountPage
    * @param {int} pageCount 数据总项数
    * @param {int} pageSize 分页大小
    * @for Gridjs
    */
        function GetCountPage(pageCount, pageSize) {
            if (pageCount < pageSize) {
                return 1;
            } else if (pageCount > 0) {
                if (pageCount % pageSize == 0) {
                    return maxpage = pageCount / pageSize;
                } else {
                    return parseInt((pageCount / pageSize)) + 1;
                }
            }
            return 0;
        }
        
        /**
        * 填充数据
        * @method fillData
        * @param {object} ops 控件参数对象
        * @param {string} template 填充模版
        * @for Gridjs
        */
        function fillData(ops, template) {
            var d = ops.data;
            if (ops.append == false)
            {
                $(tempbox).empty();
            }
            if (d && d.length > 0) {
                try {
                    $.each(d, function (i, n) {
                        $(tempbox).append($(template.fill(n).replaceo("fm-row-num", i).replaceo("rowNumber", rownum++).fillEmpty()).data("jsondata", n));
                    });
                } catch (e) {
                    console.debug(template);
                    console.debug(e);
                }
            }
        }

        /**
        * 添加行标（获取行号）
        * @method setTempRow
        * @param {object} temp jquery对象
        * @for Gridjs
        */
        function setTempRow(temp) {
            var obj = $(temp).attr("fm-row", "{{fm-row-num}}").get(0);
            var div = document.createElement("div");
            div.appendChild(obj);
            return $(div).html();
        }

        /**
        * 查找模版
        * @method fillData
        * @param {object} obj jquery对象
        * @param {object} ops 控件参数对象
        * @for Gridjs
        */
        function findTemp(obj, ops) {
            var temphtml = $(obj).find("[fm-body]").html();
            if (ops.click != null || ops.dbclick != null) {
                temphtml = setTempRow(temphtml);
            }
            return temphtml;
        }

        /**
        * 查找模板外壳
        * @method fillData
        * @param {object} obj jquery对象
        * @for Gridjs
        */
        function findTempBox(obj) {
            return $(obj).find("[fm-body]");
        }
        return func;
    }

    

})(jQuery);
