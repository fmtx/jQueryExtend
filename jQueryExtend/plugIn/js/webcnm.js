﻿

!(function ($) {
    "use strict"

    var webcnm = function (opstions) {
        this.init(opstions);
    }

    webcnm.prototype = {
        ws: null,
        init: function (opstions) {
            var ops;
            this.opstions = ops = this.getOpstions(opstions);
            if (this.checkBrowser()) {
                this.ws = this.openWS(ops.callback, ops.error, ops.close);
            } else {//不支持websocket的情况下
                this.doAjaxRequest(ops.callback, ops.error);
            }
        },
        getOpstions:function(opstions)
        {
            return $.extend(true,$.webcnm.defaults, opstions);
        },
        doAjaxRequest: function (callback, error) {
            var ops = this.opstions,$this;
            $this = this;
            $.ajax({
                type: ops.type,
                url: ops.url,
                dataType: ops.dataType,
                timeout: ops.timeout,
                data: { webComet:true,tim:Math.random() },
                success: function (data, textStatus) {
                    $this.doAjaxRequest(callback, error);
                },
                complete: function (XMLHttpRequest, textStatus) {
                    if (XMLHttpRequest.readyState == "4") {
                        callback(XMLHttpRequest.responseText);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (textStatus == "timeout")
                        $this.doAjaxRequest(callback, error);
                    else
                        error("error:" + textStatus);
                }
            });
        },
        checkBrowser: function () {
            if (window.WebSocket) {
                return true;
            } else {
                return false;
            }
        },
        openWS: function (callback, error, close)
        {
            var ws;
            ws = new WebSocket(this.opstions.ws);
            ws.onopen = function () {
                console.info("服务器链接成功！");
            };
            ws.onmessage = function (answ) {
                callback(answ.data);
            }
            ws.onerror = function (err) {
                console.info(err.error);
                if (error) { error(err); }
            };
            ws.onclose = function () {
                console.info("服务关闭！");
                if (close) { close(); }
            };
            return ws;
        },
        send: function (data) {
            if (this.ws) {
                this.ws.send(data);
            } else {
                var ops = this.opstions, $this;
                $this = this;
                $.ajax({
                    type: ops.type,
                    url: ops.url,
                    dataType: ops.dataType,
                    data: data,
                    success: function (data, textStatus) {

                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        ops.callback(XMLHttpRequest.responseText);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        ops.error("error:" + textStatus);
                    }
                });
            }
        }

    };



    $.webcnm = function (opstions)
    {
        var objx = $("body").data("websocket");
        if (!objx) {
            $("body").data("websocket", objx = new webcnm(opstions));
        }
        return objx;
    }
    $.webcnm.defaults = {
        ws: null,
        type: "post",
        url: "",
        dataType: "json",
        timeout:10000,
        callback: function () { },
        error: function () { },
        close: function () { }
    };

})(jQuery);