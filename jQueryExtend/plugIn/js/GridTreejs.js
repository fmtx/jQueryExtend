
/*!
 * GridTree表格树控件 不用表格做，肯定有问题
 * 创建人：qinnailin
 * 创建时间：2015/1/5 
 */

!(function ($) {
    "use strict"
    var GridTree = function (element, options) {
        this.init("GridTree", element, options);
    }//对象
    GridTree.prototype = {
        init: function (type, element, options) {
            var ops;
            this.type = type,
            this.$element = $(element),
            this.$id = $(element).attr("id"),
            this.options = ops = this.getoptions(options),
            this.tempBox = this.getTempBox(),
            this.template = this.getTemp();
            this.tempBox.addClass("fm-ulgtree");
            this.tempBox.html("");
            this.loadData();
            var $this = this;
            this.tempBox.on("click", "span.switch", function () {
                var obj = $(this).parents("li").eq(0);
                var chk = $(this).siblings("span.chk").hasClass("checkbox_true_full_focus");
                if (!obj.children("div").hasClass("t_open")) {
                    var data = obj.data("rowdata");
                    if (data[$this.options.dataFormat.open]) {
                        var pk = "";
                        if (ops.param) {
                            pk = "{";
                            var array = new Array();
                            $.each(ops.param, function (i, n) {
                                array.push("'" + ops.param[i] + "':'" + data[ops.param[i]] + "'");
                            });
                            pk += array.join(",");
                            pk += "}";
                        }
                        $.getJSON(ops.url, stringToJSON(pk), function (d) {
                            var rd;
                            if (ops.source != "") {
                                var temp = d;
                                var item = ops.source.split('.');
                                $.each(item, function (i, n) {
                                    if (n != "") {
                                        temp = temp[n];
                                    }
                                });
                                rd = temp;
                            }
                            $this.fillAjaxNode(obj, data, rd, chk);
                            $this.open(obj);
                        });
                    } else if (data[$this.options.dataFormat.child]) {
                        $this.fillNode(obj, data, chk);
                        $this.open(obj);
                    } else {
                        $this.open(obj);
                    }
                } else {
                    $this.fold(obj);
                }
                return false;
            });
            this.tempBox.on("click", "span.chk", function () {
                var obj = $(this);
                $this.checked(obj);
                return false;
            });
            this.tempBox.on("click", ".cols", function () {
                if (ops.showSelect)
                {
                    $this.selected($(this));
                }
            });
        },//参数
        getoptions: function (options) {
            return $.extend({}, $.fn[this.type].defaults, options);
        },//盒子
        getTempBox: function () {
            this.$element.addClass("box-border")
                         .append("<div><ul></ul></div>")
                         .append("<div><ul></ul></div>");
            var $head = this.$element.children("div").eq(0)
            $head.addClass("row-fluid");
            $head.children("ul").addClass("ul_grid_title");
            var $center = this.$element.children("div").eq(1);
            $center.addClass("row-fluid");
            var $ul = $center.children("ul").addClass("ul_grid");
            return $ul;
        },//模板
        getTemp: function () {
            var ops = this.options;
            var $this = this;
            var iconTemp = "<span class=\"line\" {{layer}} ></span><span class=\"button switch center\"></span>"
            if (ops.treeModel == "checkbox") {
                iconTemp += "<span  class=\"button chk checkbox_false_full\" ></span>";
            }
            iconTemp += "<span {{diyicon}} class=\"button {{icon2}}\"></span>";
            var temp = "<div class='cols t_close' >";
            var htemp = "<li><div class='cols'>";
            if (!ops.header) {
                alert("gridtree标头不可为空！");
                return false;
            }
            $.each(ops.header, function (i, n) {
                var html;
                if (n.tree) {
                    html = "<div>" + iconTemp + "<span class='text'>{{" + n.key + "}}</span></div>";
                } else {
                    html = "<div " + (n.html ? "class='ut-click'" : "") + " >{{" + n.key + "}}" + (n.html ? n.html : "") + "</div>";
                }
                if (n.width) {
                    temp += $(html).css("width", n.width + ops.unit).get(0).outerHTML;
                    htemp += $("<div>" + n.name + "</div>").css("width", n.width + ops.unit).get(0).outerHTML;
                } else {
                    alert("宽度是必填项！！");
                    return false;
                }
            });
            temp += "</div>";
            htemp += "</div></li>";
            $this.$element.find(".ul_grid_title").append(htemp);
            return temp;
        },//加载数据
        loadData: function () {
            var ops = this.options,
            $this = this;
            if (ops.data != null) {
                $this.fillRoot(ops.data);
                if (ops.afterCreate) {
                    ops.afterCreate();
                }
            } else if (ops.url != "") {
                $.getJSON(ops.url, { _r_t: (ops.cache ? 0 : Math.random()) }, function (data) {
                    var resd = data;
                    if (ops.source != "") {
                        var temp = data;
                        var item = ops.source.split('.');
                        $.each(item, function (i, n) {
                            if (n != "") {
                                temp = temp[n];
                            }
                        });
                        resd = temp;
                    }
                    $this.fillRoot(resd);
                    if (ops.afterCreate) {
                        ops.afterCreate();
                    }
                });
            }
        },//root数据
        fillRoot: function (data) {
            if (!data) return;
            var ops = this.options,
                $this = this;
            $.each(data, function (i, n) {
                n["tag_id"] = i;
                var child = n[ops.dataFormat.child];
                n["icon2"] = "ico";
                var html = $this.filltrRootData(n), d = n, count = 0, $li, $chk;
                count = n["tag_id"].toString().split('_').length - 1;
                count = count == -1 ? 0 : count;
                if (ops.expand != 0) {
                    d = $this.copyobject(n);
                }
                html = (n[ops.dataFormat.selected] ? '<li class="selected" >' : '<li>') + html + '</li>';
                var $html = $(html);
                $html.remove().appendTo($this.tempBox);
                $li = $this.tempBox.find("li:last");

                $chk = $li.find(".chk");
                if ($chk && n[ops.dataFormat.checked]) {
                    $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                }
                ops.bindNode(n, $li);
                if (child && child.length > 0 && ops.expand != 0) {
                    $this.fillChild(child, $li, "");
                    d[ops.dataFormat.child] = null;
                }
                $li.data("rowdata", d);
                if (ops.expand != 0) {
                    $this.open($html);
                }
            });//子数据
        }, fillChild: function (data, element, pid) {
            var ops = this.options,
                $this = this, $ul = element.children("ul");
            if (!$ul.get(0)) {
                element.append("<ul></li>");
                $ul = element.children("ul");
            }
            for (var i = 0; i < data.length ; i++) {
                var n = data[i];
                n["tag_id"] = pid + "_" + i;
                var child = n[ops.dataFormat.child];
                n["icon2"] = (child && child.length > 0) ? "ico" : "docu";
                var html = $this.filltrData(n);
                var d = n;
                var count = 0;
                count = n["tag_id"].toString().split('_').length - 1;
                count = count == -1 ? 0 : count;
                html = (n[ops.dataFormat.selected] ? '<li class="selected" >' : '<li>') + html + '</li>';
                var $html = $(html);
                $html.remove().appendTo($ul);
                var $li = $ul.find("li:last");

                var $docu = $li.find(".switch").eq(0), $chk = $li.find(".chk").eq(0);
                if (n["icon2"] == "docu" && !$docu.hasClass("docu")) {
                    $docu.addClass("docu");
                }
                if ($chk && n[ops.dataFormat.checked]) {
                    $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                }
                ops.bindNode(n, $li);
                if (child && child.length > 0 && ops.expand != 0) {
                    if (ops.expand > 0 && count > ops.expand) {
                        $li.data("rowdata", d);
                        continue;
                    } else {
                        $this.fillChild(child, $li, n["tag_id"]);//
                        $this.open($html);
                        d[ops.dataFormat.child] = null;
                        $li.data("rowdata", d);
                    }
                }
            }
        },//数据
        fillNode: function (element, d, chk) {
            var ops = this.options,
                $this = this, pid = d.tag_id, data = d[$this.options.dataFormat.child];
            element.data("rowdata", $this.copyobject(d));
            element.append("<ul></ul>");
            var $ul = element.children("ul").eq(0);
            for (var i = 0; i < data.length ; i++) {
                var n = data[i];
                n["tag_id"] = pid + "_" + i;
                var child = n[ops.dataFormat.child];
                n["icon2"] = (child && child.length > 0) || (n[ops.dataFormat.open]) ? "ico" : "docu";
                var html = $this.filltrData(n);
                var d = n;
                html = '<li>' + html + '</li>';
                var $html = $(html);
                if (chk && ops.multiple) {
                    $html.find(".chk").addClass("checkbox_true_full_focus");
                }
                $ul.append($html);
                var $li = $ul.children("li:last"), $chk;
                var $docu = $li.find(".switch").eq(0);
                if (n["icon2"] == "docu" && !$docu.hasClass("docu")) {
                    $docu.addClass("docu");
                }
                $chk = $li.find(".chk").eq(0);
                if ($chk && n[ops.dataFormat.checked]) {
                    $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                }
                $li.data("rowdata", d);
            }
        },//数据
        fillAjaxNode: function (element, pd, d, chk) {
            var ops = this.options,
                $this = this, pid = pd.tag_id, data = d;
            pd[$this.options.dataFormat.open] = false;
            element.data("rowdata", pd);
            element.append("<ul></ul>");
            var $ul = element.children("ul").eq(0);
            for (var i = 0; i < data.length ; i++) {
                var n = data[i];
                n["tag_id"] = pid + "_" + i;
                var child = n[ops.dataFormat.child];
                n["icon2"] = (child && child.length > 0) || (n[ops.dataFormat.open]) ? "ico" : "docu";
                var html = $this.filltrData(n);
                var d = n;
                var $html = $("<li>" + html + "</li>");
                if (chk && ops.multiple) {
                    $html.find(".chk").addClass("checkbox_true_full_focus");
                }
                $ul.append($html);
                var $li = $ul.children("li:last"), $chk;
                var $docu = $li.find(".switch").eq(0);
                if (n["icon2"] == "docu" && !$docu.hasClass("docu")) {
                    $docu.addClass("docu");
                }
                $chk = $li.find(".chk").eq(0);
                if ($chk && n[ops.dataFormat.checked]) {
                    $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                }
                $li.data("rowdata", d);
            }
        },
        copyobject: function (data) {
            var obj = {};
            for (var i in data) {
                if (i.toString() != this.options.dataFormat.child)
                    obj[i] = data[i];
            }
            return obj;
        },//填充
        filltrRootData: function (data) {
            var ops = this.options, $this = this, count = 0;
            if (data) {
                var html = $this.template.fill(data)
                    .replaceo("layer", "style=''")
                    .replaceo("diyicon", ops.dataFormat.icon != null ? ((data[ops.dataFormat.icon] && data[ops.dataFormat.icon] != "") ? "style=\"background: url(" + data[ops.dataFormat.icon] + ") 0 0 no-repeat;\"" : "") : "")
                    .replaceo("selected", data[ops.dataFormat.selected] ? "fm-sel=\"selected\"" : "")
                    .replaceo("data-id", data.tag_id).fillEmpty();
                var cnodeicon = false;
                if (ops.clazz > 0 && ops.expand && ops.clazz <= (count + 1)) {
                    cnodeicon = true;
                }
                return html;
            } else {
                return "";
            }
        },//填充
        filltrData: function (data) {
            var ops = this.options, $this = this, count = 0;
            count = data["tag_id"].toString().split('_').length - 1;
            count = count == -1 ? 0 : count;
            if (data) {
                var html = $this.template.fill(data)
                    .replaceo("diyicon", ops.dataFormat.icon != null ? ((data[ops.dataFormat.icon] && data[ops.dataFormat.icon] != "") ? "style=\"background: url(" + data[ops.dataFormat.icon] + ") 0 0 no-repeat;\"" : "") : "")
                    .replaceo("selected", data[ops.dataFormat.selected] ? "fm-sel=\"selected\"" : "")
                    .replaceo("data-id", data.tag_id)
                    .replaceo("layer", "style='padding-left:" + (count * 18) + "px;'").fillEmpty();;
                var cnodeicon = false;
                if (ops.clazz > 0 && ops.expand && ops.clazz <= (count + 1)) {
                    cnodeicon = true;
                }
                return html;
            } else {
                return "";
            }
        },
        serialize: function (data, pid) {
            var items = new Array(),
                $this = this,
                ops = this.options;
            $.each(data, function (i, d) {
                var child = d[ops.dataFormat.child];
                var n = $this.copyobject(d);
                n["tag_id"] = pid ? pid + "_" + i : i;
                items.push(n);
                if (child && child.length > 0) {
                    var temp = $this.serialize(child, n["tag_id"]);
                    $.each(temp, function (j, x) {
                        items.push(x);
                    });
                }
                n["icon2"] = (child && child.length > 0 || !pid) ? "ico" : "docu";
            });
            return items;
        },//展开
        open: function (element) {
            element.children("div.t_close").addClass("t_open").removeClass("t_close");
            var $ul = element.children("ul").show(200);
        },//折叠
        fold: function (element) {
            element.children("div.t_open").addClass("t_close").removeClass("t_open");
            var $ul = element.children("ul").hide(200);
        },//查找
        findParent: function (element) {
            var items = element.prevAll("tr");
            var id = element.attr("id");
            var last = id.lastIndexOf("_");
            id = id.substring(0, last);
            for (var i = 0; i < items.length; i++) {
                if ($(items[i]).attr("id") == id) {
                    return $(items[i]);
                }
            }
            return element;
        },//选中
        checkChild: function (element) {
            var $cli = element.children("ul").children("li");
            $cli.each(function (i, n) {
                $(n).find(".chk").addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
            });
        },//不选
        noCheckChild: function (element) {
            var $cli = element.children("ul").children("li");
            $cli.each(function (i, n) {
                $(n).find(".chk").addClass("checkbox_false_full").removeClass("checkbox_true_full_focus");
            });
        },//选中
        selected: function (element) {
            var data = element.parents("li").data("rowdata");
            var ops = this.options;
            var issel = true;
            if (ops.beforeSelect) {
                ops.beforeSelect(data, element);

            }
            if (element.parent("li").hasClass("selected")) {
                element.parent("li").removeClass("selected");
                issel = false;
            } else {
                if (!ops.multiple) {
                    this.$element.find("li").removeClass("selected");
                    element.parent("li").addClass("selected");
                } else {
                    element.parent("li").addClass("selected");
                }
                ops.selected(data);
            }
            if (ops.click) {
                if (issel) {
                    ops.click(data);
                } else {
                    ops.click(null);
                }

            }
        },//选中
        checked: function (element) {
            var li = element.parents("li").eq(0);
            var data = element.parents("li").data("rowdata");
            var ops = this.options;
            var bfs = true;
            if (ops.beforeCheck) {
                bfs = ops.beforeCheck(data, element);
                if (bfs == undefined) bfs = true;
            }
            if (bfs) {
                if (!this.options.multiple) {
                    this.$element.find(".checkbox_true_full_focus").addClass("checkbox_false_full")
                    .removeClass("checkbox_true_full_focus");
                }
                if (element.hasClass("checkbox_false_full")) {
                    element.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
                    if (!this.options.multiple) {
                        return false;
                    }
                    this.checkChild(li);
                } else {
                    element.addClass("checkbox_false_full").removeClass("checkbox_true_full_focus");
                    this.noCheckChild(li);
                }
            }
        },//获取选中列表
        getCheckNodes: function () {
            var array = new Array();
            this.$element.find(".checkbox_true_full_focus").each(function (i, n) {
                var tr = $(n).parents("li").eq(0);
                array.push(tr.data("rowdata"));
            });
            return array;
        },//获取选中行列表
        getSelectNodes: function () {
            var array = new Array();
            this.$element.find(".selected").each(function (i, n) {
                array.push($(n).data("rowdata"));
            });
            return array;
        },//设置为选中行
        setSelected: function (element) {
            element.addClass("selected");
        },//设置为选中
        setChecked: function (element) {
            var $chk = $li.find(".chk").eq(0);
            if ($chk && n[ops.dataFormat.checked]) {
                $chk.addClass("checkbox_true_full_focus").removeClass("checkbox_false_full");
            }
        },
        reload: function () {
            this.$element.find(".fm-ulgtree").html("");
            this.loadData();
        }
    };
    //函数入口
    $.fn.GridTree = function (options) {
        var $this = $(this);
        var data = $this.data("GridTree");
        if (!data) {
            $this.data("GridTree", (data = new GridTree(this, options)));
        } else {
            data.options = $.extend(false, $this.GridTree.defaults, options);
        }
        return data;
    }
    //默认值
    $.fn.GridTree.defaults = {
        url: "",
        data: null,
        expand: -1,//-1全部展开，0，不展开，num展开层数
        dataFormat: { child: "Child", checked: "Checked", selected: "Selected", icon: "Icon", open: "Open" },
        param: null,//附带参数['Id','Pid']
        source: "Data",
        cache: true,
        showSelect:true,
        treeModel: "default",//默认模式
        multiple: false,//是否多项
        selected: function (data) { },
        checked: function (data) { },
        click: function (data) { },
        afterCreate: null,
        beforeSelect: function (node, element) { },
        beforeCheck: function (node, element) { },
        bindNode: function (node, element) { },
        unit: '%',
        header: null
    };
})(window.jQuery);
