﻿/*!
 * 集成表格控件 
 *创建人：qinnailin
 * 创建时间：2014/11/11 尼玛个光棍节！！
 */


(function ($) {
    $.fn.uTable = function (opstions) {
        var ops = $.extend(true, {
            url: "",
            data: null,
            dataformat: {data:"Data",count:"Count"},
            click: null,
            dbclick: null,
            method: "Get",
            dataType: "json",
            sortMultiple: false,
            putJson: true,
            cache: false,
            multiple: false,
            cancel: true,
            page: {
                enable:true,
                click:null,
                pageSize: 30,
                pageIndex: 1,
                back: "上一页",
                next: "下一页",
                reqindex: "index",
                reqsize: "size"
            },
            afterCreate: null,
            checkbox: {
                enable: false,
                multiple: true,
                click: function () { }
            },
            buttons: null,
            heads: []
        }, opstions);
        var objthis = $(this);
        var idname = objthis.attr("id");
        if ($("[id=" + idname + "]").length > 1)
        {
            alert("检测到id重复了！");
        }
        bindTool();
        bindTable();

        function bindTool() {
            if (!ops.buttons && !$(objthis).find(".table-search").get(0)) return false;
            var html = "<div class=\"utable-tool\" ></div>";
            $(objthis).append(html);
            if (ops.buttons)
            {
                $.each(ops.buttons, function (i, n) {
                    $("<button class='btn " + (n.style ? n.style : "") + "' >" + n.text + "</button>").appendTo($(objthis).find(".utable-tool")).click(function () {
                        n.click();
                    });
                });
            }
            if ($(objthis).find(".table-search").length > 0 && ops.buttons) {
                $(".utable-tool").find(".btn").eq(0).before($("<button class='btn btn-search' >查找</button>").click(function () {
                    $(objthis).find(".table-search").show();
                }));
            } else if ($(objthis).find(".table-search").length>0) {
                $("<button class='btn btn-search' >查找</button>").appendTo($(objthis).find(".utable-tool")).click(function () {
                    $(objthis).find(".table-search").show();
                });
            }
            if ($(objthis).find(".table-search").get(0)) {
                $(objthis).find(".table-search").append("<div class='table-search-btn' ><button class='table-search-btn-close' >关闭</button><button class='table-search-btn-search'>检索</button></div>");
                $(objthis).find(".table-search-btn-close").click(function () {
                    $(objthis).find(".table-search").hide();
                });
                $(objthis).find(".table-search-btn-search").click(function () {
                    $(objthis).find("[fm-search]").submit();
                    $(objthis).find(".table-search").hide();
                });
            }
        }
        function bindTable() {
            var html = "<div class=\"row-fluid \">";
            html += "<table class='table table-responsive table-striped table-bordered table-hover no-margin' ><thead fm-head><tr>";
            if (ops.checkbox.enable && ops.checkbox.multiple) {
                html += "<th width='10px' ><input type='checkbox' class='chkAll' /></th>";
            } else if (ops.checkbox.enable){
                html += "<th ></th>";
            }
            $.each(ops.heads, function (i, n) {
                html += (n["sort"] ? "<th fm-sort='" + n.key + "' " + (n["width"] ? "width='" + n.width + "'" : '') + " >" : "<th " + (n["width"] ? "width='" + n.width + "'" : '') + ">") + n.name + "</th>";
            });
            html += "</tr></thead>";
            html += "<tbody fm-body><tr>";
            if (ops.checkbox.enable) {
                html += "<td ><input type='checkbox' /></td>";
            }
            $.each(ops.heads, function (i, n) {
                html += "<td " + (n.conver ? "fm-conver='" + n.conver + "'" : "") + " " + (n.width ? "width='" + n.width + "'" : '') + " >{{" + n.key + "}}" + (n.html ? n.html : "") + (n.num ? "{{rowNumber}}" : "") + "</td>";
            });
            html +="</tr></tbody></table>";
            html += "</div>";
            if (ops.page.enable) {
                html += "<div class=\"row-fluid dataTables_paginate paging_full_numbers\" style='height:40px;' ><div fm-pagerbox class=\"tfoot\"></div></div>";
            }
            $(objthis).append(html);
        }
        
        if (ops.page.click) {
            var tempc = ops.page.click;
            ops.page.click = function () {
                $(objthis).find(".chkAll").removeAttr("checked");
                tempc();
            }
        } else {
            ops.page.click = function () {
                $(objthis).find(".chkAll").removeAttr("checked");
            }
        }

        $(document).ready(function () {
            $(objthis).find("[fm-body]").on("click", "input[type='checkbox']", function () {
                var row = $(this).parents("tr");
                if (row.hasClass("checked_tr")) {
                    if (ops.cancel) {
                        $(row).removeClass("checked_tr");
                        $(row).find("input[type='checkbox']").removeAttr("checked");
                        $(objthis).find(".chkAll").removeAttr("checked");
                    }
                } else {
                    if (!ops.checkbox.multiple) {
                        $(objthis).find("[fm-body] input[type='checkbox']").removeAttr("checked");
                        $(objthis).find("[fm-body]>tr").removeClass("checked_tr");
                    }
                    $(row).addClass("checked_tr");
                    $(row).find("input[type='checkbox']").attr("checked", "checked");
                    if ($(objthis).find("[fm-body] input:checked").size() == $(objthis).find("[fm-body]>tr").size()) {
                        if (ops.checkbox.multiple) {
                            $(objthis).find(".chkAll").attr("checked", "checked");
                        }
                    }
                }
            });

            if (!ops.checkbox.enable)
            {
                $(objthis).find("[fm-body]").on("click", "tr", function () {
                    var row = $(this);
                    if (row.hasClass("checked_tr")) {
                        if (ops.cancel) {
                            $(row).removeClass("checked_tr");
                            $(row).find("input[type='checkbox']").removeAttr("checked");
                            $(objthis).find(".chkAll").removeAttr("checked");
                        }
                    } else {
                        if (!ops.multiple) {
                            $(objthis).find("[fm-body] input[type='checkbox']").removeAttr("checked");
                            $(objthis).find("[fm-body]>tr").removeClass("checked_tr");
                        }
                        $(row).addClass("checked_tr");
                        $(row).find("input[type='checkbox']").attr("checked", "checked");
                        if ($(objthis).find("[fm-body] input:checked").size() == $(objthis).find("[fm-body]>tr").size()) {
                            if (ops.multiple) {
                                $(objthis).find(".chkAll").attr("checked", "checked");
                            }
                        }
                    }
                });
            }

            $(objthis).on("click", ".chkAll", function () {
                if ($(this).attr("checked") == "checked") {
                    $(objthis).find("[fm-body] input[type='checkbox']").attr("checked", "checked");
                    $(objthis).find("[fm-body]>tr").addClass("checked_tr");
                } else {
                    $(objthis).find("[fm-body] input[type='checkbox']").removeAttr("checked").removeClass("checked_tr");
                    $(objthis).find("[fm-body]>tr").removeClass("checked_tr");
                }
                if (ops.click) {
                    ops.click();
                }
            });
        });

        var utable = objthis.Grid(ops);
        var res = {
            utable: utable,
            getSelectData: function () {//获取选中内容
                var arr = new Array();
                var data = utable.getData();
                $(objthis).find("[fm-body]>tr.checked_tr").each(function (i, n) {
                    arr.push($(n).data("jsondata"));
                });
                return arr;
            },
            refresh: function () {
                utable.refresh();
            },
            reload: function () {
                utable.reload();
            },
            load: function (opst) {
                utable.load(opst);
            }
        }
        return res;
    }
})(jQuery);