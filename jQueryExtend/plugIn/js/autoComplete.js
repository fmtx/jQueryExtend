﻿/*!
 * 自动完成控件
 * 创建人：qinnailin
 * 创建时间：2014/8/1
 *
 *
 * 
 */

(function ($) {
    $.fn.autoComplete = function (options) {
        var ops = $.extend({
            url: "",
            data:null,
            source:"",
            value: null,
            key: "id",
            text: "name",
            maxSize: 10,
            width: "auto",
            start:1,
            change: function (obj) { },
            offset:null
        }, options || {});
        var objthis = $(this);
        var basebox = objthis.parent(".dropdown");
        if (!basebox)
            basebox = objthis.parent();
        var data;
        var oldval;
        $(objthis).parent().append("<ul class=\"typeahead dropdown-menu\"></ul>");
        $(this).attr("autocomplete", "off");
        if (ops.width == "auto") {
            $(".dropdown-menu").width($(objthis).width());
        } else if (!isNaN(ops.width)) {
            $(".dropdown-menu").width(ops.width);
        }
        if (ops.offset != null) {
            $(objthis).next(".dropdown-menu").css({ top: ops.offset.Top + "px", left: ops.offset.Left + "px" });
        } 
        /**
        * 输入内容
        * @for autoComplete
        */
        $(objthis).keyup(function () {
            var keyword = $(objthis).val();
            if (keyword == "") {
                if (ops.value) {
                    $("input[name=" + ops.value + "]").val("");
                }
                $(objthis).siblings("ul:first").hide();
                $(objthis).siblings("ul:first li").remove();
                oldval = "";
                return;
            }
            if (oldval != keyword) {
                oldval = keyword;
            } else {
                return false;
            }
            if (ops.data) {
                var d = new Array();
                $.each(ops.data, function (i, n) {
                    if (n[ops.text].indexOf(keyword)>-1) {
                        d.push(n);
                    }
                });
                data = d;
                loadData(d);
            } else if (ops.url != "") {
                if (keyword.length >= ops.start) {
                    var vld = "{" + $(objthis).attr("name") + ":'" + keyword + "'" + ",maxSize:" + ops.maxSize + "}";
                    $.get(ops.url, stringToJSON(vld), function (datas) {
                        data = datas;
                        if (ops.source != "") {
                            var temp = datas;
                            var item = ops.source.split('.');
                            $.each(item, function (i, n) {
                                if (n != "") {
                                    temp = temp[n];
                                }
                            });
                            data = temp;
                        }
                        loadData(data);
                    });
                }
            }
               
        });

        /**
        * 填充数据源
        * @method loadData
        * @param {object} data 数据对象
        * @for autoComplete
        */
        function loadData(data) {
            if (data && data.length > 0) {
                var html = "";
                var temp = "<li style='display:inline'><a data-id='{{" + ops.key + "}}' href='javascript:' >{{" + ops.text + "}}</a></li>";
                for (var i = 0; i < data.length && i < 10; i++) {
                    var n = data[i];
                    html += temp.fill(n);
                }
                $(objthis).siblings("ul:first").html(html).show();
            } else {
                $(objthis).siblings("ul:first").html("").hide();
            }
        }
        $(document).ready(function () {
            $(".dropdown").on("click", "ul li a", function () {
                var name = $(this).text();
                var id = $(this).attr("data-id");
                $(objthis).val(name);
                if (ops.value) {
                    $("input[name=" + ops.value + "]").val(id);
                }
                $(objthis).siblings("ul:first").hide();
                $(objthis).focus();
                var row;
                for (var i = 0; i < data.length; i++) {
                    if (data[i][ops.key] == id) {
                        row = data[i];
                        break;
                    }
                }
                oldval = "";
                ops.change(row);
            });
            /**
            * 鼠标移动事件
            * @for autoComplete
            */
            $(".dropdown").on("mousemove", "ul", function () {
                if ($(".dropdown").find(".active").get(0)) {
                    $(".dropdown").find(".active").removeClass("active");
                }
            });
            /**
            * 控制光标游动
            * @for autoComplete
            */
            $(objthis).keyup(function (event) {
                if (event.keyCode == 40) {
                    if ($(basebox).find(".active").get(0)) {
                        $(basebox).find(".active").removeClass("active").next("li").addClass("active")
                        if (!$(basebox).find(".active").get(0)) {
                            $(basebox).find("li:first").addClass("active");
                        }
                    } else {
                        $(basebox).find("li:first").addClass("active");
                    }
                } else if (event.keyCode == 38) {
                    if ($(basebox).find(".active").get(0)) {
                        $(basebox).find(".active").removeClass("active").prev("li").addClass("active")
                        if (!$(basebox).find(".active").get(0)) {
                            $(basebox).find("li:last").addClass("active");
                        }
                    } else {
                        $(basebox).find("li:last").addClass("active");
                    }
                } else if (event.keyCode == 13) {
                    var row;
                    var objx
                    if ($(basebox).find(".active").get(0)) {
                        objx = $(basebox).find(".active>a").eq(0);
                    }else{
                        objx = $(basebox).find("li:first>a");
                    }
                    var name = $(objx).text();
                    var id = $(objx).attr("data-id");
                    $(objthis).val(name);
                    if (ops.value) {
                        $("input[name=" + ops.value + "]").val(id);
                    }
                    $(objthis).siblings("ul:first").hide();
                    $(objthis).focus();
                    for (var i = 0; i < data.length; i++) {
                        if (data[i][ops.key] == id) {
                            row = data[i];
                            break;
                        }
                    }
                    oldval = "";
                    $(objthis).siblings("ul:first").html("");
                    ops.change(row);
                }
            });
        });
    }
})(jQuery);