﻿
(function ($) {
    $.fn.extend({
        time: function (opstion) {
            var ops = $.extend(true, {
                format: 'HH:mm:ss',
                minTime: null,
                maxTime: null,
                zindex:500,
                onclose: null
            }, opstion);
            var objthis = $(this);
            var timebox;
            var timedef = {hour:0,minute:0,second:0};
            
            var tool={
                getPosition: function (inside) {
                    return $.extend({}, (inside ? { top: 0, left: 0 } : $(objthis).offset()), {
                        width: objthis[0].offsetWidth
                    , height: objthis[0].offsetHeight
                    })
                },
                create: function () {
                    var pos = this.getPosition();
                    var hour, minute, second;
                    if ($(objthis).val() == "") {
                        var date = new Date();
                        timedef.hour = hour = date.getHours();
                        timedef.minute = minute = date.getMinutes();
                        timedef.second = second = date.getSeconds();
                    } else {
                        var val = $(objthis).val();
                        var tval = val.split(":");
                        timedef.hour = hour = tval[0];
                        if (tval.length >= 2) {
                            timedef.minute = minute = tval[1];
                        }
                        if (tval.length >= 3) {
                            timedef.second = second = tval[2];
                        }
                    }
                    var template = "<div  class='timepicker time-picker-wrap' style='display: block; z-index:"+ops.zindex+"'>" +
                "<table><tbody><tr class='time-board-wrap'><td class='hour-picker-wrap'>" +
                "<div class='scoreboard'><div class='increase-button'><span></span></div>" +
                "<div class='score'><span>" + hour + "</span></div><div class='decrease-button'><span></span></div></div>" +
                "</td><td>:</td><td class='minute-picker-wrap'><div class='scoreboard'><div class='increase-button'>" +
                "<span></span></div><div class='score'><span>" + minute + "</span></div><div class='decrease-button'><span></span></div></div>" +
                "</td><td>:</td><td class='second-picker-wrap'><div class='scoreboard'><div class='increase-button'><span></span></div>" +
                "<div class='score'><span>" + second + "</span></div><div class='decrease-button'><span></span></div></div></td></tr>" +
                "<tr class='button-wrap'><td class='clear-wrap' colspan='2'><span>清除</span></td><td></td>" +
                "<td class='close-wrap' colspan='2'><span>关闭</span></td></tr></tbody></table></div>";
                    $(objthis).after(template);
                    timebox = $(objthis).next(".timepicker");
                    $(timebox).css({ top: (pos.top + pos.height), left: pos.left });

                    $(timebox).on("click", ".close-wrap", function () {
                        if (timedef.hour != 0 || timedef.minute != 0 || timedef.second != 0) {
                            $(objthis).val(timedef.hour + ":" + timedef.minute + ":" + timedef.second);
                        }
                        if (ops.onclose) {
                            ops.onclose(timedef);
                        }
                        tool.hide();
                    });

                    $(timebox).on("click", ".clear-wrap", function () {
                        $(objthis).val("");
                        tool.hide();
                    });

                    $(timebox).on("click", ".hour-picker-wrap .increase-button", function () {
                        var hour = $(this).next(".score").children("span").text();
                        var hounnum = (parseInt(hour) + 1);
                        if (hounnum > 23) { hounnum = 0; }
                        timedef.hour = hounnum;
                        $(this).next(".score").children("span").text(hounnum);
                    });
                    $(timebox).on("click", ".hour-picker-wrap .decrease-button", function () {
                        var hour = $(this).prev(".score").children("span").text();
                        var hounnum = (parseInt(hour) - 1);
                        if (hounnum < 0) { hounnum = 23; }
                        timedef.hour = hounnum;
                        $(this).prev(".score").children("span").text(hounnum);
                    });
                    $(timebox).on("click", ".minute-picker-wrap .increase-button", function () {
                        var minute = $(this).next(".score").children("span").text();
                        var minutenum = (parseInt(minute) + 1);
                        if (minutenum > 59) { minutenum = 0; }
                        timedef.minute = minutenum;
                        $(this).next(".score").children("span").text(minutenum);
                    });
                    $(timebox).on("click", ".minute-picker-wrap .decrease-button", function () {
                        var minute = $(this).prev(".score").children("span").text();
                        var minutenum = (parseInt(minute) - 1);
                        if (minutenum < 0) { minutenum = 59; }
                        timedef.minute = minutenum;
                        $(this).prev(".score").children("span").text(minutenum);
                    });
                    $(timebox).on("click", ".second-picker-wrap .increase-button", function () {
                        var second = $(this).next(".score").children("span").text();
                        var secondnum = (parseInt(second) + 1);
                        if (secondnum > 59) { secondnum = 0; }
                        timedef.second = secondnum;
                        $(this).next(".score").children("span").text(secondnum);
                    });
                    $(timebox).on("click", ".second-picker-wrap .decrease-button", function () {
                        var second = $(this).prev(".score").children("span").text();
                        var secondnum = (parseInt(second) - 1);
                        if (secondnum < 0) { secondnum = 59; }
                        timedef.second = secondnum;
                        $(this).prev(".score").children("span").text(secondnum);
                    });

                    $(window).click(function (e) {
                        if (e.srcElement != $(objthis).get(0) && $(e.srcElement).parents(".timepicker").get(0) != $(timebox).get(0)) {
                            tool.hide();
                        }
                    });
                },
                show: function () {
                    if ($(objthis).val() == "") {
                        var date = new Date();
                        timedef.hour = date.getHours();
                        timedef.minute = date.getMinutes();
                        timedef.second = date.getSeconds();
                    } else {
                        var val = $(objthis).val();
                        var tval = val.split(":");
                        timedef.hour =  tval[0];
                        if (tval.length >= 2) {
                            timedef.minute =  tval[1];
                        }
                        if (tval.length >= 3) {
                            timedef.second =  tval[2];
                        }
                    }
                    $(timebox).find(".hour-picker-wrap .score>span").eq(0).text(timedef.hour);
                    $(timebox).find(".minute-picker-wrap .score>span").eq(0).text(timedef.minute);
                    $(timebox).find(".second-picker-wrap .score>span").eq(0).text(timedef.second);
                    var pos = this.getPosition();
                    $(timebox).css({ top: (pos.top + pos.height), left: pos.left });
                    $(timebox).show();
                },
                hide: function () {
                    $(timebox).hide();
                }
            }

            
            
            $(objthis).focus(function () {
                if ($(timebox).get(0)) {
                    tool.show();
                    var val = $(objthis).val();
                    if (val!="") {
                        var tval = val.split(":");
                        hour = tval[0];
                        if (tval.length >= 2) {
                            minute = tval[1];
                        }
                        if (tval.length >= 3) {
                            second = tval[2];
                        }
                    }
                } else {
                    tool.create();
                }
            });
            
            $(objthis).keyup(function () {
                $(objthis).val("");
            });
        }
    });
    
})(jQuery);