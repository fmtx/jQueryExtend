﻿/*!
 * select控件只为select标签提供数据异步读取功能不再修改样式因为现在ie8以上版本的样式不会像ie7一下版本
 * 创建人：qinnailin
 * 创建时间：2016/1/26 
 *
 *
 * 
 */
!(function ($) {
    "use strict"
    var select = function (element, options) {
        this.init("select", element, options);
    }
    var isin = 0;

    select.prototype = {
        constructor: select,
        init: function (type, element, options) {
            this.type = type;
            this.$element = $(element);
            this.options = this.getOptions(options);
            var width = $(element).width();
            var height = $(element).height();
            var selthis = $(element);
            var empty = false;
            var $this = this;
            this.selthis = selthis;
            this.defaultval = this.options.defaults ? this.options.defaults[1] : $(selthis).val();
            this.defaulttext = this.options.defaults ? this.options.defaults[0] : $(selthis).children("option[value='" + this.defaultval + "']").text();
            if (!$(element).children("option").get(0)) {
                empty = true;
            }
            if (empty) {
                if (this.options.url == "" && this.options.data != null) {
                    this.loadData();
                } else if (this.options.url != "") {
                    var ops = this.options;
                    $.getJSON(ops.url, function (rd) {
                        var data;
                        if (ops.source == "") {
                            data = rd;
                        } else {
                            var temp = rd;
                            var item = ops.source.split('.');
                            $.each(item, function (i, n) {
                                if (n != "") {
                                    temp = temp[n];
                                }
                            });
                            data = temp;
                        }
                        $this.setData(data);
                        $this.loadData();
                    });
                }
            } else {
                if (this.options.initval) {
                    var text = $(this.selthis).children("option[value='" + this.options.initval + "']").text();
                    this.setvalue(this.options.initval, text);
                }
            }
            var ops = this.options;
            selthis.change(function () {
                var val = selthis.val();
                var text = selthis.find("opstion[selected]").eq(0).text();
                if (ops.change) {
                    ops.change(val, text);
                }
            });
        },
        reqdata: function () {
            var $this = this;
            var ops = this.options;
            $.getJSON(ops.url, function (rd) {
                var data;
                if (ops.source == "") {
                    data = rd;
                } else {
                    var temp = rd;
                    var item = ops.source.split('.');
                    $.each(item, function (i, n) {
                        if (n != "") {
                            temp = temp[n];
                        }
                    });
                    data = temp;
                }
                $this.setData(data);
                $this.loadData();
            });
        },
        getOptions: function (options) {
            return $.extend({}, $.fn[this.type].defaults, options, this.$element.data());
        },
        loadData: function () {
            var ops = this.options;
            if (ops == undefined) return false;
            var $selthis = this.selthis;
            if (this.options.defaults) {
                $selthis.append("<option value='" + this.options.defaults[1] + "' >" + this.options.defaults[0] + "</option>");
            }
            $.each(ops.data, function (i, n) {
                $selthis.append("<option value='" + n[ops.param.key] + "'>" + n[ops.param.text] + "</option>");
            });
            if (this.defaultval != $(this.selthis).val()) {
                this.defaultval = $(this.selthis).val();
                this.defaulttext = $(this.selthis).children("option[value='" + this.defaultval + "']").text();
            }

            if (this.options.initval) {
                var text = $(this.selthis).children("option[value='" + this.options.initval + "']").text();
                this.setvalue(this.options.initval, text);
            }
            if (this.options.afterCreate) {
                this.options.afterCreate();
            }
        },
        setvalue: function (val, text) {
            var ops = this.options;
            $(this.selthis).children("option").siblings().removeAttr("selected");
            $(this.selthis).children("option[value=" + val + "]").attr("selected", "true");
            if (ops.change) {
                ops.change(val, text);
            }
        },
        setData:function(data){
            this.options.data = data;
        },
        getVal: function () {
            return this.selthis.val();
        },
        getText: function () {
            return this.selthis.children("option[selected]").text();
        },
        setSelected: function (key) {
            var text = this.selthis.children("option[value='" + key + "']").text();
            this.setvalue(key, text);
        },
        clear: function () {
            this.selthis.html("");
            var ops = this.options;
            if (ops.change) {
                ops.change("", "");
            }
        },
        reload: function (obj) {
            this.clear();
            if (obj instanceof Object) {
                this.options.data = obj;
                this.loadData();
            } else {
                var ops = this.options;
                var $this = this;
                $.getJSON(obj, function (rd) {
                    var data;
                    if (ops.source == "") {
                        data = rd;
                    } else {
                        var temp = rd;
                        var item = ops.source.split('.');
                        $.each(item, function (i, n) {
                            if (n != "") {
                                temp = temp[n];
                            }
                        });
                        data = temp;
                    }
                    $this.setData(data);
                    $this.loadData();
                });
            }
        }
    }
    //插件入口
    $.fn.select = function (options) {
        var $this = $(this);
        var idname = $this.attr("id");
        if ($("[id=" + idname + "]").length > 1) {
            alert("检测到id重复了！");
        }
        var data = data = new select(this, options);
        return data;
    }
    //初始值
    $.fn.select.defaults = {
        url: "",
        data: null,
        source: "",
        defaults: ["--请选择--", -1],
        param: { key: "id", text: "name" },
        change: null,
        initval: null,
        afterCreate: null
    }

})(window.jQuery);