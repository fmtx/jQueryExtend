﻿/*!
 * splitter
 * 创建人：qinnailin
 * 创建时间：2015/1/21
 */

!(function ($) {
    "use strict"

    var splitter = function () {
        this.init();
    };
    var canmove = false, $moveobj, isL = true;
    splitter.prototype = {
        constructor: splitter,
        //初始化
        init: function () {
            var $this = this;
            $(".splitter").each(function (i, n) {
                $this.splitter(n);
            });
            $(".splitter").on("mousedown", ".layout-splitter", function (e) {
                $moveobj = $(this);
                canmove = true;
                if ($(this).width() > 4) {
                    isL = false;
                } else {
                    isL = true;
                }
                document.onselectstart = new Function("event.returnValue=false");
            });

            $(document).mouseup(function (e) {
                canmove = false;
                $moveobj = null;
                document.onselectstart = new Function("event.returnValue=true");
            });

            $(document).mousemove(function (e) {
                if (canmove && isL) {
                    var mpos = $this.mouseCoords(e);
                    $this.checkWidth($moveobj, mpos);
                } else if (canmove) {
                    var mpos = $this.mouseCoords(e);
                    $this.checkHeight($moveobj, mpos);
                }
            });
        },//主体
        splitter: function (element) {
            var $element = $(element),
                    w = $element.width() - getwborder($element),
                    h = $element.height()- getontborder($element),
                    left = $element.children(".layout-left"),
                    right = $element.children(".layout-right"),
                    top = $element.children(".layout-up"),
                    down = $element.children(".layout-down"),
                    dlw = left.width(),
                    duh = top.height(),
                    ddh=down.height(),
                    l = dlw > 0 ? dlw : w * 0.2,
                    r = dlw > 0 ? (w - l - 4) : w * 0.8 - 4,
                    u =(duh>0||ddh>0)?(duh>0?duh:(h-ddh-4)): h * 0.5 - 2,
                    d = (duh > 0 || ddh > 0) ? (ddh > 5 ? ddh : (h - u - 4)) : h * 0.5 - 2,
                    $splitter = $("<div></div>");
            d = h > u + d + 4 ? h - u - 4 : d;
            $splitter.addClass("layout-splitter");
            if (left.get(0) && right.get(0)) {
                $splitter.css("width", "4px").css("cursor", "col-resize").css("height", "100%").css("float", "left");
                $splitter.addClass("splitter-L");
                left.after($splitter);
                left.css("width", l).css("height", "100%").css("float", "left");
                right.css("width", r).css("height", "100%").css("float", "left");
            } else if (top.get(0) && down.get(0)) {
                $splitter.css("width", "100%").css("cursor", "row-resize").css("height", "4px");
                $splitter.addClass("splitter-H");
                top.after($splitter);
                top.css("height", u);
                down.css("height", d);
            }
        },//判断宽度
        checkWidth: function (element, pos) {
            var $parent = element.parent(".splitter"),
                    w = $parent.width() - getwborder($parent),
                    left = $parent.children(".layout-left"),
                    right = $parent.children(".layout-right"),
                    lx = this.getPosition(left).left,
                    x = pos.x - lx,
                    x = x < 50 ? 50 : x,
                    l = x - 4,
                    l = l > w - 50 ? w - 50 : l,
                    r = w - l - 4;
            left.width(l);
            right.width(r);
        },//技术坐标
        checkHeight: function (element, pos) {
            var $parent = element.parent(".splitter"),
                    h = $parent.height(),
                    up = $parent.children(".layout-up"),
                    down = $parent.children(".layout-down"),
                    ly = this.getPosition(up).top,
                    y = pos.y - ly,
                    y = y < 5 ? 5 : y,
                    l = y - 4,
                    l = l > h - 5 ? h - 5 : l,
                    r = h - l - 4;
            up.height(l);
            down.height(r);
        },//对象坐标
        getPosition: function (element) {
            return $.extend({}, element.offset(), {
                width: element[0].offsetWidth,
                height: element[0].offsetHeight
            });
        },//坐标
        mouseCoords: function (ev) {
            if (ev.pageX || ev.pageY) {
                return { x: ev.pageX, y: ev.pageY };
            }
            return {
                x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                y: ev.clientY + document.body.scrollTop - document.body.clientTop
            };
        }
    };
    //自启动
    $(document).ready(function () {
        var obj = new splitter();
    });
})(jQuery);