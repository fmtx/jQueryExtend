﻿/*!
 * 多级联动
 * 创建人：qinnailin
 * 创建时间：2014/11/14
 *
 *
 * 
 */

!(function ($) {
    "use strict"
    var cascade = function (element, opstions) {
        this.init("cascade", element, opstions);
    }
    cascade.prototype = {
        constructor: cascade,
        init: function (type, element, opstions) {
            this.type = type,
            this.$element = $(element),
            this.opstions = this.getOpstions(opstions);
            if (this.$element.height() == 0) {
                this.$element.height(28);
            }
            this.$id = $(element).attr("id");
            var $this = this;
            if (this.opstions.url == "" && this.opstions.data != null) {
                this.load();
            } else if (this.opstions.url != "") {
                $.get(this.opstions.url, function (data) {
                    $this.opstions.data = data;
                    $this.load();
                });
            }
        },
        getOpstions: function (opstions) {
            return opstions=$.extend({}, $.fn[this.type].defaults, opstions);
        },//加载数据
        load: function () {
            var $this = this;
            this.$items = new Array();
            var ops = this.opstions;
            if ($this.opstions.items) {
                $.each($this.opstions.items, function (i, n) {
                    var html = "";
                    var width = 100;
                    if (n.width)
                    {
                        width = n.width;
                    }
                    var rname = n.name;
                    if (n.rname)
                    {
                        rname = n.rname;
                    }
                    var selid = $this.$id + "-child-" + i;
                    if (!$("#"+selid).get(0)) {
                        html+="<div id='div-" + selid + "' style=\"float:left;\" >";
                        if (n.title) {
                            html+="<label style=\"line-height:28px;float:left;margin:5px 5px 5px 0px;\">" + n.title + "</label>";
                        }
                        html += "<select id=\"" + selid + "\" name=\"" + rname + "\" class='form-control' style=\"float:left;margin:5px 0px;width:" + width + "px;\" ></select></div>";
                        $this.$element.append(html);
                    }
                    var sel = $("#" + selid).select({
                        data: [],
                        param: n.param,
                       // defaults:null,
                        change: function (id) {
                            $this.change(i,id);
                        }
                    });
                    $this.$items.push(sel);
                });
                $this.setDate(0, $this.opstions.data[$this.opstions.items[0].name]);
            }
            setTimeout(function () {
                if (ops.afterCreate) {
                    ops.afterCreate();
                }
            }, 200);
        },//选择改变事件
        change: function (tag, id) {
            if (tag == this.opstions.items.length - 1) return false;
            var data = new Array();
            var item = this.opstions.items[tag + 1];
            var pid = item.pid;
            if (this.opstions.data[item.name])
            $.each(this.opstions.data[item.name], function (i, n) {
                if (n[pid] == id) {
                    data.push(n);
                }
            });
            if (this.$items[tag + 1]) {
                this.$items[tag + 1].reload(data);
            }
        },
        refresh: function (d) {
            var tag = 0;
            var $this = this;
            var id = $this.$items[tag].getVal();
            if (tag == this.opstions.items.length - 1) return false;
            var data = new Array();
            var item = this.opstions.items[tag + 1];
            var pid = item.pid;
            $.each(this.opstions.data[item.name], function (i, n) {
                if (n[pid] == id) {
                    data.push(n);
                }
            });
            if (this.$items[tag + 1]) {
                this.$items[tag + 1].reload(data);
            }
        },
        setDate:function(num,d)
        {
            var $this = this;
            $this.$items[num].reload(d);
            var id = $this.$items[num].getVal();
            if ($this.$items.length - 1 > num)
            {
                var data = new Array();
                var item = this.opstions.items[num + 1];
                var pid = item.pid;
                if (this.opstions.data[item.name])
                $.each(this.opstions.data[item.name], function (i, n) {
                    if (n[pid] == id) {
                        data.push(n);
                    }
                });
                $this.setDate(++num, data);
            }
        }
    };

   

    //程序入口
    $.fn.cascade = function (opstions) {
        var $this = $(this),
        data = $this.data("cascade");
        var idname = $this.attr("id");
        if ($("[id=" + idname + "]").length > 1) {
            alert("检测到id重复了！");
        }
        $this.data("cascade", (data = new cascade(this, opstions)));
        if (typeof opstions == "string") {
            $.get(opstions, function (d) {
                data.opstions.data = d;
                data.load();
            });
        }
        return data;
    };
    //默认值
    $.fn.cascade.defaults = {
        url: "",
        data: null,
        items: []
    };
})(window.jQuery);

