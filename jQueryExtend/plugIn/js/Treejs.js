﻿/*!
 * 树控件
 * 创建人：qinnailin
 * 创建时间：2014/7/9 
 */

(function ($) {
    $.fn.Tree = function (options) {
        var ops = $.extend(true, {
            url: null,//异步数据
            data: null,//本地数据数据
            treeModel: "default",//默认模式
            multiple: false,//是否多项
            key: "Id",//隐藏id
            text: "Name",//显示内容
            child: "Child",//子节点名称
            expand: false,
            clazz: 0,
            source: "",
            onlyleaf: false,
            selected: function (data) { },//选中节点事件 multiple==true 时该事件不触发 请使用getSelected()
            afterCreate: null
        }, options);
        var objthis = $(this);
        var nodeicon = ops.expand ? "open" : "close";
        var selData;

        var counter = 0;

        var node_li = "<li {{selected}} ><span class=\"button switch {{icon1}}\" ></span>"
        if (ops.treeModel == "checkbox") {
            node_li += "<span  class=\"button chk checkbox_false_full\" ></span>"
        }
        node_li += "<a  data-id=\"{{" + ops.key + "}}\" ><span {{diyicon}} class=\"button {{icon2}}\"></span><span >{{" + ops.text + "}}</span></a>{{child}}</li>"

        if (ops.url == null && ops.data != null) {
            $(objthis).html(fillRootData());
            setdisplay();
            setSelected();
            if (ops.afterCreate) {
                ops.afterCreate();
            }
        } else {
            loadData();
        }

        /**
         * 页面加载完毕
         * @for Treejs
         */
        $(document).ready(function () {
            if (ops.treeModel == "default") {
                $(objthis).on("click", "a", function () {
                    var oth = $(this);
                    var id = $(oth).attr("data-id");
                    var isnode = $(oth).find(".ico_docu").get(0);
                    if (ops.onlyleaf && isnode == undefined) {
                        return false;
                    }
                    if (!ops.multiple) {
                        $(objthis).find("a").removeClass("curSelectedNode");
                    }
                    if ($(oth).hasClass("curSelectedNode")) {
                        $(oth).removeClass("curSelectedNode");
                    } else {
                        $(oth).addClass("curSelectedNode");
                    }
                    var pid = $(oth).parent("li").parent("ul").prev("a").attr("data-id");
                    if (id) {
                        selData = { key: id, pid: pid ? pid : 0 };
                        ops.selected(selData);
                    }
                });
            }

            $(objthis).on("click", "span.switch", function () {
                var oth = $(this);
                var cs = $(oth);
                if (cs.attr("class").search("_close") > 0) {
                    var classs = getClass(cs, "close");
                    $(oth).removeClass(classs).addClass(classs.replace("_close", "_open"));
                } else if (cs.attr("class").search("_open") > 0) {
                    var classs = getClass(cs, "open");
                    $(oth).removeClass(classs).addClass(classs.replace("_open", "_close"));
                }
                cs = $(oth).siblings("a").children("span");

                if (cs.attr("class").search("_close") > 0) {
                    var classs = getClass(cs, "close");
                    $(oth).siblings("a").children("span").removeClass(classs).addClass(classs.replace("_close", "_open"));
                } else if (cs.attr("class").search("_open") > 0) {
                    var classs = getClass(cs, "open");
                    $(oth).siblings("a").children("span").removeClass(classs).addClass(classs.replace("_open", "_close"));
                }
                $(oth).parent("li").find("ul").each(function (i, n) {
                    if ($(this).prevAll("span.switch").attr("class").search("_open") > 0) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            });
            $(objthis).on("click", "span.chk", function () {
                var oth = $(this)
                var isnode = $(oth).siblings("ul").get(0);
                if (isnode && !ops.multiple) {
                    return;
                }
                if (!ops.multiple) {
                    if ($(oth).hasClass("checkbox_true_full_focus")) {
                        $(objthis).find(".chk").removeClass("checkbox_true_full_focus").addClass("checkbox_false_full");
                    } else {
                        $(objthis).find(".chk").removeClass("checkbox_true_full_focus").addClass("checkbox_false_full");
                        $(oth).removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                        var id = $(oth).next("a").attr("data-id");
                        var pid = $(oth).parent("li").parent("ul").prev("a").attr("data-id");
                        if (id) {
                            selData = { key: id, pid: pid ? pid : 0 };
                            ops.selected(selData);
                        }
                    }
                } else {
                    if ($(oth).hasClass("checkbox_false_full")) {
                        $(oth).removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                        $(oth).siblings("ul").find(".chk").removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                        var id = $(oth).next("a").attr("data-id");
                        var pid = $(oth).parent("li").parent("ul").prev("a").attr("data-id");
                        if (id) {
                            selData = { key: id, pid: pid ? pid : 0 };
                            ops.selected(selData);
                        }
                    } else {
                        $(oth).removeClass("checkbox_true_full_focus").addClass("checkbox_false_full");
                        var parent = $(oth).parent("li").parent("ul");
                        var childsize = $(parent).find(".checkbox_true_full_focus").size();
                        $(oth).parent("li").parents("ul").siblings(".chk").removeClass("checkbox_true_full_focus").addClass("checkbox_false_full");
                        var childent = $(oth).siblings("ul").find(".chk").removeClass("checkbox_true_full_focus").addClass("checkbox_false_full");
                    }
                    selectParent($(oth));
                }


            });
        });

        /**
         * 数据加载
         * @method loadData
         * @for Treejs
         */
        function loadData() {
            $.getJSON(ops.url, function (rd) {
                var data;
                if (ops.source == "") {
                    data = rd;
                } else {
                    var temp = rd;
                    var item = ops.source.split('.');
                    $.each(item, function (i, n) {
                        if (n != "") {
                            temp = temp[n];
                        }
                    });
                    data = temp;
                }
                ops.data = data;
                $(objthis).html(fillRootData());
                setdisplay();
                setSelected();
                if (ops.afterCreate) {
                    ops.afterCreate();
                }
            });
        }

        /**
        * 获取class
        * @method getClass
        * @param {object} classs class列表
        * @param {string} str 过滤后缀
        * @for Treejs
        */
        function getClass(node, str) {
            var ccs = node.attr("class").split(' ');
            var res;
            for (var i in ccs) {
                if (str == "close") {
                    res = ccs[i].match(/(.+?_close)/g);
                } else {
                    res = ccs[i].match(/(.+?_open)/g);
                }
                if (res) {
                    break;
                }
            }
            return res + "";
        }

        /**
        * 显示或隐藏子节点
        * @method setdisplay
        * @for Treejs
        */
        function setdisplay() {
            if (nodeicon == "close") {
                $(objthis).find(".fm-tree ul").hide();
            } else {
                $(objthis).find(".fm-tree ul").show();
                if (ops.clazz > 0) {//ico_close
                    $(objthis).find("span.ico_close").parent("a").siblings("ul").hide();
                }
            }
        }

        /**
        * 默认选中
        * @method setSelected
        * @for Treejs
        */
        function setSelected() {
            $(objthis).find("li.selected").each(function (i, n) {
                if (ops.treeModel == "default") {
                    if ($(n).children("ul").size() == 0) {
                        $(n).children("a").addClass("curSelectedNode");
                        $(n).parents("ul").show();
                        $(n).parents("ul").siblings("span.switch").each(function (i, n) {
                            var cs = getClass($(n), "close");
                            $(n).removeClass(cs).addClass(cs.replace("_close", "_open"));
                        });
                        $(n).parents("ul").siblings("a").each(function (i, n) {
                            var cs = getClass($(n).children("span"), "close");
                            $(n).children("span").removeClass(cs).addClass(cs.replace("_close", "_open"));
                        });
                    }
                } else if (ops.treeModel == "checkbox") {
                    if ($(n).children("ul").size() == 0) {
                        $(n).children("span.chk").removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                    } else {
                        $(n).find("span.chk").removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                    }
                    $(n).parents("ul").show();
                    $(n).find("ul").show();
                    $(n).parents("ul").siblings("span.switch").each(function (i, n) {
                        var cs = getClass($(n), "close");
                        $(n).removeClass(cs).addClass(cs.replace("_close", "_open"));
                    });
                    $(n).find("ul").siblings("span.switch").each(function (i, n) {
                        var cs = getClass($(n), "close");
                        $(n).removeClass(cs).addClass(cs.replace("_close", "_open"));
                    });

                    $(n).parents("ul").siblings("a").each(function (i, n) {
                        var cs = getClass($(n).children("span"), "close");
                        $(n).children("span").removeClass(cs).addClass(cs.replace("_close", "_open"));
                    });
                    $(n).find("ul").siblings("a").each(function (i, n) {
                        var cs = getClass($(n).children("span"), "close");
                        $(n).children("span").removeClass(cs).addClass(cs.replace("_close", "_open"));
                    });
                }
            });
        }

        /**
        * 填充root节点数据
        * @method fillData
        * @param {object} ops 控件参数对象
        * @param {string} template 填充模版
        * @for Treejs
        */
        function fillRootData() {
            var html = "<ul class=\"unstyled fm-tree\">";
            $.each(ops.data, function (i, n) {
                var cnt = "roots_" + nodeicon;
                if (i > 0 && i < ops.data.length - 1) {
                    cnt = "center_" + nodeicon;
                } else if (i > 0) {
                    cnt = "bottom_" + nodeicon;
                }
                counter = 0;
                html += fillLiData(n, cnt, "ico_" + nodeicon, ops.data.length - 1 == i);
            });
            html += "</ul>";
            return html;
        }

        /**
        * 填充child节点数据
        * @method fillLiData
        * @param {object} ops 控件参数对象
        * @param {string} template 填充模版
        * @for Treejs
        */


        function fillLiData(data, icon1, icon2, islast) {
            if (data) {
                var cnodeicon = false;
                if (ops.clazz > 0 && ops.expand && ops.clazz - 1 <= counter++) {
                    cnodeicon = true;
                }
                return node_li.replaceo("child", fillChildData(data[ops.child], islast, cnodeicon))
                .replaceo(ops.key, data[ops.key])
                .replaceo(ops.text, data[ops.text])
                .replaceo("icon1", icon1)
                .replaceo("diyicon", (data.Icon && data.Icon != "") ? "style=\"background: url(" + data.Icon + ") 0 0 no-repeat;\"" : "")
                .replaceo("selected", data.Selected ? "class=\"selected\"" : "")
                .replaceo("icon2", icon2);
            } else {
                return "";
            }
        }

        /**
        * 填充child节点数据
        * @method fillChildData
        * @param {object} ops 控件参数对象
        * @param {bool} islast 是否最后一个根节点
        * @for Treejs
        */
        function fillChildData(data, islast, cnodeicon) {
            if (data && data.length > 0) {
                var html = "<ul "
                html += islast ? ">" : "class=\"line\" >";
                $.each(data, function (i, n) {
                    var icon2 = "ico_" + (cnodeicon ? "close" : nodeicon);
                    var icon1 = data.length - 1 == i ? "bottom_" + (cnodeicon ? "close" : nodeicon) : "center_" + (cnodeicon ? "close" : nodeicon);
                    if (!n[ops.child] || n[ops.child].length == 0) {
                        icon2 = "ico_docu";
                        icon1 = data.length - 1 == i ? "bottom_docu" : "center_docu";
                    }
                    html += fillLiData(n, icon1, icon2, data.length - 1 == i);
                });
                html += "</ul>";
                return html;
            } else {
                return "";
            }
        }

        /**
        * 方法组
        * @method fnc
        * @for Treejs
        */
        var fnc = {
            remove: function (url, key) { },
            getSelected: function () {
                selData = new Array();
                if (ops.treeModel == "default") {
                    var seled = $(objthis).find("a.curSelectedNode");
                    $.each(seled, function (i, n) {
                        var id = $(n).attr("data-id");
                        var pid = $(n).parent("li").parent("ul").prev("a").attr("data-id");
                        if (id) {
                            selData.push({ key: id, pid: pid ? pid : 0, text: $(n).parent("li").children("a").text() });
                        }
                    });
                } else if (ops.treeModel == "checkbox") {
                    var seled = $(objthis).find("span.checkbox_true_full_focus");
                    $.each(seled, function (i, n) {
                        //var isnode = $(n).siblings("ul").html();
                        var id = $(n).next("a").attr("data-id");
                        var pid = $(n).parent("li").parent("ul").prev("a").attr("data-id");
                        if (id) {
                            selData.push({ key: id, pid: pid ? pid : 0, text: $(n).parent("li").children("a").text() });
                        }
                    });
                }
                return selData;
            },
            reload: function () {
                loadData();
            },
            refresh: function () {
                $(objthis).html(fillRootData());
                setdisplay();
            },
            setSelected: function (data) {
                if (ops.multiple) {
                    if (data instanceof Object) {
                        $.each(data, function (i, n) {
                            var objsel = $(objthis).find("[data-id=" + n + "]").eq(0);
                            if ($(objsel).get(0)) {
                                $(objsel).siblings(".chk").removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                            }
                            if ($(objsel).nextAll("ul").get(0)) {
                                $(objsel).nextAll("ul").find(".chk").removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                            }
                            selectParent($(objsel).siblings(".chk"));
                        })
                    } else {
                        var strs = data.split(',');
                        $.each(strs, function (i, n) {
                            var objsel = $(objthis).find("[data-id=" + n + "]").eq(0);
                            if ($(objsel).get(0)) {
                                $(objsel).siblings(".chk").removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                                if (ops.treeModel != "checkbox") {
                                    $(objsel).addClass("curSelectedNode");
                                }
                            }
                            if ($(objsel).nextAll("ul").get(0)) {
                                $(objsel).nextAll("ul").find(".chk").removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                            }
                            selectParent($(objsel).siblings(".chk"));
                        })
                    }
                } else {
                    var objsel = $(objthis).find("[data-id=" + data + "]").eq(0);
                    if (ops.treeModel != "checkbox") {
                        $(objsel).addClass("curSelectedNode");
                    }
                    if (!$(objsel).nextAll("ul").get(0)) {
                        $(objsel).siblings(".chk").removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                    }
                }
            },
            getData: function () {
                return ops.data;
            },
            donSelect: function (key) {
                if(ops.treeModel=="checkbox"){
                    $(objthis).find("a[data-id=" + key + "]").prev("span.chk").removeClass("checkbox_true_full_focus").addClass("checkbox_false_full");
                } else {
                    $(objthis).find("a[data-id=" + key + "]").removeClass("curSelectedNode");
                }
            }
        }
        //递归选中需要选中的父节点
        function selectParent(obj) {
            var parent = $(obj).parent("li").parent("ul");
            if ($(parent).get(0)) {
                var size = $(parent).find(".checkbox_false_full").size();
                if (size == 0) {
                    $(parent).siblings(".chk").removeClass("checkbox_false_full").addClass("checkbox_true_full_focus");
                }
                selectParent($(parent).siblings(".chk"));
            }
        }
        return fnc;
    }

})(jQuery);