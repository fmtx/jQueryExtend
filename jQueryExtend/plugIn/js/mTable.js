﻿/*!
 * mTable主要针对移动端的表格插件，瀑布流的形式
 * 创建人：qinnailin
 * 创建时间：2016/5/6 
 *
 *
 * 
 */
!(function ($) {
    "use mTable"
    var mTable = function (element, options) {
        this.init("mTable", element, options);
    }

    mTable.prototype = {
        constructor: mTable,
        init: function (type, element, options) {
            this.type = type;
            this.$element = $(element);
            this.options = this.getOptions(options);
            var width = $(element).width();
            var height = $(element).height();
            var objthis = $(element);
            var empty = false;
            var $this = this;
            this.objthis = objthis;
            this.bindTable();
            this.loadData();
            $(objthis).addClass("container-fluid");
            var bodybox = objthis.find(".scroll_table").eq(0);
            var footbox = bodybox.find("tfoot").eq(0);
            var scrollbox = objthis.find(".scroll_box").eq(0);
            scrollbox.scroll(function (e) {
                var closeToBottom = (scrollbox.scrollTop() + scrollbox.height() > bodybox.height() - 100);
                if (closeToBottom) {
                    $this.reload();
                }
            });
        },
        getOptions: function (options) {
            return $.extend(true, $.fn[this.type].defaults, options, this.$element.data());
        },
        loadData: function () {
            var ops = this.options;
            this.grid=this.objthis.Grid(ops);
        },
        bindTable: function () {
            var ops = this.options;
            var objthis = this.objthis;
            var html = "<div class=\"row-fluid layout-center\"><div class=\"layout-box\">";
            html += "<div class=\"layout-top table_th " + (ops.buttons ? "utable-hasbutton" : "") + "\" style='height:30px; background:#f7f7f7; padding-right:17px;' >";
            html += "<table class='utable' ><thead fm-head><tr>";
            html += "<th ></th>";
            $.each(ops.heads, function (i, n) {
                html += (n.sort ? "<th fm-sort='" + n.key + "' " + (n.width ? "width='" + n.width + "'" : '') + " >" : "<th " + (n.width ? "width='" + n.width + "'" : '') + ">") + n.name + "</th>";
            });
            html += "</tr></thead></table>";
            html += "</div><div class=\"row-fluid layout-center scroll_box\" style='overflow-y:auto'>";
            html += "<table class='utable scroll_table table_hover table_ch_c'  ><tbody fm-body><tr>";
            $.each(ops.heads, function (i, n) {
                html += "<td " + (n.conver ? "fm-conver='" + n.conver + "'" : "") + " " + (n.width ? "width='" + n.width + "'" : '') + " >{{" + n.key + "}}" + (n.html ? n.html : "") + (n.num ? "{{rowNumber}}" : "") + "</td>";
            });
            html += "</tr></tbody><tfoot><tr><td style='text-align:center;' colspan='" + ops.heads.length+ "'>数据加载中...</td></tr></tfoot></table>";
            html += "</div>";
            if (ops.page.enable) {
                html += "<div class=\"row-fluid layout-bottom hide\" style='height:0px;' ><div fm-pagerbox class=\"tfoot\"></div></div>";
            }
            html += "</div></div>";
            $(objthis).append(html);
        },
        reload: function () {
            var objthis = this.objthis;
            var app = this.grid;
            var res = app.loadNext();
            if (!res)
            {
                objthis.find("tfoot").addClass("hide");
            }
        }
    }
    //插件入口
    $.fn.mTable = function (options) {
        var data = new mTable(this, options);
        return data.grid;
    }
    //初始值
    $.fn.mTable.defaults = {
        url: "",
        data: null,
        source: "",
        afterCreate: null,
        checkbox: {
            enable: false,
            click: function () { }
        },
        page: {
            enable: true,
            pageSize: 50,
            reqindex: "index",
            reqsize: "size"
        },
        append:true
    }

})(window.jQuery);