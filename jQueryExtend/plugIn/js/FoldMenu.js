﻿/*!
 * 手风琴菜单
 * 创建人：qinnailin
 * 创建时间：2014/8/10 
 *
 *
 * 
 */

(function ($) {
    $.fn.FoldMenu = function (options) {
        var ops = $.extend(true, {
            url: "",
            data: null,
            source: "",
            child: "Child",
            key: "Id",
            text: "Name",
            width: 220,
            selected: function (key) { }
        }, options);
        var objthis = $(this);
        $(objthis).addClass("fm-fold").css("width", ops.width);

        loadData();
        /**
         * 数据加载
         * @method loadData
         * @for FoldMenu
         */
        function loadData() {
            if (ops.url == "" && ops.data != null) {
                fillData();
            } else if (ops.url != "") {
                $.getJSON(ops.url, function (rd) {
                    var temp = rd;
                    var item = ops.source.split('.');
                    $.each(item, function (i, n) {
                        if (n != "") {
                            temp = temp[n];
                        }
                    });
                    ops.data = temp;
                    fillData();
                });
            }
        }

        /**
         * 填充数据
         * @method fillData
         * @for FoldMenu
         */
        function fillData() {
            var html = "<ul class=\"menu-one\" " + (ops.width != 220 ? "style='width:" + ops.width + "px'" : '') + ">";
            var li = "<li " + (ops.width != 220 ? "style='width:" + ops.width + "px'" : '') + " ><div class=\"header\"><span class=\"txt\">{{" + ops.text + "}}</span><span class=\"arrow\"></span></div>{{childs}}</li>";
            $.each(ops.data, function (i, n) {
                html += li.fill(n).replaceo("childs", fillChild(n[ops.child]));
            });
            html += "</ul>";
            $(objthis).html(html);
            setDefault();
        }

        /**
         * 填充子数据
         * @method fillChild
         * @param {object} child 子节点对象
         * @for FoldMenu
         */
        function fillChild(child) {
            if (child && child.length > 0) {
                var html = "<ul class=\"menu-two\" " + (ops.width != 220 ? "style='width:" + ops.width + "px'" : '') + " >";
                var li = "<li " + (ops.width != 220 ? "style='width:" + ops.width + "px'" : '') + "  ><a href=\"javascript:\" data-sel=\"{{Selected}}\" data-id=\"{{" + ops.key + "}}\" >{{" + ops.text + "}}</a></li>";
                $.each(child, function (i, n) {
                    html += li.fill(n);
                });
                html += "</ul>";
                return html;
            } else {
                return "";
            }
        }

        /**
         * 设置默认选择
         * @method setDefault
         * @for FoldMenu
         */
        function setDefault() {
            $(objthis).find("a").each(function (i, n) {
                if ($(n).attr("data-sel").toUpperCase() == "TRUE") {
                    $(n).parents("ul").show();
                    $(n).parents("li").addClass("menu-show")
                    ops.selected($(n).attr("data-id"));
                    return;
                }
            });
            $(objthis).find("li").eq(0).addClass("firstChild");
            $(objthis).find("ul").each(function (i, n) {
                $(n).find("li").eq(0).addClass("firstChild");
            });
        }

        /**
         * 页面加载完毕
         * @method ready
         * @for FoldMenu
         */
        $(document).ready(function () {
            $(objthis).on("click", ".header", function () {
                if ($(this).parent("li").hasClass("menu-show")) {
                    $(this).next("ul").slideUp(300);
                    $(this).parent("li").removeClass("menu-show");
                } else {
                    $(this).next("ul").slideDown(300);
                    $(this).parent("li").addClass("menu-show");
                }
                $(this).parent("li").siblings("li").removeClass("menu-show").find("ul").slideUp(300);
            });

            $(objthis).on("click", "a", function () {
                var key = $(this).attr("data-id");
                ops.selected(key);
            });
        });

        var fnc = {

        }

        return fnc;
    }
})(jQuery);