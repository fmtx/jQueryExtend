﻿/*!
 * 树控件
 * 创建人：qinnailin
 * 创建时间：2014/10/17
 */


(function ($) {
    $.fn.uTree = function (opstions) {
        var setting = $.extend(true, {
            treeId: "",
            treeObj: null,
            view: {
                autoCancelSelected:false,
                selectedMulti: true,
                checkedMulti:true,
                expand:-1//设置节点展开层级，默认-1不展开
            },
            check: {
                enable: false,
                chkStyle: "checkbox",
                chkboxType: { "Y": "ps", "N": "ps" },
                chkParent:true
            },
            data: {
                key: {
                    children: "Child",
                    name: "Name",
                    title: "",
                    url: "url"
                },
                keep: {
                    parent: false,
                    leaf: false
                },
                Nodes:null
            },
            callback: {
                afterCreate:null
            }
        }, opstions);
        if (setting.view.checkedMulti == false)
        {
            setting.check.chkboxType = { "Y": "", "N": "" };
        }
        var objthis = $(this);
        var strid = $(objthis).attr("id");
        if (!objthis.hasClass("ztree")) {
            objthis.addClass("ztree");
        }
        var zTree;
        try {
            if (setting.view.expand != -1) {//设置为展开时
                var oncreatefun;
                if (setting.callback.onNodeCreated != null) {
                    oncreatefun = setting.callback.onNodeCreated;
                }
                setting.callback.onNodeCreated = function (e, treeId, treeNode) {
                    var zTreeo = $.fn.zTree.getZTreeObj(strid);
                    if (treeNode.level <= setting.view.expand) {
                        zTreeo.expandNode(treeNode, null, null, null, true);
                    }
                    if (oncreatefun) {
                        oncreatefun(e, treeId, treeNode);
                    }
                }
            }
            var onbeforeCheck;
            if (setting.callback.beforeCheck != null) {
                onbeforeCheck = setting.callback.beforeCheck;
            }
            setting.callback.beforeCheck = function (treeId, treeNode) {
                if (onbeforeCheck) {
                    onbeforeCheck(treeId, treeNode);
                }
                if (setting.check.chkParent == false && treeNode.isParent) {
                    return false;
                }
                if (setting.view.checkedMulti == false) {
                    var zTreeo = $.fn.zTree.getZTreeObj(strid);
                    var nowschkNodes = zTreeo.getCheckedNodes();
                    if (nowschkNodes && nowschkNodes[nowschkNodes.length - 1] == treeNode) {
                        return;
                    }
                    if (!treeNode[setting.data.key.checked]) {
                        zTreeo.checkAllNodes(false);
                    }
                }
            }
            
            var tempfunc = setting.callback.onAsyncSuccess;
            setting.callback.onAsyncSuccess = function (event, treeId, treeNode, msg) {
                if (tempfunc) {
                    tempfunc(event, treeId, treeNode, msg);
                }
                if (setting.callback.afterCreate) {
                    setting.callback.afterCreate();
                }
                var selnodes = [];
                if (setting.check.enable) {
                    selnodes = zTree.getCheckedNodes(true);

                } else {
                    selnodes = zTree.getSelectedNodes();
                }
                $.each(selnodes, function (i, n) {
                    expandpNode(n);
                });
            }
            zTree = $.fn.zTree.init(objthis, setting, setting.data.Nodes);
        } catch (e) {
            $.msg.alert("请引入zTree类库!");
            console.debug(e);
        }
        //拓展方法
        $.extend(true, zTree, {
            setSelected: function (obj) {
                var nodes = zTree.transformToArray(zTree.getNodes());
                var selid = obj.split(',');
                for (var i = 0; i < selid.length; i++) {
                    for (var j = 0; j < nodes.length; j++) {
                        if (nodes[j][setting.data.simpleData.idKey] == selid[i]) {
                            if (setting.check.enable) {
                                zTree.checkNode(nodes[j], true, true);
                            } else {
                                zTree.selectNode(nodes[j], setting.view.selectedMulti);
                            }
                            break;
                        }
                    }
                }
                var selnodes = [];
                if (setting.check.enable) {
                    selnodes = zTree.getCheckedNodes(true);
                    
                } else {
                    selnodes = zTree.getSelectedNodes();
                }
                $.each(selnodes, function (i, n) {
                    expandpNode(n);
                });
            },
            donSelect: function (id) {
                if (setting.check.enable) {
                    var selnode = zTree.getCheckedNodes(true);
                    for (var i = 0; i < selnode.length; i++) {
                        if (selnode[i][setting.data.simpleData.idKey] == id) {
                            zTree.checkNode(selnode[i], false, true);
                            break;
                        }
                    }
                } else {
                    var selnode = zTree.getSelectedNodes();
                    for (var i = 0; i < selnode.length; i++) {
                        if (selnode[i][setting.data.simpleData.idKey] == id)
                        {
                            zTree.cancelSelectedNode(selnode[i]);
                            break;
                        }
                    }
                }
            },
            unSelectAll: function () {
                if (setting.check.enable) {
                    var selnode = zTree.getCheckedNodes(true);
                    for (var i = 0; i < selnode.length; i++) {
                        zTree.checkNode(selnode[i], false, true);
                    }
                } else {
                    var selnode = zTree.getSelectedNodes();
                    for (var i = 0; i < selnode.length; i++) {
                        zTree.cancelSelectedNode(selnode[i]);
                    }
                }
            },
            reload: function () {
                zTree.refresh();
            }
        });
        //展开父节点
        function expandpNode(nodes) {
            zTree.expandNode(nodes, true, false,false, false);
            var pnode = nodes.getParentNode();
            if (pnode) {
                expandpNode(pnode);
            }
        }
        if (setting.async == undefined || !setting.async.enable) {
            var selnodes = [];
            if (setting.check.enable) {
                selnodes = zTree.getCheckedNodes(true);

            } else {
                selnodes = zTree.getSelectedNodes();
            }
            $.each(selnodes, function (i, n) {
                expandpNode(n);
            });
            setTimeout(timeout, 50);
        }
        function timeout() {
            if (setting.callback.afterCreate) {
                setting.callback.afterCreate();
            }
        }
        return zTree;
    }
})(jQuery);