﻿

!(function ($) {
    "use strict"
    var Table = function (element, options) {
        this.init("Table", element, options);
    };
    var grid;
    Table.prototype = {
        constructor: Table,
        init: function (type, element, options) {
            this.type = type;
            var objthis = this.$element = element;
            var ops = this.options = this.getOptions(options);
            var html = "<div class='widget' >";
            html += "<div class='widget-header'>";
            html += "<div class='title'>";
            html += ops.title;
            html += "</div>";
            if (objthis.find(".table-search").get(0)) {
                html += '<span class="tools" ><a class="btn tool_search" ><i class="fa fa-filter"></i></a></span>';
            }
            html += "</div>";
            html += "<div class='widget-body' >";
            html += "<table class='table table-responsive table-striped table-bordered table-hover no-margin' ><thead>";
            html += "<tr>";
            if (ops.checkbox.enable && ops.checkbox.multiple) {
                html += "<th class='hidden-xs' width='18px' ><input type='checkbox' class='chkAll' /></th>";
            } else if (ops.checkbox.enable) {
                html += "<th class='hidden-xs' ></th>";
            }
            $.each(ops.heads, function (i, n) {
                html += (n.sort ? "<th " + (n['xsshow'] ? '' : 'class="hidden-xs"') + " fm-sort='" + n.key + "' " + (n.width ? "width='" + n.width + "'" : '') + " >" : "<th " + (n['xsshow'] ? '' : 'class="hidden-xs"') + " " + (n.width ? "width='" + n.width + "'" : '') + ">") + n.name + "</th>";
            });
            html += "</tr>";
            html += "</thead>";
            html += "<tbody fm-body ><tr>";
            if (ops.checkbox.enable) {
                html += "<td class='hidden-xs' ><input type='checkbox' /></td>";
            }
            $.each(ops.heads, function (i, n) {
                html += "<td " + (n['xsshow'] ? '' : 'class="hidden-xs"') + " " + (n.conver ? "fm-conver='" + n.conver + "'" : "") + " " + (n.width ? "width='" + n.width + "'" : '') + " >{{" + n.key + "}}" + (n.html ? n.html : "") + (n.num ? "{{rowNumber}}" : "") + "</td>";
            });
            html += "</tr></tbody>";
            html += "</table>";
            if (ops.page.enable) {
                html += "<div class='dataTables_paginate paging_full_numbers' style='height:40px;' ><div style='max-height:40px;float:right;' fm-pagerbox></div></div>";
            }
            html += "</div>";
            html += "</div>";
            this.$element.append(html);
            $.each(ops.tools, function (i, n) {
                $(objthis).find(".title").after('<span class="tools" ><a class="btn toola_' + i + '" ><i class="fa ' + n.icon + '"></i></a></span>');
                $(objthis).on("click", ".toola_" + i, function () {
                    n.click();
                });
            });

            $(objthis).on("click", ".tool_search", function () {
                $(objthis).find(".table-search").show();
            });

            if ($(objthis).find(".table-search").get(0)) {
                $(objthis).find(".table-search").css("right","15px");
                $(objthis).find(".table-search").append("<div class='table-search-btn' ><button class='table-search-btn-close' >关闭</button><button class='table-search-btn-search'>检索</button></div>");
                $(objthis).find(".table-search-btn-close").click(function () {
                    $(objthis).find(".table-search").hide();
                });
                $(objthis).find(".table-search-btn-search").click(function () {
                    $(objthis).find("[fm-search]").submit();
                    $(objthis).find(".table-search").hide();
                });
            }
            if (ops.page.click) {
                var tempc = ops.page.click;
                ops.page.click = function () {
                    objthis.find(".chkAll").prop("checked", false);
                    tempc();
                }
            } else {
                ops.page.click = function () {
                    objthis.find(".chkAll").prop("checked", false);
                }
            }
            grid = this.$element.Grid(ops);

            $(objthis).find("[fm-body]").on("click", "input[type='checkbox']", function () {
                var row = $(this).parents("tr");
                if (row.hasClass("checked_tr")) {
                    $(row).removeClass("checked_tr");
                    $(row).find("input[type='checkbox']").prop("checked", false);
                    $(objthis).find(".chkAll").prop("checked", false);
                } else {
                    if (!ops.checkbox.multiple) {
                        $(objthis).find("[fm-body] input[type='checkbox']").prop("checked", false);
                        $(objthis).find("[fm-body]>tr").removeClass("checked_tr");
                    }
                    $(row).addClass("checked_tr");
                    $(row).find("input[type='checkbox']").prop("checked", true);
                    if ($(objthis).find("[fm-body] input:checked").size() == $(objthis).find("[fm-body]>tr").size()) {
                        if (ops.checkbox.multiple) {
                            $(objthis).find(".chkAll").prop("checked", true);
                        }
                    }
                }
            });

            if (!ops.checkbox.enable) {
                $(objthis).find("[fm-body]").on("click", "tr", function () {
                    var row = $(this);
                    if (row.hasClass("checked_tr")) {
                        if (ops.cancel) {
                            $(row).removeClass("checked_tr");
                            $(row).find("input[type='checkbox']").removeAttr("checked");
                            $(objthis).find(".chkAll").removeAttr("checked");
                        }
                    } else {
                        if (!ops.multiple) {
                            $(objthis).find("[fm-body] input[type='checkbox']").removeAttr("checked");
                            $(objthis).find("[fm-body]>tr").removeClass("checked_tr");
                        }
                        $(row).addClass("checked_tr");
                        $(row).find("input[type='checkbox']").attr("checked", "checked");
                        if ($(objthis).find("[fm-body] input:checked").size() == $(objthis).find("[fm-body]>tr").size()) {
                            if (ops.multiple) {
                                $(objthis).find(".chkAll").attr("checked", "checked");
                            }
                        }
                    }
                });
            }

            $(objthis).on("click", ".chkAll", function () {
                if ($(this).is(':checked')) {
                    $(objthis).find("[fm-body] input[type='checkbox']").prop("checked", true);
                    $(objthis).find("[fm-body]>tr").addClass("checked_tr");
                } else {
                    $(objthis).find("[fm-body] input[type='checkbox']").removeClass("checked_tr");
                    $(objthis).find("[fm-body] input[type='checkbox']").prop("checked", false);;
                    $(objthis).find("[fm-body]>tr").removeClass("checked_tr");
                }
                if (ops.click) {
                    ops.click();
                }
            });
            $("#div-mtable-box").on("contextmenu", "[fm-body]>tr", function () {
                //var data = $(this).data("jsondata");
                //console.log(data);
                return false;
            });
        },
        getOptions: function (options) {
            options = $.extend(true, $.fn[this.type].defaults, options, this.$element.data());
            return options;
        },
        getSelectData: function () {//获取选中内容
            var objthis = this.$element;
            var arr = new Array();
            var data = grid.getData();
            $(objthis).find("[fm-body]>tr.checked_tr").each(function (i, n) {
                arr.push($(n).data("jsondata"));
            });
            return arr;
        },
        refresh: function () {
            grid.refresh();
        },
        reload: function () {
            grid.reload();
        },
        load: function (opst) {
            grid.load(opst);
        }
    };
    //入口
    $.fn.Table = function (options) {
        var data = this.data("Table");
        data = new Table(this, options);
        if (typeof options == 'string' && options != "") {
            data.options.placement = options;
        }
        return data;
    }
    //默认值
    $.fn.Table.defaults = {
        title: "",
        tools: [],
        checkbox: {
            enable: false,
            multiple: true,
            click: function () { }
        },
        page: {
            enable: true
        },
        buttons: null,
        heads: [],
        afterCreate: null
    }
})(jQuery);